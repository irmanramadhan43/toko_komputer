<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RajaOngkir API Key
 * Silakan daftar akun di RajaOngkir.com untuk mendapatkan API Key
 * http://rajaongkir.com/akun/daftar
 */
$config['rajaongkir_api_key'] = "b3f87ceef6eb7e499d57a781ce64ffc4";

/**
 * RajaOngkir account type: starter or basic
 * http://rajaongkir.com/dokumentasi#akun-ringkasan
 *
 */
$config['rajaongkir_account_type'] = "starter";
