<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	function __construct()
  {
    parent::__construct();
		$this->load->helper('berat_helper');

		$this->load->model('Bank_model');
		$this->load->model('Cart_model');
    $this->load->model('Company_model');
    $this->load->model('Kontak_model');
		$this->load->model('Produk_model');
		$this->load->model('Featured_model');
		$this->load->model('Blog_model');
		$this->load->model('Penjualan_model');

		$this->data['company_data'] 			= $this->Company_model->get_by_company();
    $this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
  }

	public function index()
	{
		$this->data['title'] 										= 'Keranjang Belanja';

		// ambil data keranjang
		$this->data['cart_data'] 			    			= $this->Cart_model->get_cart_per_customer();
		// ambil total_berat_dan_subtotal
		$this->data['total_berat_dan_subtotal'] = $this->Cart_model->get_total_berat_dan_subtotal();
		// ambil data customer
		$this->data['customer_data'] 			    	= $this->Cart_model->get_data_customer();
		//var_dump($this->data['customer_data']);die;

    $this->load->view('front/cart/body', $this->data);
  }

	function buy_n(){
		$this->data['title'] 										= 'Keranjang Belanja';
    $this->data['action']                 = site_url('cart/checkout');

		// ambil data keranjang
		$this->data['cart_data'] 			    			= $this->Cart_model->get_cart_per_customer();
		// ambil total_berat_dan_subtotal
		$this->data['kota'] = $this->Cart_model->get_AllKota();
		$this->data['total_berat_dan_subtotal'] = $this->Cart_model->get_total_berat_dan_subtotal();
		// ambil data customer
		$this->data['customer_data'] 			    	= $this->Cart_model->get_data_customer();

    $this->load->view('front/cart/buy', $this->data);
	}

	function delete_cart(){
		//batas waktu di cart



		$tz = 'Asia/Jakarta';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
		$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
		$tanggal_waktu=$dt->format('Y-m-d H:i:s');
		$tanggal=$dt->format('Y-m-d');
		//END TIMEZONE

		//var_dump($data);
		$this->db->select('transaksi_detail.*');
		$this->db->from('transaksi');
		$this->db->join('transaksi_detail', 'transaksi.id_trans = transaksi_detail.trans_id', 'left');
		$this->db->where('waktu_habis <', $tanggal_waktu);
		$this->db->where('status', '0');
		$data = $this->db->get()->result();

		foreach ($data as $k) {
			$query = "UPDATE produk SET stok = stok + $k->total_qty where id_produk = $k->produk_id ";
			$this->db->query($query);
		}

		$this->db->where('waktu_habis <', $tanggal_waktu);
		$this->db->where('status', '0');
		$this->db->delete('transaksi');
		$this->session->unset_userdata('transaksi');
		echo "Data sudah terhapus";

	}


	public function buy($id)
		{
			// echo "halo";
			// die;
			//batas waktu di cart
			$tz = 'Asia/Jakarta';
	    $timestamp = time();
	    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
	    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
	    $tanggal_waktu=$dt->format('Y-m-d H:i:s');
	    $tanggal=$dt->format('Y-m-d');
	    //END TIMEZONE
			$currentDate = strtotime($tanggal_waktu);
			//menset waktu dalam 1 hari
			$futureDate = $currentDate+(60*1);
			$formatDate = date("Y-m-d H:i:s", $futureDate);
			// ambil data produk
			$row = $this->Produk_model->get_by_id($id);
			$stok = $row->stok;
			// cek id produk
	    if($row)
	    {
					// Cek sudah/ belum login
					if ($this->ion_auth->logged_in()){
						// cek transaksi per user sedang login
						$cek_transaksi 	= $this->Cart_model->cek_transaksi();
						$id_trans 			= $cek_transaksi->id_trans;

						// cek data barang yang dibeli dan masuk ke tabel transaksi_detail
						$notransdet 				= $this->Cart_model->get_notransdet($id);

						// jika transaksi sudah ada
						if($cek_transaksi)
						{
							// jika barang yang dibeli sudah ada di cart == update
							if($notransdet)
							{
								$jmllama          = $notransdet->total_qty;
								$qty_new        	= $jmllama + 1;
								$subtotaltambah   = $qty_new * $row->harga_diskon;

								$jmlberatlama     = $row->berat;
								$jmlberattambah   = $jmlberatlama * $qty_new;

								$stoksekarang 		= $stok - 1;

								$datastok = array(
									'stok' => $stoksekarang,
								);

								$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);

								$data = array(
									'total_qty'  	=> $qty_new,
									'total_berat' => $jmlberattambah,
									'subtotal'  	=> $subtotaltambah,
								);

								// update transaksi
								$this->Cart_model->update_transdet($id,$data);

								// set pesan data berhasil dibuat
								$this->session->set_flashdata('message', '<div class="alert alert-success alert">Barang berhasil ditambahkan</div>');
								redirect(site_url('cart'));
							}
								// jika barang yang dibeli belum ada di cart == tambahkan
								else
								{

									$data2 = array(
										'trans_id'  	=> $id_trans,
										'user'  			=> $this->session->userdata('user_id'),
										'produk_id' 	=> $id,
										'harga'  			=> $row->harga_diskon,
										'berat'  			=> $row->berat,
										'total_qty'  	=> '1',
										'total_berat' => $row->berat,
										'subtotal'  	=> $row->harga_diskon,
									);


									$stoksekarang 		= $stok - 1;

									$datastok = array(
										'stok' => $stoksekarang,
									);

									$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);


									$this->Cart_model->insert_detail($data2);

									// set pesan data berhasil dibuat
									$this->session->set_flashdata('message', '<div class="alert alert-success alert">Barang berhasil ditambahkan</div>');
									redirect(site_url());
								}
						}
							// jika belum ada transaksi
							else
							{
								$data = array(
									'user_id'  => $this->session->userdata('user_id'),
									'jenis_transaksi' => 'online',
									'created' => date('Y-m-d'),
									'status' => '0',
									'waktu_mulai' => $tanggal_waktu,
									'waktu_habis' => $formatDate,
								);

								// eksekusi query INSERT
								$this->Cart_model->insert($data);

								$cek_transaksi 	= $this->Cart_model->cek_transaksi();

								$data2 = array(
									'trans_id'  	=> $cek_transaksi->id_trans,
									'user'  			=> $this->session->userdata('user_id'),
									'produk_id' 	=> $id,
									'harga'  			=> $row->harga_diskon,
									'berat'  			=> $row->berat,
									'total_qty'   	=> '1',
									'total_berat' => $row->berat,
									'subtotal'  	=> $row->harga_diskon,
								);

								$stoksekarang 		= $stok - 1;

								$datastok = array(
									'stok' => $stoksekarang,
								);

								$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);

								$this->Cart_model->insert_detail($data2);

								// set pesan data berhasil dibuat
								$this->session->set_flashdata('message', '<div class="alert alert-success alert">Barang berhasil ditambahkan</div>');
								redirect(site_url());
							}
					}else{
						// cek transaksi per user sedang login
						// $cek_transaksi 	= $this->Cart_model->cek_transaksi_login_null();
						$cek_transaksi=$this->session->userdata('transaksi');
						// $id_trans 			= $cek_transaksi->id_trans;
						// var_dump($cek_transaksi);
						// die;
						// cek data barang yang dibeli dan masuk ke tabel transaksi_detail
						$notransdet 				= $this->Cart_model->get_notransdet($id);
						// var_dump($cek_transaksi);
						// die;
						// jika transaksi sudah ada
						if($cek_transaksi!='')
						{

							// jika barang yang dibeli sudah ada di cart == update
							if($notransdet)
							{
								$jmllama          = $notransdet->total_qty;
								$qty_new        	= $jmllama + 1;
								$subtotaltambah   = $qty_new * $row->harga_diskon;

								$jmlberatlama     = $row->berat;
								$jmlberattambah   = $jmlberatlama * $qty_new;

								$data = array(
									'total_qty'  	=> $qty_new,
									'total_berat' => $jmlberattambah,
									'subtotal'  	=> $subtotaltambah,
								);

										$stoksekarang 		= $stok - 1;

								$datastok = array(
									'stok' => $stoksekarang,
								);

								$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);

								// update transaksi
								$this->Cart_model->update_transdet($id,$data);

								// set pesan data berhasil dibuat
								$this->session->set_flashdata('message', '<div class="alert alert-success alert">Barang berhasil ditambahkan</div>');
								redirect(site_url());
							}
								// jika barang yang dibeli belum ada di cart == tambahkan
								else
								{
									$data2 = array(
										'trans_id'  	=> $cek_transaksi,
										'user'  			=> null,
										'produk_id' 	=> $id,
										'harga'  			=> $row->harga_diskon,
										'berat'  			=> $row->berat,
										'total_qty'  	=> '1',
										'total_berat' => $row->berat,
										'subtotal'  	=> $row->harga_diskon,
									);

									$stoksekarang 		= $stok - 1;

									$datastok = array(
										'stok' => $stoksekarang,
									);

									$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);

									$this->Cart_model->insert_detail($data2);

									// set pesan data berhasil dibuat
									$this->session->set_flashdata('message', '<div class="alert alert-success alert">Barang berhasil ditambahkan</div>');
									redirect(site_url());
								}
						}
							// jika belum ada transaksi
							else
							{
								$data = array(
									'cart'  => 'ya',
									'jenis_transaksi' => 'online',
									'created' => date('Y-m-d'),
									'status' => '0',
									'waktu_mulai' => $tanggal_waktu,
									'waktu_habis' => $formatDate,
								);

								// eksekusi query INSERT
								$this->Cart_model->insert($data);
								$id_transaksi = $this->db->insert_id();
								$this->session->set_userdata('transaksi',$id_transaksi);

								$cek_transaksi 	= $this->Cart_model->cek_transaksi_login_null();

								$data2 = array(
									'trans_id'  	=> $cek_transaksi->id_trans,
									'user'  			=> null,
									'produk_id' 	=> $id,
									'harga'  			=> $row->harga_diskon,
									'berat'  			=> $row->berat,
									'total_qty'  	=> '1',
									'total_berat' => $row->berat,
									'subtotal'  	=> $row->harga_diskon,
								);

								$stoksekarang 		= $stok - 1;

								$datastok = array(
									'stok' => $stoksekarang,
								);

								$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);

								$this->Cart_model->insert_detail($data2);

								// set pesan data berhasil dibuat
								$this->session->set_flashdata('message', '<div class="alert alert-success alert">Barang berhasil ditambahkan</div>');
								redirect(site_url());
							}
					}
				}
				else
				{
					$this->session->set_flashdata('message', '
					<div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
						<i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
					</div>');
					redirect(base_url());
				}
		}

	public function update($id)
	{
		$id = $this->input->post('produk_id');
		$id_td =$this->input->post('id_transdet');
		$row 			= $this->Produk_model->get_by_id($id);
		$rowd			= $this->Cart_model->get_transdet_by_id($id_td);
		$stok = $row->stok;
		$updatee = $_POST['update'];
		if ($this->input->post('qty') > $row->stok && $updatee) {
			$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
				<i class="ace-icon fa fa-bullhorn green"></i> Melebihi Stok
			</div>');
			redirect(base_url('cart'));
		}else{
			if(isset($_POST['update']))
			{
				$qty_new        	= $this->input->post('qty');
				$subtotaltambah   = $qty_new * $row->harga_diskon;

				$jmlberatlama     = $row->berat;
				$jmlberattambah   = $jmlberatlama * $qty_new;

				$data = array(
					'total_qty'  	=> $this->input->post('qty'),
					'total_berat' => $jmlberattambah,
					'subtotal'  	=> $subtotaltambah,
				);

				if ($qty_new > $rowd->total_qty) {
						$stoksekarang 		= $stok - ($qty_new - $rowd->total_qty);
						$datastok = array(
							'stok' => $stoksekarang,
						);
						$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);
				} else {
						$stoksekarang 		= $stok + ($rowd->total_qty - $qty_new);
						$datastok = array(
							'stok' => $stoksekarang,
						);
						$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);
				}
				$this->Cart_model->update_transdet($id,$data);

				// set pesan data berhasil dibuat
				$this->session->set_flashdata('message', '<div class="alert alert-success alert">Berhasil Update Keranjang</div>');
				redirect(site_url('cart'));
			}
			elseif(isset($_POST['delete']))
			{
		    if ($row)
		    {
					$cek_transaksi 	= $this->Cart_model->cek_transaksi();
					$stoksekarang 		= $stok + $rowd->total_qty;
					$datastok = array(
						'stok' => $stoksekarang,
					);
					$this->Cart_model->update_stok($datastok,'produk','id_produk',$id);

					$id_trans 			= $cek_transaksi->id_trans;

		      $this->Cart_model->delete($id,$id_trans);
		      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Barang berhasil dihapus</div>');
		      redirect(site_url('cart'));
		    }
		      // Jika data tidak ada
		      else
		      {
		        $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Barang tidak ditemukan</div>');
		        redirect(site_url('cart'));
		      }
			}
		}


	}

	public function empty_cart($id_trans)
	{
		$id_trans = $this->uri->segment(3);

		$this->Cart_model->kosongkan_keranjang($id_trans);

		$this->session->unset_userdata('transaksi');

		$this->session->set_flashdata('message', '<div class="alert alert-block alert-success"><i class="ace-icon fa fa-bullhorn green"></i> Keranjang Anda telah dikosongkan</div>');

		redirect(site_url('cart'));
	}

	function kode(){
		$invoice = $this->Cart_model->buat_kode();
		echo $invoice;
	}

	public function checkout()
	{
		$this->data['title'] 							= 'Transaksi Selesai';

		$id_trans = $this->input->post('id_trans');
		$invoice = $this->Cart_model->buat_kode();
		$id = $id_trans;
		$this->data['id_trans'] = $id_trans;
		$this->data['data_n_login'] = $this->Cart_model->get_all_transaksi($id_trans);
		$this->data['data_n_login_row'] = $this->Cart_model->get_trans_row($id_trans);


		if($this->input->post('jenis_pembayaran') == 'online'){

			$data = array(
				'nama_pembeli' => $this->input->post('nama_pembeli'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'created' => date('Y-m-d'),
				'kurir'  		=> $this->input->post('kurir'),
				'ongkir' 		=> $this->input->post('ongkir'),
				'service'  	=> $this->input->post('service'),
				'no_invoice' => $invoice,
				'jenis_transaksi' => $this->input->post('jenis_pembayaran'),
				'status'		=> '1',
			);
		}else{
			$data = array(
				'nama_pembeli' => $this->input->post('nama_pembeli'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'created' => date('Y-m-d'),
				'kurir'  		=> $this->input->post('kurir'),
				'ongkir' 		=> $this->input->post('ongkir'),
				'service'  	=> $this->input->post('service'),
				'no_invoice' => $invoice,
				'jenis_transaksi' => $this->input->post('jenis_pembayaran'),
				'status'		=> '1',
			);
		}



		$this->Cart_model->checkout($id_trans,$data);

		$this->data['cart_finished']	    			= $this->Cart_model->get_cart_per_customer_finished($id_trans);
		$this->data['cart_finishedd']	    			= $this->Cart_model->get_cart_per_customer_finished_($id_trans);
		$this->data['customer_data'] 						= $this->Cart_model->get_data_customer();
		$this->data['total_berat_dan_subtotal'] = $this->Cart_model->get_total_berat_dan_subtotal_finished($id_trans);
		$this->data['data_bank'] 								= $this->Bank_model->get_all();
		$this->data['invoice']										= $this->Cart_model->get_invoice_by_id_trans($id)->row();

		$this->session->set_flashdata('message', '
		<div class="col-lg-12">
			<div class="alert alert-block alert-success"><i class="ace-icon fa fa-bullhorn green"></i> Transaksi Selesai</div>
		</div>');

		$this->session->unset_userdata('transaksi');

		$this->load->view('front/cart/finished', $this->data);

	}

	public function download_invoice($id)
	{
    $row 			= $this->Cart_model->get_by_id($id);
		$id_trans = $id;

    if ($row)
    {
      ob_start();

			$this->data['cart_finished']	    				= $this->Cart_model->get_cart_per_customer_finished($id);
			$this->data['total_berat_dan_subtotal'] 	= $this->Cart_model->get_total_berat_dan_subtotal_finished($id);
			$this->data['customer_data'] 							= $this->Cart_model->get_data_customer();
			$this->data['data_bank'] 									= $this->Bank_model->get_all();
			$this->data['invoice']										= $this->Cart_model->get_invoice_by_id_trans($id)->row();
			$this->data['data_n_login'] = $this->Cart_model->get_all_transaksi($id_trans);
			$this->data['data_n_login_row'] = $this->Cart_model->get_trans_row($id_trans);

      $this->load->view('front/cart/download_invoice', $this->data);

      $html = ob_get_contents();
      $html = '<title style="font-family: freeserif">'.nl2br($html).'</title>';
      ob_end_clean();

      require_once('application/libraries/html2pdf/html2pdf.class.php');
      $pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 0, 10, 0));
      $pdf->setDefaultFont('Arial');
      $pdf->setTestTdInOnePage(false);
      $pdf->WriteHTML($html);
      $pdf->Output('download_invoice.pdf');
    }
      else
      {
        $this->session->set_flashdata('message', "<script>alert('Data tidak ditemukan');</script>");
        redirect(site_url());
      }

	}

	public function history()
	{
		$this->data['title'] 							= 'Daftar Transaksi';
		$this->data['cek_cart_history']	  = $this->Cart_model->cart_history()->row();
		$this->data['cart_history']	    	= $this->Cart_model->cart_history()->result();
		$this->data['cek']						= $this->Cart_model->pay_confirm_history()->row();
		$this->data['cek_']						= $this->Cart_model->cek_pembayaran_by_invoice();
		$this->data['cek2']						= $this->Cart_model->get_pembayaran_by_retur()->result();
		$this->data['cekcek']						= $this->Cart_model->get_trans_by_id();



		$this->load->view('front/cart/history', $this->data);
	}

	public function history_detail($id)
	{
		$this->data['title'] 								= 'Detail Riwayat Transaksi';

		$this->data['history_detail']	    	= $this->Cart_model->history_detail($id)->result();
		$this->data['history_detail_row']		= $this->Cart_model->history_detail($id)->row();
		$this->data['history_total_berat'] 	= $this->Cart_model->history_total_berat($id);
		$this->data['subtotal_history'] 		= $this->Cart_model->subtotal_history($id);

		$this->load->view('front/cart/history_detail', $this->data);
	}

	function konfirmasi_pembayaran($id){
		$this->data['title'] 								= 'Konfirmasi Pembayaran';
    $this->data['action']         = site_url('Page/konfirmasi_kirim');

		$this->data['history_detail']	    	= $this->Cart_model->history_detail($id)->result();
		$this->data['history_detail_row']		= $this->Cart_model->get_invoice_by_id_trans($id)->row();
		$this->data['featured_data'] 			= $this->Featured_model->get_all_front();
		$this->data['blog_data'] 					= $this->Blog_model->get_all_sidebar();
		$this->load->view('front/page/konfirmasi_pembayaran', $this->data);
	}

	function edit_konfirmasi_pembayaran($id){
		$this->data['title'] 								= 'edit Konfirmasi Pembayaran';
    $this->data['action']         = site_url('Cart/edit_konfirmasi_pembayaran_act');
		$this->data['data']		= $this->Cart_model->get_pemb($id)->row();
		$this->data['featured_data'] 			= $this->Featured_model->get_all_front();
		$this->data['blog_data'] 					= $this->Blog_model->get_all_sidebar();
		$this->load->view('front/page/edit_konfirmasi_pembayaran', $this->data);
	}

	function edit_konfirmasi_pembayaran_act(){
		$id = $this->session->userdata('id_pembayaran');
		if(!empty($_FILES['gambar']['name'])){
			$gambar = $_FILES['gambar']['name'];
			$config['upload_path']='./assets/images/buktibayar/';
	    $config['allowed_types']='jpg|gif|png';
	    $this->load->library('upload',$config);
	      if (!$this->upload->do_upload('gambar')) {
	          echo "Download Gagal"; die();
	      }else{
	          $gambar=$this->upload->data('file_name');
	      }
			$data = array(
				'no_invoice' 				=> $this->input->post('invoice'),
				'nama_pengirim' 		=> $this->input->post('nama'),
				'jumlah' 						=> $this->input->post('jumlah'),
				'bank_asal' 				=> $this->input->post('bank_asal'),
				'bank_tujuan' 			=> $this->input->post('bank_tujuan'),
				'bukti_pembayaran'  => $gambar,
				'status_pembayaran' => 'Belum Diterima',
			);

			$sql =	$this->Penjualan_model->update($data,'pembayaran','id_pembayaran',$id);
			if($sql){
				echo "<script type='text/javascript'>alert('Konfirmasi pembayaran berhasil di edit'); </script>";
				redirect(site_url('Cart/history'));
			}else{
				$this->session->set_flashdata('message', '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert">Konfirmasi pembayaran gagal di edit</div></div></div>');
				redirect(site_url('konfirmasi_pembayaran'));
			}
		}else{
			$data = array(
				'no_invoice' 				=> $this->input->post('invoice'),
				'nama_pengirim' 		=> $this->input->post('nama'),
				'jumlah' 						=> $this->input->post('jumlah'),
				'bank_asal' 				=> $this->input->post('bank_asal'),
				'bank_tujuan' 			=> $this->input->post('bank_tujuan'),
				'status_pembayaran' => 'Belum Diterima',
			);

			$sql =	$this->Penjualan_model->update($data,'pembayaran','id_pembayaran',$id);
			if($sql){
				echo "<script type='text/javascript'>alert('Konfirmasi pembayaran berhasil di edit'); </script>";
				redirect(site_url('Cart/history'));
			}else{
				$this->session->set_flashdata('message', '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert">Konfirmasi pembayaran Gagal di edit</div></div></div>');
				redirect(site_url('konfirmasi_pembayaran'));
			}
		}
	}

	function kirimEmail($emailtujuan){
		$pass="129FAasdsk25kwBjakjDlff";
		$panjang='8';
		$len=strlen($pass);
		$start=$len-$panjang;
		$xx=rand('0',$start);
		$yy=str_shuffle($pass);
		$randomString=substr($yy, $xx, $panjang);
		$data = array(
			'forgotten_password_code' => $randomString,
		);
		//update ke kolom forgoten
		$this->Ion_auth_model->ubahpasswordUser($data,'users','email',$emailtujuan);

		$link=base_url()."/cart/download_invoice/".$randomString;
		//ekseksusi kirim email berdasarkan params
		//isi email berupa link
		$config['protocol'] = 'smtp';
          $config['smtp_host'] = 'ssl://smtp.gmail.com';
          $config['smtp_port'] = '465';
          $config['smtp_user'] = 'fskomputerbec@gmail.com';
          $config['smtp_pass'] = 'komputer100%'; //ini pake akun pass google email
          $config['mailtype'] = 'html';
          $config['charset'] = 'iso-8859-1';
          $config['wordwrap'] = 'TRUE';
          $config['newline'] = "\r\n";

          $this->load->library('email', $config);
          $this->email->initialize($config);

          $this->email->from('shorfanaiqbal98@gmail.com');
          $this->email->to($emailtujuan);
          $this->email->subject('Reset Password');
          $this->email->message('Silahkan klik ini berikut untuk reset password '.$link);
          $this->email->set_mailtype('html');
          $this->email->send();
	}

	public function kurirdata()
	{
		$this->load->library('rajaongkir');
		$tujuan	= $this->input->get('kota');
		$dari		= '22';
		$berat	= $this->input->get('berat');
		$kurir	= $this->input->get('kurir');
		$dc			= $this->rajaongkir->cost($dari,$tujuan,$berat,$kurir);
		$data		= json_decode($dc,TRUE);
		$o			= '';

		if(!empty($data['rajaongkir']['results']))
		{
			$data['data']=$data['rajaongkir']['results'][0]['costs'];
			$this->load->view('front/cart/datakurir',$data);
		}
	}

}
