<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
  {
    parent::__construct();
		$this->load->helper('berat_helper');
		$this->load->model('Blog_model');
		$this->load->model('Cart_model');
		$this->load->model('Company_model');
		$this->load->model('Kategori_model');
		$this->load->model('Kontak_model');
		$this->load->model('Produk_model');
		$this->load->model('Featured_model');
		$this->load->model('Kategori_model');
		$this->load->model('Kontak_model');
    $this->load->model('Produk_model');
		$this->load->model('Slider_model');

  }

	public function index()
	{
		$this->load->model('Blog_model');
		$this->load->model('Cart_model');
		$this->load->model('Company_model');
		$this->load->model('Kategori_model');
		$this->load->model('Kontak_model');
		$this->load->model('Produk_model');
		$this->load->model('Slider_model');

		$this->data['title'] 							= 'FS Komputer';

		$this->data['company_data'] 			= $this->Company_model->get_by_company();
		$this->data['produk_new_data'] 		= $this->Produk_model->get_all_new_home();
		$this->data['blog_new_data'] 			= $this->Blog_model->get_all_new_home();
		$this->data['slider_data'] 				= $this->Slider_model->get_all_home();
		$this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
		$this->data['bandingan'] 	= $this->Produk_model->bandingan();

		$this->load->view('front/home/body', $this->data);
	}

	function simulasi_produk(){
		$this->data['title'] 							= 'Simulasi Perakitan Komputer';
		$this->data['blog_data'] 					= $this->Blog_model->get_all_sidebar();
		$this->data['company_data'] 			= $this->Company_model->get_by_company();
		$this->data['featured_data'] 			= $this->Featured_model->get_all_front();
		$this->data['kategori_data'] 			= $this->Kategori_model->get_all();
		$this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
		$this->data['company_data'] 			= $this->Company_model->get_by_company();
		$this->data['produk_new_data'] 		= $this->Produk_model->az	();
		$this->data['blog_new_data'] 			= $this->Blog_model->get_all_new_home();
		$this->data['slider_data'] 				= $this->Slider_model->get_all_home();
		$this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
		$this->data['bandingan'] 	= $this->Produk_model->bandingan();
		$this->load->view('front/produk/simulasi_produk', $this->data);


	}



	function sortingAZ(){

		$this->data['title'] 							= 'FS Komputer';

		$this->data['company_data'] 			= $this->Company_model->get_by_company();
		$this->data['produk_new_data'] 		= $this->Produk_model->az	();
		$this->data['blog_new_data'] 			= $this->Blog_model->get_all_new_home();
		$this->data['slider_data'] 				= $this->Slider_model->get_all_home();
		$this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
		$this->data['bandingan'] 	= $this->Produk_model->bandingan();

		$this->load->view('front/header', $this->data);
		$this->load->view('front/navbar', $this->data);
		$this->load->view('front/home/slider', $this->data);
		$this->load->view('front/modul/mod_AZ', $this->data);
		$this->load->view('front/home/blog_new', $this->data);
		$this->load->view('front/footer', $this->data);
	}

	function sortingZA(){

	$this->data['title'] 							= 'FS Komputer';

	$this->data['company_data'] 			= $this->Company_model->get_by_company();
	$this->data['produk_new_data'] 	= $this->Produk_model->za();
	$this->data['blog_new_data'] 			= $this->Blog_model->get_all_new_home();
	$this->data['slider_data'] 				= $this->Slider_model->get_all_home();
	$this->data['kontak'] 						= $this->Kontak_model->get_all();
	$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
	$this->data['bandingan'] 	= $this->Produk_model->bandingan();
	$this->load->view('front/header', $this->data);
	$this->load->view('front/navbar', $this->data);
	$this->load->view('front/home/slider', $this->data);
	$this->load->view('front/modul/mod_ZA', $this->data);
	$this->load->view('front/home/blog_new', $this->data);
	$this->load->view('front/footer', $this->data);
	}

	function harga_terendah(){

	$this->data['title'] 							= 'FS Komputer';

	$this->data['company_data'] 			= $this->Company_model->get_by_company();
	$this->data['produk_new_data'] 		= $this->Produk_model->harga_terendah();
	$this->data['blog_new_data'] 			= $this->Blog_model->get_all_new_home();
	$this->data['slider_data'] 				= $this->Slider_model->get_all_home();
	$this->data['kontak'] 						= $this->Kontak_model->get_all();
	$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
	$this->data['bandingan'] 	= $this->Produk_model->bandingan();
	$this->load->view('front/header', $this->data);
	$this->load->view('front/navbar', $this->data);
	$this->load->view('front/home/slider', $this->data);
	$this->load->view('front/modul/mod_termurah', $this->data);
	$this->load->view('front/home/blog_new', $this->data);
	$this->load->view('front/footer', $this->data);
	}

	function harga_tertinggi(){

	$this->data['title'] 							= 'FS Komputer';

	$this->data['company_data'] 			= $this->Company_model->get_by_company();
	$this->data['produk_new_data'] 		= $this->Produk_model->harga_tertinggi();
	$this->data['blog_new_data'] 			= $this->Blog_model->get_all_new_home();
	$this->data['slider_data'] 				= $this->Slider_model->get_all_home();
	$this->data['kontak'] 						= $this->Kontak_model->get_all();
	$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
	$this->data['bandingan'] 	= $this->Produk_model->bandingan();
	$this->load->view('front/header', $this->data);
	$this->load->view('front/navbar', $this->data);
	$this->load->view('front/home/slider', $this->data);
	$this->load->view('front/modul/mod_termahal', $this->data);
	$this->load->view('front/home/blog_new', $this->data);
	$this->load->view('front/footer', $this->data);
	}



}
