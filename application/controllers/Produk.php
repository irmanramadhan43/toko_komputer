<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller
{
	function __construct()
  {
    parent::__construct();

    /* memanggil model untuk ditampilkan pada masing2 modul */
		$this->load->model('Blog_model');
		$this->load->model('Cart_model');
		$this->load->model('Company_model');
		$this->load->model('Featured_model');
		$this->load->model('Kategori_model');
		$this->load->model('Kontak_model');
    $this->load->model('Produk_model');
		$this->load->model('Slider_model');
		$this->load->model('Penjualan_model');

    /* memanggil function dari masing2 model yang akan digunakan */
		$this->data['blog_data'] 					= $this->Blog_model->get_all_sidebar();
		$this->data['company_data'] 			= $this->Company_model->get_by_company();
		$this->data['featured_data'] 			= $this->Featured_model->get_all_front();
		$this->data['kategori_data'] 			= $this->Kategori_model->get_all();
		$this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
  }

	public function read($id)
	{
    /* mengambil data berdasarkan id */
		$row = $this->Produk_model->get_by_id_front($id);

    /* melakukan pengecekan data, apabila ada maka akan ditampilkan */
		if ($row)
    {
      /* memanggil function dari masing2 model yang akan digunakan */
    	$this->data['produk']       	= $this->Produk_model->get_by_id_front($id);
			$this->data['produk_lainnya']	= $this->Produk_model->get_random();
			$this->data['produk_new_data'] 		= $this->Produk_model->get_all_new_home();
			$this->data['featured_data'] 			= $this->Featured_model->get_all_front();


			$this->data['title'] = $row->judul_produk;

      /* memanggil view yang telah disiapkan dan passing data dari model ke view*/
			$this->load->view('front/produk/body', $this->data);
		}
			else
	    {
				echo "<script>alert('Produk tidak ditemukan');location.replace('".base_url()."')</script>";
	    }
	}


	public function cari_produk()
  {
    /* menyiapkan data yang akan disertakan/ ditampilkan pada view */
  	$this->data['title'] = 'Hasil Pencarian Anda';

    /* memanggil function dari model yang akan digunakan */
    $this->data['hasil_pencarian'] = $this->Produk_model->get_cari_produk();

    /* memanggil view yang telah disiapkan dan passing data dari model ke view*/
    $this->load->view('front/produk/hasil_pencarian', $this->data);
  }


	function bandingkan($id){
		$data = array(
			'bandingkan' => 'ya',
		);

		$this->data['bandingan'] 	= $this->Produk_model->bandingan();
		if($this->data['bandingan']->banding >= 4){
			$this->session->set_flashdata('message', '<div class="alert alert-success alert">Anda Sudah Menambahkan 4 Barang</div>');
			redirect(site_url());
		}else{
			$sql = $this->Produk_model->bandingkan($data,'produk','id_produk',$id);
			if ($sql) {
				$this->session->set_flashdata('message', '<div class="alert alert-success alert">Berhasil Membandingkan</div>');
				redirect(site_url());
			}else{
				redirect(site_url());
			}
		}


	}

	function hapus_banding($id){
		$data = array(
			'bandingkan' => null,
		);

		$sql = $this->Produk_model->bandingkan($data,'produk','id_produk',$id);
		if ($sql) {
			echo "<script type='text/javascript'>alert('Berhasil Menghapus Bandingkan'); document.location='http://localhost/kl_skripsi/Produk/banding' </script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal Menghapus'); document.location='http://localhost/kl_skripsi/Produk/banding' </script>";
		}
	}

	public function banding(){
		$this->data['title'] 							= 'FS Komputer';
		$this->data['company_data'] 			= $this->Company_model->get_by_company();
		$this->data['produk_new_data'] 		= $this->Produk_model->banding();
		$this->data['blog_new_data'] 			= $this->Blog_model->get_all_new_home();
		$this->data['slider_data'] 				= $this->Slider_model->get_all_home();
		$this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
		$this->data['bandingan'] 	= $this->Produk_model->bandingan();
		$this->load->view('front/header', $this->data);
		$this->load->view('front/navbar', $this->data);
		$this->load->view('front/home/slider', $this->data);
		$this->load->view('front/modul/bandingkan', $this->data);
		$this->load->view('front/home/blog_new', $this->data);
		$this->load->view('front/footer', $this->data);
	}

	function simulasi_komputer(){
		$this->data['title'] 									= 'Simulasi Perakitan Komputer';
		$this->data['kontak'] 								= $this->Kontak_model->get_all();
    $this->data['action']                 = site_url('Produk/simulasi_komputer_act');
		$this->data['total_cart_navbar'] 			= $this->Cart_model->total_cart_navbar();
		$this->data['bandingan'] 							= $this->Produk_model->bandingan();
		$this->data['simulasi'] 							= $this->Produk_model->simulasi_komputer();
    $this->data['produk']                 = $this->Penjualan_model->get_produk();
    $this->data['kategori']               = $this->Penjualan_model->get_kategori();
    $this->data['subkat']                 = $this->Penjualan_model->get_subkat();
    $this->data['super_subkat']           = $this->Penjualan_model->get_super_subkat();
    $this->data['kategori_selected']      = '';
    $this->data['subkat_selected']        = '';
    $this->data['super_subkat_selected']  = '';
    $this->data['produk_selected']        = '';
    $this->data['harga_selected']         = '';
    $this->data['berat_selected']      	  = '';
		$this->load->view('front/produk/simulasi_produk', $this->data);
	}

	function simulasi_komputer_act(){
		ob_start();
		$barang = $this->input->post('nama_barang');
		$qty = $this->input->post('qty');
		$totalHarga=0;
		$panjangArrayBarang=sizeof($barang);
		$this->data['barang'] = $barang;
		$this->data['qty'] = $qty;
		$this->data['panjangArrayBarang'] = $panjangArrayBarang;
		for ($i=0; $i <$panjangArrayBarang ; $i++) {
			if($barang[$i]!=''){
				$exBarang=explode("#",$barang[$i]);
				$harga=$exBarang[1];
				$harga=$qty[$i]*$harga;
				$totalHarga=$totalHarga+$harga;
			}
		}
		$this->data['totalHarga'] = $totalHarga;
		$this->load->view('front/produk/hasil_simulasi',$this->data);

    $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Pembelian All.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
	}



	public function hasil_simulasi()
	{
		ob_start();
    $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Pembelian All.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }

	}

	function kasih_rating($id){
		$this->data['produk'] 						= $this->Produk_model->get_produk_rating($id);
		$this->load->view('front/produk/rating', $this->data);
	}

	function kasih_rating_act(){
		$data = array(
			'id_produk' => $this->input->post('id_produk'),
			'id_user' 	=> $this->session->userdata('user_id'),
			'tingkat_rating'		=> $this->input->post('rate'),
			'komentar' => $this->input->post('komentar'),
		);
		$sql = $this->Produk_model->insert_rating($data,'rating');
		if ($sql) {
				echo "<script type='text/javascript'>alert('Rating Anda Disimpan'); document.location='http://localhost/kl_skripsi/' </script>";
		}else {
				echo "<script type='text/javascript'>alert('Rating Anda Gagal Disimpan'); document.location='http://localhost/kl_skripsi/' </script>";
		}

	}



  public function katalog()
  {
    /* menyiapkan data yang akan disertakan/ ditampilkan pada view */
    $this->data['title'] = "Katalog Produk";

    /* memanggil library pagination (membuat halaman) */
    $this->load->library('pagination');

    /* menghitung jumlah total data */
    $jumlah = $this->Produk_model->total_rows();

    // Mengatur base_url
    $config['base_url'] = base_url().'produk/katalog/halaman/';
    //menghitung total baris
    $config['total_rows'] = $jumlah;
    //mengatur total data yang tampil per halamannya
    $config['per_page'] = 9;
    // tag pagination bootstrap

		$config['full_tag_open'] 		= '<nav><ul class="pagination">';
		$config['full_tag_close'] 	= '</ul></nav>';
		$config['num_tag_open'] 		= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 		= '</span></li>';
		$config['cur_tag_open'] 		= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 		= '<span class="sr-only">(current)</span></span></li>';
		$config['next_link']        = "Selanjutnya";
		$config['next_tag_open'] 		= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_link']        = "Sebelumnya";
		$config['prev_tag_open'] 		= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_link']       = "Awal";
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_link']        = 'Terakhir';
		$config['last_tag_open'] 		= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';

    // mengambil uri segment ke-4
    $dari = $this->uri->segment('4');

    /* eksekusi library pagination ke model penampilan data */
    $this->data['katalog_data'] = $this->Produk_model->get_all_katalog($config['per_page'],$dari);

    $this->pagination->initialize($config);

    /* memanggil view yang telah disiapkan dan passing data dari model ke view*/
    $this->load->view('front/produk/katalog', $this->data);
  }

}
