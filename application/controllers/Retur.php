<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur extends CI_Controller {

	function __construct()
  {
    parent::__construct();
		$this->load->helper('berat_helper');

		$this->load->model('Bank_model');
		$this->load->model('Cart_model');
    $this->load->model('Company_model');
    $this->load->model('Kontak_model');
		$this->load->model('Produk_model');
		$this->load->model('Featured_model');
		$this->load->model('Blog_model');
		$this->load->model('Retur_model');

		$this->data['company_data'] 			= $this->Company_model->get_by_company();
    $this->data['kontak'] 						= $this->Kontak_model->get_all();
		$this->data['total_cart_navbar'] 	= $this->Cart_model->total_cart_navbar();
  }

	public function index()
	{
		$this->data['title'] 										= 'Keranjang Belanja';
		$this->data['retur_row'] 			    			= $this->Retur_model->get_retur_by_user_id()->row();
		// ambil data keranjang
		$this->data['retur'] 			    			= $this->Retur_model->get_retur_by_user_id()->result();

    $this->load->view('front/retur/retur_list', $this->data);
  }

  public function form_retur($id){
    $this->data['title'] 								= 'Form Retur';
    $this->data['action']         = site_url('Retur/form_retur_act');
		$this->data['history_detail_row']		= $this->Cart_model->get_invoice_by_id_trans($id)->row();
		$this->data['barang']		= $this->Retur_model->get_produk_by_id($id);
		$this->load->view('front/retur/form_retur', $this->data);
  }

  public function form_retur_act(){
    $data = array(
      'user_id'     => $this->input->post('user_id'),
      'pesan'       => $this->input->post('pesan'),
      'tanggal'     => $this->input->post('tanggal'),
      'no_resi'     => $this->input->post('no_resi'),
      'no_invoice'  => $this->input->post('invoice'),
      'status_customer' => 'terkirim ke toko',
			'status_admin' => 'Belum Retur',
    );
		$sql = $this->Retur_model->insert($data,'retur');
		if ($sql) {
			$barang = $this->input->post('nama_barang');
			$id_return = $this->get_id();
			foreach ($barang as $k) {
				$data_barang = array(
					'id_retur' => $id_return,
					'nama_barang' => $k,
				);
				$sql2 = $this->Retur_model->insert($data_barang,'retur_detail');
			}

			if ($sql2) {
				$this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil dibuat</div>');
				redirect(site_url('Retur'));
			}else{
				$this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
				redirect(site_url('Retur'));
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
			redirect(site_url('Retur'));
		}


  }

	function get_id()   {
		$this->db->select('id_retur', FALSE);
		$this->db->order_by('id_retur','DESC');
		$this->db->limit(1);
		$query = $this->db->get('retur');      //cek dulu apakah  sudah ada no di tabel.
			if($query->num_rows() <> 0){
			 //jika no ternyata sudah ada.
			 $data = $query->row();
			 $kode = intval($data->id_retur);
			}
			else{
			 //jika no belum ada
			 $kode = 1;
			}
		$kodemax = str_pad($kode, 1, STR_PAD_LEFT);
		$kodejadi = $kodemax;
		return $kodejadi;
 }

	public function kurirdata()
	{
		$this->load->library('rajaongkir');
		$tujuan	= $this->input->get('kota');
		$dari		= '22';
		$berat	= $this->input->get('berat');
		$kurir	= $this->input->get('kurir');
		$dc			= $this->rajaongkir->cost($dari,$tujuan,$berat,$kurir);
		$data		= json_decode($dc,TRUE);
		$o			= '';

		if(!empty($data['rajaongkir']['results']))
		{
			$data['data']=$data['rajaongkir']['results'][0]['costs'];
			$this->load->view('front/cart/datakurir',$data);
		}
	}

}
