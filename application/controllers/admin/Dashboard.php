<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	// function __construct()
  // {
  //   parent::__construct();
	//
  //   if ($this->session->userdata('usertype') == '' or $this->session->userdata('usertype') == '3' or $this->session->userdata('usertype') == '5' ){
	// 		redirect('admin/auth/login', 'refresh');
	// 	}
  //   // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
  // }

	public function index()
	{
		$this->load->model('Blog_model');
		$this->load->model('Cart_model');
		$this->load->model('Featured_model');
		$this->load->model('Ion_auth_model');
		$this->load->model('Kontak_model');
		$this->load->model('Produk_model');
    $this->load->model('Kategori_model');
		$this->load->model('Subkategori_model');
		$this->load->model('Supersubkategori_model');
		$this->load->model('Slider_model');
    	$this->load->helper('url');

			if ($this->session->userdata('usertype') == '3'){

			}else if($this->session->userdata('usertype') == '1' or $this->session->userdata('usertype') == '2' or $this->session->userdata('usertype') == '4' or $this->session->userdata('usertype') == '6'){
				$this->data = array(
		      'title' 									=> 'Dashboard',
					'get_bulan' 							=> $this->Cart_model->get_bulan(),
					'total_transaksi' 				=> $this->Cart_model->total_rows(),
					'top5_transaksi' 					=> $this->Cart_model->top5_transaksi(),
					'total_featured' 					=> $this->Featured_model->total_rows(),
					'total_produk' 						=> $this->Produk_model->total_rows(),
					'total_slider' 						=> $this->Slider_model->total_rows(),
				);

				$this->load->view('back/dashboard',$this->data);
			}
		else
		{
			redirect('admin/auth/login', 'refresh');
		}
	}

}
