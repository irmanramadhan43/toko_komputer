<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct()
  {
		parent::__construct();
		$this->load->model('Cart_model');
    $this->load->model('Ion_auth_model');
    $this->load->model('Company_model');
    $this->load->model('Pembelian_model');
    $this->load->model('Retur_model');

		$this->load->helper('tgl_indo');

    $this->data['module'] = 'Laporan';

    $this->data['company_data'] 			= $this->Company_model->get_by_company();

		// apabila belum login maka diarahkan ke halaman login
		if ($this->session->userdata('usertype') == '' or $this->session->userdata('usertype') == '3' or $this->session->userdata('usertype') == '5' ){
			redirect('admin/auth/login', 'refresh');
		}
	}

  public function index()
  {
    $this->data['title'] = "Laporan Penjualan";
    $this->load->view('back/laporan/body_penjualan',$this->data);
  }

  public function export_all()
  {
		ob_start();
    $this->data['get_all']    = $this->Cart_model->get_all_laporan();
    $this->load->view('back/laporan/print_all_penjualan',$this->data);
		$html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Penjualan All.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
  }

  public function export_periode()
  {
    ob_start();
    $this->data['get_periode']        = $this->Cart_model->get_data_penjualan_periode();
    $this->data['hitung']   = $this->Cart_model->hitung_penjualan();

    $this->load->view('back/laporan/print_periode_penjualan',$this->data);
		$html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Penjualan Periode.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
  }


	function laporan_pembelian(){
		$this->data['title'] = "Laporan Pembelian";
    $this->load->view('back/laporan/body_pembelian',$this->data);
	}

	function export_all_pembelian(){
    ob_start();
		$this->data['get_all']    = $this->Pembelian_model->get_all_pembelian();

    $this->load->view('back/laporan/print_all_pembelian',$this->data);
    $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Pembelian All.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
  }

	// public function export_all_pembelian()
	// {
	// 	$this->data['get_all']    = $this->Pembelian_model->get_all_pembelian();
	// 	$this->load->view('back/laporan/print_all_pembelian',$this->data);
	// }

	public function export_periode_pembelian()
	{
    ob_start();
		$this->data['get_periode']        = $this->Pembelian_model->get_data_pembelian_periode();

		$this->load->view('back/laporan/print_periode_pembelian',$this->data);
    $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Pembelian Periode.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
	}

	function laporan_retur(){
		$this->data['title'] = "Laporan Retur";
    $this->load->view('back/laporan/body_retur',$this->data);
	}

	public function export_all_retur()
	{
		ob_start();
		$this->data['get_all']    = $this->Retur_model->get_all_retur_laporan();
		$this->load->view('back/laporan/print_all_retur',$this->data);
		$html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Retur All.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }

	}

	public function export_periode_retur()
	{
		ob_start();
		$this->data['get_periode']        = $this->Retur_model->get_data_retur_periode();

		$this->load->view('back/laporan/print_periode_retur',$this->data);
		$html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Retur Periode.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
	}

	function laporan_stok_barang(){
		$this->data['title'] = "Laporan Stok Barang";
    $this->load->view('back/laporan/body_stok_barang',$this->data);
	}

	public function export_all_stok_barang()
  {
		ob_start();
    $this->data['get_all']    = $this->Cart_model->get_all_stok_barang();
    $this->load->view('back/laporan/print_all_stok_barang',$this->data);
		$html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Stok Barang All.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
  }

  public function export_periode_stok_barang()
  {
		ob_start();
    $this->data['get_periode']        = $this->Cart_model->get_data_stok_barang_periode();

    $this->load->view('back/laporan/print_periode_stok_barang',$this->data);
		$html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Laporan Stok Barang Periode.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
  }

}
