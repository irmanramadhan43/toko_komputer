<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pembelian extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Pembelian_model');

    $this->data['module'] = 'pembelian';

    if ($this->session->userdata('usertype') == '' or $this->session->userdata('usertype') == '3' or $this->session->userdata('usertype') == '5' ){
			redirect('admin/auth/login', 'refresh');
		}
    // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
  }

  public function index()
  {
    $this->data['title'] = 'Data '.$this->data['module'];
    $this->data['pembelian'] = $this->Pembelian_model->get_data_pembelian();
    $this->load->view('back/Pembelian/data_pemesanan', $this->data);
  }

  public function data_penerimaan(){
    $this->data['title'] = 'Data Penerimaan';
    $this->data['penerimaan'] = $this->Pembelian_model->get_data_penerimaan();
    $this->load->view('back/Pembelian/data_penerimaan', $this->data);
  }


  public function form_pemesanan()
  {
    $this->data['title']          = 'Form Pemesanan ';
    $this->data['action']         = site_url('admin/Pembelian/create_pemesanan');
    $this->data['button_submit']  = 'Simpan';
    $this->data['button_reset']   = 'Reset';
    $this->data['produk']                 = $this->Pembelian_model->get_produk_();
    $this->data['kategori']               = $this->Pembelian_model->get_kategori();
    $this->data['subkat']                 = $this->Pembelian_model->get_subkat();
    $this->data['super_subkat']           = $this->Pembelian_model->get_super_subkat();
    $this->data['kategori_selected']      = '';
    $this->data['subkat_selected']        = '';
    $this->data['super_subkat_selected']  = '';
    $this->data['produk_selected']        = '';
    $this->data['supplier']       = $this->Pembelian_model->get_supplier();
    // $this->data['produk']       = $this->Pembelian_model->get_produk();
    $this->data['id_pembelian'] = $this->Pembelian_model->buat_kode();
    // $this->data['kategori']       = $this->Pembelian_model->get_kategori();
    // $this->data['subkategori']       = $this->Pembelian_model->get_sub_kategori();
    // $this->data['supersubkategori'] = $this->Pembelian_model->get_super_sub_kategori();
    $this->load->view('back/Pembelian/form_pemesanan', $this->data);
  }

  public function create_pemesanan()
  {
        $id = $this->input->post('id_pembelian',TRUE);
        $kode_pembelian = $this->Pembelian_model->buat_kodep();
        if($this->input->post('judul_barang') != ''){
          $data = array(
              'id_pembelian'    => $this->input->post('id_pembelian',TRUE),
              'kode_pembelian'    => $kode_pembelian,
              'tanggal_order'   => $this->input->post('tanggal_order',TRUE),
              'id_supp'         => $this->input->post('id_supp',TRUE),
              'kat_id'          => $this->input->post('kategori', TRUE),
              'subkat_id'       => $this->input->post('subkat', TRUE),
              'supersubkat_id'  => $this->input->post('supersubkat', TRUE),
              'status_beli' => 'Tunggu Persetujuan Owner'
          );
          $sql = $this->Pembelian_model->insert($data,'pembelian');
          if($sql){
            $data2 = array(
                'id_pembelian' => $this->input->post('id_pembelian',TRUE),
                'judul_produk' => $this->input->post('judul_barang',TRUE),
                'deskripsi' => $this->input->post('deskripsi',TRUE),
                'jumlah_order' => $this->input->post('jumlah',TRUE),
              );
              $sql2 = $this->Pembelian_model->insert($data2,'pembelian_detail');
              if($sql2){
                // set pesan data berhasil dibuat
                   $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
                   redirect(site_url('admin/Pembelian/rincian/'.$id));
              }
          }else{
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
             redirect(site_url('admin/Pembelian/form_pemesanan'));
          }
        }else{
          $data = array(
              'id_pembelian'    => $this->input->post('id_pembelian',TRUE),
              'kode_pembelian'    => $kode_pembelian,
              'tanggal_order'   => $this->input->post('tanggal_order',TRUE),
              'id_supp'         => $this->input->post('id_supp',TRUE),
              'kat_id'          => $this->input->post('kategori', TRUE),
              'subkat_id'       => $this->input->post('subkat', TRUE),
              'supersubkat_id'  => $this->input->post('supersubkat', TRUE),
              'status_beli' => 'Tunggu Persetujuan Owner'
          );
          $sql = $this->Pembelian_model->insert($data,'pembelian');
          if($sql){
            $data2 = array(
                'id_pembelian' => $this->input->post('id_pembelian',TRUE),
                'judul_produk' => $this->input->post('judul_produk_c',TRUE),
                'deskripsi' => $this->input->post('deskripsi',TRUE),
                'jumlah_order' => $this->input->post('jumlah',TRUE),
              );
              $sql2 = $this->Pembelian_model->insert($data2,'pembelian_detail');
              if($sql2){
                // set pesan data berhasil dibuat
                   $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
                   redirect(site_url('admin/Pembelian/rincian/'.$id));
              }
          }else{
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
             redirect(site_url('admin/Pembelian/form_pemesanan'));
          }
        }
  }

  function tambah_pemesanan($id){
    $this->data['title']            = 'Tambah Pemesanan Barang ';
    $this->data['action']           = site_url('admin/Pembelian/tambah_pemesanan_act');
    $this->data['button_submit']  = 'Tambah';
    $this->data['produk']                 = $this->Pembelian_model->get_produk_();
    $this->data['kategori']               = $this->Pembelian_model->get_kategori();
    $this->data['subkat']                 = $this->Pembelian_model->get_subkat();
    $this->data['super_subkat']           = $this->Pembelian_model->get_super_subkat();
    $this->data['kategori_selected']      = '';
    $this->data['subkat_selected']        = '';
    $this->data['super_subkat_selected']  = '';
    $this->data['produk_selected']        = '';
    $this->data['produk']       = $this->Pembelian_model->get_produk();
    $this->data['identity']         = $this->Pembelian_model->get_data_pembelian_by_id($id);
    $this->data['barang']           = $this->Pembelian_model->data_barang_dipesan($id);
    $this->load->view('back/Pembelian/tambah_pemesanan', $this->data);
  }

  function tambah_pemesanan_act(){
    $id = $this->input->post('id_pembelian',TRUE);
    if($this->input->post('judul_barang') != ''){
        $data = array(
            'id_pembelian' => $this->input->post('id_pembelian',TRUE),
            'judul_produk' => $this->input->post('judul_barang',TRUE),
            'deskripsi' => $this->input->post('deskripsi',TRUE),
            'jumlah_order' => $this->input->post('jumlah',TRUE),
          );
          $sql = $this->Pembelian_model->insert($data,'pembelian_detail');
          if($sql){
            // set pesan data berhasil dibuat
               $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
               redirect(site_url('admin/Pembelian/rincian/'.$id));
          }else{
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
             redirect(site_url('admin/Pembelian/form_pemesanan'));
          }
    }else{
        $data = array(
            'id_pembelian' => $this->input->post('id_pembelian',TRUE),
            'judul_produk' => $this->input->post('judul_produk_c',TRUE),
            'deskripsi' => $this->input->post('deskripsi',TRUE),
            'jumlah_order' => $this->input->post('jumlah',TRUE),
          );
          $sql = $this->Pembelian_model->insert($data,'pembelian_detail');
          if($sql){
            // set pesan data berhasil dibuat
               $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
               redirect(site_url('admin/Pembelian/rincian/'.$id));
          }else{
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
             redirect(site_url('admin/Pembelian/form_pemesanan'));
          }
    }
  }

  function rincian($id){
       $this->data['title']           = 'Rincian Pemesanan Barang ';
       $this->data['identity']        = $this->Pembelian_model->get_data_pembelian_by_id($id);
       $this->data['barang']          = $this->Pembelian_model->data_barang_dipesan($id);
       $this->load->view('back/Pembelian/rincian_pembelian', $this->data);

  }

  function rincian_penerimaan_barang($id){
    $this->data['title']           = 'Rincian Penerimaan Barang ';
    $this->data['identity']        = $this->Pembelian_model->get_data_pembelian_by_id($id);
    $this->data['barang']          = $this->Pembelian_model->data_barang_dipesan($id);
    $this->load->view('back/Pembelian/rincian_terima_barang', $this->data);
  }



  function persetujuan_order($id){
       $this->data['title']           = 'Persetujuan Order Barang ';
       $this->data['action']         = site_url('admin/Pembelian/setuju_owner');
       $this->data['button_submit']  = 'Simpan';
       $this->data['button_reset']   = 'Reset';
       $this->data['identity']        = $this->Pembelian_model->get_data_pembelian_by_id($id);
       $this->data['barang']          = $this->Pembelian_model->data_barang_dipesan($id);
       $this->data['data']            = $this->Pembelian_model->get_data_pembelian_by_id_($id);
       $this->load->view('back/Pembelian/persetujuan_order', $this->data);
  }

  function setuju_owner(){
    $id = $this->input->post('id');
    $status_beli = $this->input->post('status_beli');
    $where = array(
      'status_beli' => $status_beli,
    );
    $sql = $this->Pembelian_model->update($where,'pembelian','id_pembelian',$id);
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil Disetujui</div>');
      redirect(site_url('admin/Pembelian'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil Ditolak</div>');
      redirect(site_url('admin/Pembelian'));
    }
  }

  function form_penerimaan($id){
       $this->data['title']           = 'Penerimaan Order Barang ';
       $this->data['action']          = site_url('admin/Pembelian/form_penerimaan_act_');
       $this->data['button_submit']   = 'Simpan';
       $this->data['button_reset']   = 'Reset';
       $this->data['identity']        = $this->Pembelian_model->get_data_pembelian_by_id($id);
       $this->data['barang']          = $this->Pembelian_model->data_barang_dipesan($id);
       $this->data['total_beli']          = $this->Pembelian_model->hitung_jumlah_barang($id);
       $this->data['total_pembelian']          = $this->Pembelian_model->hitung_total_beli($id);
       $this->load->view('back/Pembelian/form_penerimaan', $this->data);
  }

  function edit_barang_pemesanan($id){
    $this->data['title']           = 'Penerimaan Order Barang ';
    $this->data['action']          = site_url('admin/Pembelian/edit_barang_act');
    $this->data['button_submit']   = 'Simpan';
    $this->data['button_reset']   = 'Reset';
    $this->data['identity']        = $this->Pembelian_model->get_data_pemdet($id);
    $this->data['total_pembelian']          = $this->Pembelian_model->hitung_total_beli($id);
    $this->load->view('back/Pembelian/edit_barang_pemesanan', $this->data);
  }

  function edit_barang_act(){
    $id = $this->input->post('id_pemdet');
    $this->data['identity']        = $this->Pembelian_model->get_data_pemdet($id);
    $id_pembelian = $this->input->post('id_pembelian');
    if ($this->input->post('jumlah_datang') > $this->data['identity']->jumlah_order ){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Jumlah Datang Lebih besar dari Jumlah Order </div>');
      redirect(site_url('admin/Pembelian/form_penerimaan/'.$id_pembelian));
    } else {
      $data = array(
        'harga_barang' => $this->input->post('harga_beli'),
        'harga_jual' => $this->input->post('harga_jual'),
        'jumlah_datang' => $this->input->post('jumlah_datang'),
        'subtotal_beli' => $this->input->post('jumlah_datang') * $this->input->post('harga_beli'),
      );
      $sql = $this->Pembelian_model->update($data,'pembelian_detail','id_pemdet',$id);
      if($sql){
        $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data  Berhasil diedit</div>');
        redirect(site_url('admin/Pembelian/form_penerimaan/'.$id_pembelian));
      }else{
        $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data  Gagal diedit</div>');
        redirect(site_url('admin/Pembelian/form_penerimaan/'.$id_pembelian));
      }
    }


  }

  function form_penerimaan_act_(){
    $id = $this->input->post('id_pembelian');
    $this->data['data']          = $this->Pembelian_model->get_dat_pembelian($id);
    $dat = $this->data['data'];
    foreach ($dat as $d ) {
      $judul_pembelian = $d->judul_produk;
      $where = array(
        'judul_produk' => $judul_pembelian,
      );
      $judul_produk = $this->Pembelian_model->cek_judul("produk",$where)->num_rows()>0;
      if($judul_pembelian == $judul_produk){
        $j = $this->Pembelian_model->get_id_produk($judul_pembelian);
        $data = array(
          'stok'            => $d->jumlah_datang + $j->stok,
        );
        $sql = $this->Pembelian_model->update($data,'produk','id_produk',$j->id_produk);
      }else{
        $data = array(
          'judul_produk'    => $d->judul_produk,
          'slug_produk'     => $d->judul_produk,
          'deskripsi'       => $d->deskripsi,
          'kat_id'          => $d->kat_id,
          'subkat_id'       => $d->subkat_id,
          'supersubkat_id'  => $d->supersubkat_id,
          'harga_normal'    => $d->harga_jual,
          'stok'            => $d->jumlah_datang,
        );
        $sql = $this->Pembelian_model->insert($data,'produk');
      }
    }

    if($sql){
      $data2 = array(
        'tanggal_datang' => $this->input->post('tanggal_datang'),
        'total_pembelian' => $this->input->post('subtotal_beli'),
        'status_beli' => $this->input->post('status'),
      );
      $sql2 = $this->Pembelian_model->update($data2,'pembelian','id_pembelian',$id);
      if($sql2){
        $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil Diterima</div>');
        redirect(site_url('admin/Pembelian/data_penerimaan'));
      }else{
        $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal Diterima</div>');
        redirect(site_url('admin/Pembelian/data_penerimaan'));
      }
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal Diterima</div>');
      redirect(site_url('admin/Pembelian'));
    }

  }

  function form_penerimaan_act(){
    $id = $this->input->post('id_pembelian');
    $this->data['data']          = $this->Pembelian_model->get_dat_pembelian($id);
    $dat = $this->data['data'];
      foreach ($dat as $k) {
        $data2 = array(
            'judul_produk'    => $k->judul_produk,
            'slug_produk'     => $k->judul_produk,
            'deskripsi'       => $k->deskripsi,
            'kat_id'          => $k->kat_id,
            'subkat_id'       => $k->subkat_id,
            'supersubkat_id'  => $k->supersubkat_id,
            'stok'            => $k->jumlah_datang,
        );

       $sql2 = $this->Pembelian_model->insert($data2,'produk');
      }

        if($sql2){
          $data = array(
            'tanggal_datang' => $this->input->post('tanggal_datang'),
            'total_pembelian' => $this->input->post('subtotal_beli'),
            'status_beli' => $this->input->post('status'),
          );
          $data_trans = array(
            'jumlah_datang' => $this->input->post('jumlah_datang'),
            'subtotal_beli' => $this->input->post('subtotal_beli'),
          );
          $sql = $this->Pembelian_model->update($data,'pembelian','id_pembelian',$id);
          $sql3 = $this->Pembelian_model->update($data_trans,'pembelian_detail','id_pembelian',$id);
          if ($sql && $sql3) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data  Berhasil '.$this->input->post('status').'</div>');
            redirect(site_url('admin/Pembelian/data_penerimaan'));
          }else {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data  Gagal '.$this->input->post('status').'</div>');
            redirect(site_url('admin/Pembelian/data_penerimaan'));
          }
        }else{
          $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data  Gagal '.$this->input->post('status').'</div>');
          redirect(site_url('admin/Pembelian/data_penerimaan'));
        }

  }


  public function batal_pembelian($id){
    $data = array(
      'status_beli' => 'Batal',
    );
    $sql = $this->Pembelian_model->update($data,'pembelian','id_pembelian',$id);
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data  Berhasil Dibatalkan</div>');
      redirect(site_url('admin/Pembelian'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data  Gagal Dibatalkan</div>');
      redirect(site_url('admin/Pembelian'));
    }
  }




  function notapo($id){
    ob_start();
    $this->data['identity']        = $this->Pembelian_model->get_data_pembelian_by_id($id);
    $this->data['barang']          = $this->Pembelian_model->data_barang_dipesan($id);

    $this->load->view('back/pembelian/notapo',$this->data);
    $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Convert1.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
  }

  public function _rules()
  {
    $this->form_validation->set_rules('judul_kategori', 'Judul Kategori', 'trim|required');

    // set pesan form validasi error
    $this->form_validation->set_message('required', '{field} wajib diisi');

    $this->form_validation->set_rules('id_kategori', 'id_kategori', 'trim');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
  }

}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-17 02:19:21 */
/* http://harviacode.com */
