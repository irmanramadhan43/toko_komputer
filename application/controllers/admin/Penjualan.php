<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Penjualan extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('berat_helper');

    $this->load->model('Cart_model');
    $this->load->model('Company_model');
    $this->load->model('Penjualan_model');

    $this->data['module'] = 'Penjualan';

    $this->data['company_data'] 			= $this->Company_model->get_by_company();

    if ($this->session->userdata('usertype') == '' or $this->session->userdata('usertype') == '3' or $this->session->userdata('usertype') == '5' ){
			redirect('admin/auth/login', 'refresh');
		}
  }

  public function index()
  {
    $this->data['title']            = 'Data '.$this->data['module'];
    $this->data['penjualan_data']   = $this->Cart_model->get_all();
    $this->data['data']   = $this->Penjualan_model->get_penjualan();
    $this->data['id']           = $this->Penjualan_model->get_pembayaran();

    $this->load->view('back/penjualan/penjualan_list', $this->data);
  }

  public function view($id)
  {
    $row      = $this->Cart_model->get_by_id($id);
    $invoice = $row->id_trans;

    $this->data['penjualan'] = $this->Cart_model->get_by_id($id);

    if($row)
    {
      $this->data['title']              = 'Detail '.$this->data['module'];

      // ambil data keranjang
  		$this->data['cart_data'] 			    			= $this->Cart_model->get_cart_per_customer_finished_back($invoice);
  		// ambil total_berat_dan_subtotal
  		$this->data['total_berat_dan_subtotal'] = $this->Cart_model->get_total_berat_dan_subtotal_finished_back($invoice);
      $this->data['customer_data'] 			    	= $this->Cart_model->get_data_customer_back($invoice);

      $this->load->view('back/penjualan/penjualan_detail', $this->data);
    }
      else
      {
        $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Data tidak ditemukan</div>');
        redirect(site_url('admin/penjualan'));
      }
  }

  public function form_penjualan_POS(){
    $this->data['title']                  = 'Form Point Of Sale ';
    $this->data['action']                 = site_url('admin/Penjualan/create_pembelian_act');
    $this->data['button_submit']          = 'Simpan';
    $this->data['button_reset']           = 'Reset';
    $this->data['produk']                 = $this->Penjualan_model->get_produk();
    $this->data['kategori']               = $this->Penjualan_model->get_kategori();
    $this->data['subkat']                 = $this->Penjualan_model->get_subkat();
    $this->data['super_subkat']           = $this->Penjualan_model->get_super_subkat();
    $this->data['kategori_selected']      = '';
    $this->data['subkat_selected']        = '';
    $this->data['super_subkat_selected']  = '';
    $this->data['produk_selected']        = '';
    $this->data['harga_selected']         = '';
    $this->data['berat_selected']        = '';
    $this->data['id_trans']               = $this->Penjualan_model->buat_kode();

    $this->load->view('back/penjualan/form_penjualan_sop', $this->data);
  }

  function tambah_barang_penjualan($id){
    $this->data['title']                  = 'Form Point Of Sale ';
    $this->data['action']                 = site_url('admin/Penjualan/create_pembelian_act');
    $this->data['button_submit']          = 'Simpan';
    $this->data['button_reset']           = 'Reset';
    $this->data['produk']                 = $this->Penjualan_model->get_produk();
    $this->data['kategori']               = $this->Penjualan_model->get_kategori();
    $this->data['subkat']                 = $this->Penjualan_model->get_subkat();
    $this->data['super_subkat']           = $this->Penjualan_model->get_super_subkat();
    $this->data['kategori_selected']      = '';
    $this->data['subkat_selected']        = '';
    $this->data['super_subkat_selected']  = '';
    $this->data['produk_selected']        = '';
    $this->data['harga_selected']         = '';
    $this->data['berat_selected']        = '';
    $this->data['data_penjualan']         = $this->Penjualan_model->get_penjualan_by_id($id);

    $this->load->view('back/penjualan/rincian_penjualan', $this->data);

  }

  public function rincian($id){
    $row      = $this->Cart_model->get_by_id($id);
    $invoice = $row->id_trans;
    $this->data['title']                = 'Rincian Barang Penjualan';
    $this->data['trans']                = $this->Penjualan_model->get_trans_by_id($id);
    $this->data['trans_det']            = $this->Penjualan_model->get_trans_det_by_id($id);
    $this->data['cart_data'] 			    			= $this->Cart_model->get_cart_per_customer_finished_back($invoice);
    // ambil total_berat_dan_subtotal
    $this->data['total_berat_dan_subtotal'] = $this->Cart_model->get_total_berat_dan_subtotal_finished_back($invoice);
    $this->data['customer_data'] 			    	= $this->Cart_model->get_data_customer_back($invoice);
    $this->load->view('back/penjualan/rincian_pos', $this->data);
  }

  public function data_penjualan_pos(){
    $this->data['title']            = 'Data Point Of Sale';
    $this->data['produk']           = $this->Penjualan_model->get_produk_pos();
    $this->load->view('back/penjualan/data_penjualan_pos', $this->data);
  }

  public function rincian_penjualan($id){
    $row      = $this->Cart_model->get_by_id($id);
    $invoice = $row->id_trans;
    $this->data['title']                = 'Rincian Barang Penjualan';
    $this->data['trans']                = $this->Penjualan_model->get_trans_by_id($id);
    $this->data['trans_det']            = $this->Penjualan_model->get_trans_det_by_id($id);
    $this->data['cart_data'] 			    			= $this->Cart_model->get_cart_per_customer_finished_back($invoice);
    // ambil total_berat_dan_subtotal
    $this->data['total_berat_dan_subtotal'] = $this->Cart_model->get_total_berat_dan_subtotal_finished_back($invoice);
    $this->data['customer_data'] 			    	= $this->Cart_model->get_data_customer_back($invoice);
    $this->load->view('back/penjualan/rincian_penjualan', $this->data);
  }

  public function tambah_barang($id){
    $this->data['title']                   = 'Tambah Barang';
    $this->data['action']                 = site_url('admin/Penjualan/tambah_barang_act');
    $this->data['button_submit']          = 'Simpan';
    $this->data['button_reset']           = 'Reset';
    $this->data['produk']                 = $this->Penjualan_model->get_produk();
    $this->data['kategori']               = $this->Penjualan_model->get_kategori();
    $this->data['subkat']                 = $this->Penjualan_model->get_subkat();
    $this->data['super_subkat']           = $this->Penjualan_model->get_super_subkat();
    $this->data['kategori_selected']      = '';
    $this->data['subkat_selected']        = '';
    $this->data['super_subkat_selected']  = '';
    $this->data['produk_selected']        = '';
    $this->data['harga_selected']         = '';
    $this->data['berat_selected']        = '';
    $this->data['trans']            = $this->Penjualan_model->get_trans_by_id($id);
    $this->data['trans_det']            = $this->Penjualan_model->get_trans_det_by_id($id);
    $this->load->view('back/penjualan/tambah_barang', $this->data);
  }

  public function tambah_barang_act(){
    $data = array(
      'trans_id'  => $this->input->post('trans_id'),
      'produk_id' => $this->input->post('id_produk'),
      'harga'     => $this->input->post('harga'),
      'berat'     => $this->input->post('berat'),
      'total_qty' => $this->input->post('total_qty'),
      'total_berat' => $this->input->post('berat') * $this->input->post('total_qty'),
      'subtotal'  => $this->input->post('harga') * $this->input->post('total_qty'),
    );
    $sql = $this->Penjualan_model->insert($data,'transaksi_detail');
    if($sql){
      //mengambil data stok barang
      $id = $this->input->post('id_produk');
      $idproduk = $this->Penjualan_model->get_stok_produk($id);
      $stok_int = intval($idproduk->stok);
      $sisa = $stok_int - $this->input->post('total_qty');
      $stok = array(
        'stok' => $sisa,
      );
      $this->Penjualan_model->update($stok,'produk','id_produk',$id);
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil ditambah</div>');
      redirect(site_url('admin/Penjualan/rincian_penjualan/'.$this->input->post('trans_id')));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal ditambah</div>');
      redirect(site_url('admin/Penjualan/tambah_barang'.$this->input->post('trans_id')));
    }
  }

  public function create_pembelian_act(){

		$invoice = $this->Cart_model->buat_kode();
    $data_trans = array(
      'nama_pembeli'  => $this->input->post('nama_pembeli'),
      'no_invoice' => $invoice,
      'created' => $this->input->post('created'),
      'ongkir'  => '-',
      'kurir'   => '-',
      'service' => '-',
      'status'  => ' ',
      'resi'    => ' ',
      'jenis_transaksi' => 'Offline',
      'alamat_pembeli' => $this->input->post('alamat_pembeli'),
    );
      $sql = $this->Penjualan_model->insert($data_trans,'transaksi');
      if ($sql){
        $get_id = $this->Penjualan_model->buat_kode2();
        $data_trans_dtl = array(
            'trans_id'  => $get_id,
            'produk_id' => $this->input->post('id_produk'),
            'harga'     => $this->input->post('harga'),
            'berat'     => $this->input->post('berat'),
            'total_qty' => $this->input->post('total_qty'),
            'total_berat' => $this->input->post('berat') * $this->input->post('total_qty'),
            'subtotal'  => $this->input->post('harga') * $this->input->post('total_qty'),
        );
        $sql2 = $this->Penjualan_model->insert($data_trans_dtl,'transaksi_detail');
        if($sql2){
          //mengambil data stok barang
          $id = $this->input->post('id_produk');
          $idproduk = $this->Penjualan_model->get_stok_produk($id);
          $stok_int = intval($idproduk->stok);
          $sisa = $stok_int - $this->input->post('total_qty');
          $stok = array(
            'stok' => $sisa,
          );
          $this->Penjualan_model->update($stok,'produk','id_produk',$id);
          $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
          redirect(site_url('admin/Penjualan/rincian_penjualan/'.$get_id));
        }else{
          $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
          redirect(site_url('admin/Penjualan/form_penjualan_SOP'));
        }

      }else{
        $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
        redirect(site_url('admin/Penjualan/form_penjualan_SOP'));
      }
  }



  public function pembayaran(){
    $this->data['title']            = 'Data Pembayaran';
    $this->data['bayar']           = $this->Penjualan_model->get_pembayaran();
    $this->load->view('back/penjualan/data_pembayaran', $this->data);
  }

  public function terima_pembayaran($id){
    $data = array(
      'status_pembayaran' => 'Diterima',
    );
    $sql = $this->Penjualan_model->update($data,'pembayaran','id_pembayaran', $id);
    if($sql){
      $get = $this->Penjualan_model->get_pembayarann($id);
      $no_invoicee = $get->no_invoice;
      $idd = $this->Penjualan_model->get_trans($no_invoicee);
      $id_transaksi = $idd->id_trans;
      $this->data['data']  = $this->Penjualan_model->get_trans_detail($id_transaksi)->result();

        $transdet_data = $this->data['data'];
        foreach ($transdet_data as $k) {
          $id_transdet = $k->id_transdet;
          $id_produk = $k->produk_id;
          $produk_data = $this->Penjualan_model->get_produk_by_id($id_produk);
          $data_transdet = array(
            'stok' => $produk_data->stok - $k->total_qty,
          );
          $sql_update = $this->Penjualan_model->update($data_transdet,'produk','id_produk', $id_produk);
        }
        if ($sql_update) {
          $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil Masuk</div>');
          redirect(site_url('admin/Penjualan/data_transfer'));
        } else {
          $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal Masuk</div>');
          redirect(site_url('admin/Penjualan/data_transfer'));
        }
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal Masuk</div>');
      redirect(site_url('admin/Penjualan/data_transfer'));
    }
  }

  public function tolak_pembayaran($id){
    $data = array(
      'status_pembayaran' => 'Ditolak',
    );
    $sql = $this->Penjualan_model->update($data,'pembayaran','id_pembayaran', $id);
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil Di Tolak</div>');
      redirect(site_url('admin/Penjualan/data_transfer'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal Di Tolak</div>');
      redirect(site_url('admin/Penjualan/data_transfer'));
    }
  }

  public function update($id)
  {
    $row      = $this->Cart_model->get_by_id($id);
    $invoice  = $row->id_trans;

    $this->data['penjualan'] = $this->Cart_model->get_by_id($id);

    if($row)
    {
      $this->data['title']              = 'Update Status '.$this->data['module'];

      $this->data['id_trans'] = array(
        'name'  => 'id_trans',
        'id'    => 'id_trans',
        'class' => 'form-control',
        'type'  => 'hidden',
      );
      $this->data['resi'] = array(
        'name'  => 'resi',
        'id'    => 'resi',
        'class' => 'form-control',
        'required' => ''
      );

      $this->load->view('back/penjualan/penjualan_status', $this->data);
    }
      else
      {
        $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Data tidak ditemukan</div>');
        redirect(site_url('admin/penjualan'));
      }
  }

  public function update_action()
  {
    $this->_rules();

    if ($this->form_validation->run() == FALSE)
    {
      $this->update($this->input->post('id_trans'));
    }
      else
      {
        $data = array(
          'resi'    => $this->input->post('resi'),
          'status'  => '2',
        );

        $this->Cart_model->update($this->input->post('id_trans'), $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert">Edit Data Berhasil</div>');
        redirect(site_url('admin/penjualan//data_transfer'));
      }
  }

  public function terkirim($id){
    $data = array(
      'status'  => '2',
    );
    $sql = $this->Penjualan_model->update($data,'transaksi','id_trans',$id);
    if ($sql) {
      $this->session->set_flashdata('message', '<div class="alert alert-success alert"> Data Berhasil Terkirim</div>');
      redirect(site_url('admin/penjualan/data_datang_ke_toko'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert"> Data Gagal Terkirim</div>');
      redirect(site_url('admin/penjualan/data_datang_ke_toko'));
    }
  }

  function data_transfer(){
    $this->data['title']            = 'Data '.$this->data['module'];
    $this->data['penjualan_data']   = $this->Cart_model->get_all();
    $this->data['data']   = $this->Penjualan_model->get_penjualan_transfer();
    $this->data['id']           = $this->Penjualan_model->get_pembayaran();

    $this->load->view('back/penjualan/penjualan_list', $this->data);
  }

  function data_datang_ke_toko(){
    $this->data['title']            = 'Data '.$this->data['module'];
    $this->data['penjualan_data']   = $this->Cart_model->get_all();
    $this->data['data']   = $this->Penjualan_model->get_penjualan_ke_toko();
    $this->data['id']           = $this->Penjualan_model->get_pembayaran();

    $this->load->view('back/penjualan/penjualan_list_offline', $this->data);
  }

  public function _rules()
  {
    $this->form_validation->set_rules('resi', 'No. Resi', 'trim|required');

    // set pesan form validasi error
    $this->form_validation->set_message('required', '{field} wajib diisi');

    $this->form_validation->set_rules('id_trans', 'id_trans', 'trim');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
  }

}
