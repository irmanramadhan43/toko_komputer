<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Retur extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Retur_model');

    $this->data['module'] = 'Retur';

    if ($this->session->userdata('usertype') == '' or $this->session->userdata('usertype') == '3' or $this->session->userdata('usertype') == '5' ){
			redirect('admin/auth/login', 'refresh');
		}
    // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
  }

  public function index()
  {
    $this->data['title'] = 'Data '.$this->data['module'];
    $this->data['retur'] = $this->Retur_model->get_all_retur();
    $this->load->view('back/retur/data_retur', $this->data);
  }

  function data_retur_supplier(){
    $this->data['title'] = 'Data Retur Supplier';
    $this->data['retur'] = $this->Retur_model->get_all_retur();
    $this->load->view('back/retur/data_retur_supplier', $this->data);
  }


function berhasil_retur($id){
  $data = array(
    'status_admin' => 'Berhasil Retur',
  );
  $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
  if($sql){
    $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil diretur</div>');
    redirect(site_url('admin/Retur/data_retur_supplier'));
  }else{
    $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal diretur</div>');
    redirect(site_url('admin/Retur/data_retur_supplier'));
  }
}



function belum_retur($id){
  $data = array(
    'status_admin' => 'Belum Retur',
  );
  $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
  if($sql){
    $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Belum Direturr</div>');
    redirect(site_url('admin/Retur/data_retur_supplier'));
  }else{
    $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal diretur</div>');
    redirect(site_url('admin/Retur/data_retur_supplier'));
  }
}



  function form_retur(){
    $this->data['title']          = 'Form Retur ';
    $this->data['action']         = site_url('admin/Retur/create_retur');
    $this->data['button_submit']  = 'Simpan';
    $this->data['button_reset']   = 'Reset';
    $this->data['retur']          = $this->Retur_model->get_retur();
    $this->data['id_retur']       = $this->Retur_model->buat_kode();

    $this->load->view('back/retur/form_retur', $this->data);
  }

  function create_retur(){
    $data = array(
      'id_retur'    => $this->input->post('id_retur', TRUE),
      'no_invoice'  => $this->input->post('invoice', TRUE),
      'pesan'       => $this->input->post('deskripsi', TRUE),
      'nama_barang'  => $this->input->post('nama_barang', TRUE),
      'tanggal'     => $this->input->post('tanggal', TRUE),
      'status_customer' => 'menunggu',
    );
    $sql = $this->Retur_model->insert($data,'retur');
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
      redirect(site_url('admin/Retur'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
      redirect(site_url('admin/Retur'));
    }
  }

  function proses_retur_supplier($id){
    $data = array(
      'status_admin' => 'Retur Sedang Diproses',
    );
    $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Retur Berhasil Diproses</div>');
      redirect(site_url('admin/Retur/data_retur_supplier'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Retur Gagal Diproses</div>');
      redirect(site_url('admin/Retur/data_retur_supplier'));
    }
  }

  function batal_retur($id){
    $data = array(
      'status_admin' => 'Retur Dibatalkan',
    );
    $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Retur Berhasil Dibatalkan</div>');
      redirect(site_url('admin/Retur/data_retur_supplier'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Retur Gagal Dibatalkan</div>');
      redirect(site_url('admin/Retur/data_retur_supplier'));
    }
  }

  function proses_retur($id){
    $this->data['title']          = 'Proses Retur ';
    $this->data['action']         = site_url('admin/Retur/proses_retur_act');
    $this->data['button_submit']  = 'Simpan';
    $this->data['button_reset']   = 'Reset';
    $this->data['retur']          = $this->Retur_model->get_retur_by_id($id);
    $data = array(
      'status_customer' => 'Sedang Dalam Proses',
    );
    $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
    $this->load->view('back/retur/proses_retur', $this->data);
  }

  function proses_retur_act(){
    $id = $this->input->post('id_retur');
    $data = array(
       'status_customer' => $this->input->post('status'),
       'pesan_admin' => $this->input->post('pesan'),
    );
    $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Retur Berhasil '.$this->input->post('status').'</div>');
      redirect(site_url('admin/Retur'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Retur Gagal '.$this->input->post('status').'</div>');
      redirect(site_url('admin/Retur'));
    }
  }

  function input_resi($id){
    $this->data['title']          = 'Proses Retur ';
    $this->data['action']         = site_url('admin/Retur/input_resi_act');
    $this->data['button_submit']  = 'Simpan';
    $this->data['button_reset']   = 'Reset';
    $this->data['retur']          = $this->Retur_model->get_retur_by_id($id);
    $data = array(
      'status_customer' => 'Proses',
    );
    $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
    $this->load->view('back/retur/input_resi', $this->data);
  }

  function input_resi_act(){
    $id = $this->input->post('id_retur');
    $data = array(
      'no_resi_admin' => $this->input->post('resi'),
      'status_customer' => 'Dikirim'
    );
    $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
    if($sql){
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Retur Berhasil </div>');
      redirect(site_url('admin/Retur'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Retur Gagal </div>');
      redirect(site_url('admin/Retur/input_resi'.$id));
    }
  }

  function notapo($id){
    ob_start();
    $this->data['identity']        = $this->Pembelian_model->get_data_pembelian_by_id($id);
    $this->data['barang']          = $this->Pembelian_model->data_barang_dipesan($id);

    $this->load->view('back/pembelian/notapo',$this->data);
    $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
    // $pdf = new HTML2PDF('P','A4','en');
    // $pdf->WriteHTML($html);
    // $pdf->Output('Data Siswa.pdf', 'D');
    try
   {
   $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
   $pdf->writeHTML($html, isset($_GET['vuehtml']));
   $pdf->Output('Convert1.pdf');
   }
   catch(HTML2PDF_exception $e) { echo $e; }
  }

  public function _rules()
  {
    $this->form_validation->set_rules('judul_kategori', 'Judul Kategori', 'trim|required');

    // set pesan form validasi error
    $this->form_validation->set_message('required', '{field} wajib diisi');

    $this->form_validation->set_rules('id_kategori', 'id_kategori', 'trim');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
  }

}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-17 02:19:21 */
/* http://harviacode.com */
