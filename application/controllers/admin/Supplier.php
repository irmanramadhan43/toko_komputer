<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Supplier extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Supplier_model');

    $this->data['module'] = 'Supplier';

    if ($this->session->userdata('usertype') == '' or $this->session->userdata('usertype') == '3' or $this->session->userdata('usertype') == '5' ){
			redirect('admin/auth/login', 'refresh');
		}
    // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
  }

  public function index()
  {
    $this->data['title'] = 'Data '.$this->data['module'];
    $this->data['supplier'] = $this->Supplier_model->get_supplier();
    $this->load->view('back/supplier/data_supplier', $this->data);
  }


  function tambah_supplier(){
      $this->data['title']          = 'Form Supplier ';
      $this->data['action']         = site_url('admin/Supplier/tambah_supplier_act');
      $this->data['button_submit']  = 'Simpan';
      $this->data['button_reset']   = 'Reset';
      $this->data['supplier']          = $this->Supplier_model->get_supplier();
      $this->data['id_supplier']       = $this->Supplier_model->buat_kode();

      $this->load->view('back/supplier/form_supplier', $this->data);
  }

  function tambah_supplier_act(){
    $data = array(
      'id_supp'     => $this->input->post('id_supp', TRUE),
      'nama_supp'   => $this->input->post('nama_supp', TRUE),
      'no_supp'     => $this->input->post('no_supp', TRUE),
      'alamat_supp' => $this->input->post('alamat_supp', TRUE),
    );
    $sql =  $this->Supplier_model->insert($data,'supplier');
    if($sql){
          $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil dibuat</div>');
          redirect(site_url('admin/Supplier'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
      redirect(site_url('admin/Supplier'));
    }
  }

  function edit_supplier($id){
    $this->data['title']          = 'Edit Supplier ';
    $this->data['action']         = site_url('admin/Supplier/edit_supplier_act');
    $this->data['button_submit']  = 'Simpan';
    $this->data['button_reset']   = 'Reset';
    $this->data['supplier']          = $this->Supplier_model->get_supplier_by_id($id);

    $this->load->view('back/supplier/edit_supplier', $this->data);
  }

  function edit_supplier_act(){
    $id = $this->input->post('id_supp', TRUE);
    $data = array(
      'nama_supp'   => $this->input->post('nama_supp', TRUE),
      'no_supp'     => $this->input->post('no_supp', TRUE),
      'alamat_supp' => $this->input->post('alamat_supp', TRUE),
    );
    $sql = $this->Supplier_model->update($data,'supplier','id_supp',$id);
    if($sql){
          $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil diubah</div>');
          redirect(site_url('admin/Supplier'));
    }else{
      $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal diubah</div>');
      redirect(site_url('admin/Supplier'));
    }
  }

  function hapus_supp($id){
    $where = array('id_supp' => $id);
		$this->Supplier_model->delete($where,'supplier');
    $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Berhasil dihapus</div>');
    redirect(site_url('admin/Supplier'));
  }
  // function form_retur(){
  //   $this->data['title']          = 'Form Retur ';
  //   $this->data['action']         = site_url('admin/Retur/create_retur');
  //   $this->data['button_submit']  = 'Simpan';
  //   $this->data['button_reset']   = 'Reset';
  //   $this->data['retur']          = $this->Retur_model->get_retur();
  //   $this->data['id_retur']       = $this->Retur_model->buat_kode();
  //
  //   $this->load->view('back/retur/form_retur', $this->data);
  // }
  //
  // function create_retur(){
  //   $data = array(
  //     'id_retur'    => $this->input->post('id_retur', TRUE),
  //     'pesan'       => $this->input->post('deskripsi', TRUE),
  //     'link_video'  => $this->input->post('link_video', TRUE),
  //     'tanggal'     => $this->input->post('tanggal', TRUE),
  //   );
  //   $sql = $this->Retur_model->insert($data,'retur');
  //   if($sql){
  //     $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
  //     redirect(site_url('admin/Retur'));
  //   }else{
  //     $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data Gagal dibuat</div>');
  //     redirect(site_url('admin/Retur'));
  //   }
  // }
  //
  // function proses_retur($id){
  //   $this->data['title']          = 'Proses Retur ';
  //   $this->data['action']         = site_url('admin/Retur/proses_retur_act');
  //   $this->data['button_submit']  = 'Simpan';
  //   $this->data['button_reset']   = 'Reset';
  //   $this->data['retur']          = $this->Retur_model->get_retur_by_id($id);
  //   $data = array(
  //     'status' => 'Sedang Dalam Proses',
  //   );
  //   $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
  //   $this->load->view('back/retur/proses_retur', $this->data);
  // }
  //
  // function proses_retur_act(){
  //   $id = $this->input->post('id');
  //   $data = array(
  //     // 'no_resi' => $this->input->post('no_resi'),
  //     'status' => $this->input->post('status'),
  //   );
  //
  //   $sql = $this->Retur_model->update($data,'retur','id_retur',$id);
  //   if($sql){
  //     $this->session->set_flashdata('message', '<div class="alert alert-success alert">Retur Berhasil '.$this->input->post('status').'</div>');
  //     redirect(site_url('admin/Retur'));
  //   }else{
  //     $this->session->set_flashdata('message', '<div class="alert alert-success alert">Retur Gagal '.$this->input->post('status').'</div>');
  //     redirect(site_url('admin/Retur'));
  //   }
  // }
  //
  // function notapo($id){
  //   ob_start();
  //   $this->data['identity']        = $this->Pembelian_model->get_data_pembelian_by_id($id);
  //   $this->data['barang']          = $this->Pembelian_model->data_barang_dipesan($id);
  //
  //   $this->load->view('back/pembelian/notapo',$this->data);
  //   $html = ob_get_contents();
  //       ob_end_clean();
  //
  //       require_once('./assets/html2pdf/html2pdf.class.php');
  //   // $pdf = new HTML2PDF('P','A4','en');
  //   // $pdf->WriteHTML($html);
  //   // $pdf->Output('Data Siswa.pdf', 'D');
  //   try
  //  {
  //  $pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
  //  $pdf->writeHTML($html, isset($_GET['vuehtml']));
  //  $pdf->Output('Convert1.pdf');
  //  }
  //  catch(HTML2PDF_exception $e) { echo $e; }
  // }

  public function _rules()
  {
    $this->form_validation->set_rules('judul_kategori', 'Judul Kategori', 'trim|required');

    // set pesan form validasi error
    $this->form_validation->set_message('required', '{field} wajib diisi');

    $this->form_validation->set_rules('id_kategori', 'id_kategori', 'trim');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
  }

}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-17 02:19:21 */
/* http://harviacode.com */
