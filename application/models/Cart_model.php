<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model
{
  public $table   = 'transaksi';
  public $table2  = 'transaksi_detail';
  public $id      = 'id_trans';
  public $id2     = 'id_transdet';

  // BACKEND //
  function get_all()
  {

    $this->db->join('users', 'transaksi.user_id = users.id');
      $this->db->join('transaksi_detail', 'transaksi_detail.user = users.id');
    return $this->db->get($this->table)->result();
  }

  function get_produk_qty($id){
    $dml = "SELECT * FROM produk WHERE id_produk = $id";
    $query = $this->db->query()->row();
    return $query;
  }

  function get_all_laporan(){
    $dml = "SELECT transaksi.created,judul_produk,total_qty,subtotal FROM `produk` join transaksi_detail JOIN transaksi WHERE status=2 and produk.id_produk = transaksi_detail.produk_id and transaksi.id_trans = transaksi_detail.trans_id";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  public function get_data_penjualan_periode()
  {
    $tgl_awal 	= $this->input->post('tgl_awal'); //getting from post value
    $tgl_akhir 	= $this->input->post('tgl_akhir'); //getting from post value

    $this->db->join('produk', 'transaksi_detail.produk_id = id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = id_trans');
    $this->db->where('status = 2');
    $this->db->where('transaksi.created >=', $tgl_awal.' 00:00:00');
    $this->db->where('transaksi.created <=', $tgl_akhir.' 23:59:59');
    return $this->db->get('transaksi_detail')->result();
  }

  function hitung_penjualan(){
    $tgl_awal 	= $this->input->post('tgl_awal'); //getting from post value
    $tgl_akhir 	= $this->input->post('tgl_akhir'); //getting from post value
    $dml = "SELECT SUM(subtotal) as total FROM transaksi_detail JOIN produk ON transaksi_detail.produk_id = id_produk JOIN transaksi ON transaksi_detail.trans_id = id_trans WHERE status=2  AND transaksi.created >= '$tgl_awal.00:00:00' AND transaksi.created <= '$tgl_akhir.23:59:59'";
    $query = $this->db->query($dml)->row();
    return $query;
  }


  function top5_transaksi()
  {
    $this->db->join('users', 'transaksi.user_id = users.id');
    $this->db->limit(5);
    $this->db->order_by('transaksi.id_trans', 'DESC');
    return $this->db->get($this->table)->result();
  }

  function get_cart_per_customer_finished_back($invoice)
  {
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->join('users', 'transaksi_detail.user = users.id');
    $this->db->where('transaksi.id_trans', $invoice);
    return $this->db->get($this->table2)->result();
  }



  function get_data_customer_back($invoice)
  {
    $this->db->join('provinsi', 'provinsi.id_provinsi = users.provinsi');
    $this->db->join('kota', 'kota.id_kota = users.kota');
    $this->db->join('transaksi', 'users.id = transaksi.user_id');
    $this->db->where('transaksi.id_trans', $invoice);
    return $this->db->get('users')->row();
  }

  function get_data_customer_back_POS()
  {
    $this->db->join('provinsi', 'provinsi.id_provinsi = users.provinsi');
    $this->db->join('kota', 'kota.id_kota = users.kota');
    $this->db->join('transaksi', 'users.id = transaksi.user_id');
    $this->db->where('transaksi.id_trans', null);
    return $this->db->get('users')->row();
  }

  function get_total_berat_dan_subtotal_finished_back($invoice)
  {
    $this->db->select_sum('total_berat');
    $this->db->select_sum('subtotal');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->where('transaksi.id_trans', $invoice);
    return $this->db->get($this->table2)->row();
  }

  function cek_transaksi_login_null(){
    $this->db->where('user_id', null);
    $this->db->where('status','0');
    return $this->db->get($this->table)->row();

  }

  // FRONTEND
  public function total_cart_navbar()
  {
    if ($this->session->userdata('transaksi') != "" and !$this->ion_auth->logged_in()) {
      $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
      $this->db->where('id_trans',$this->session->userdata('transaksi'));
      return $this->db->get($this->table2)->num_rows();
    }else{
      $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
      $this->db->where('user', $this->session->userdata('user_id'));
      $this->db->where('jenis_transaksi','online' );
      $this->db->where('status','0');
      return $this->db->get($this->table2)->num_rows();
    }
  }

  // cek transaksi per customer login
  function cek_transaksi()
  {
    $this->db->where('user_id', $this->session->userdata('user_id'));
    $this->db->where('status','0');
    return $this->db->get($this->table)->row();
  }

  function get_notransdet($id)
  {
    $this->db->join('transaksi_detail', 'transaksi.id_trans = transaksi_detail.trans_id');
    $this->db->where('produk_id',$id);
    $this->db->where('user_id', $this->session->userdata('user_id'));
    $this->db->where('status','0');
    return $this->db->get($this->table)->row();
  }

  // ambil semua data dari 4 tabel per customer
  function get_cart_per_customer()
  {
    if ($this->session->userdata('transaksi') != "" and !$this->ion_auth->logged_in()) {
      $this->db->join('transaksi_detail', 'transaksi.id_trans = transaksi_detail.trans_id');
      $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
      $this->db->where('transaksi.id_trans', $this->session->userdata('transaksi'));
      return $this->db->get($this->table)->result();
    }else{
      $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
      $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
      $this->db->join('users', 'transaksi_detail.user = users.id');
      $this->db->where('transaksi.user_id', $this->session->userdata('user_id'));
      $this->db->where('transaksi.status','0');
      return $this->db->get($this->table2)->result();
    }
  }


  function get_cart_per_customer_finished($invoice)
  {
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->join('users', 'transaksi.user_id = users.id');
    $this->db->where('transaksi.id_trans', $invoice);
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->where('status','1');
    return $this->db->get($this->table2)->result();
  }

  function get_cart_per_customer_finished_($invoice)
  {
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->join('users', 'transaksi.user_id = users.id');
    $this->db->where('transaksi.id_trans', $invoice);
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->where('status','1');
    return $this->db->get($this->table2)->row();
  }

  // ambil data pribadi per customer login
  function get_data_customer()
  {
    $this->db->join('provinsi', 'provinsi.id_provinsi = users.provinsi');
    $this->db->join('kota', 'kota.id_kota = users.kota');
    $this->db->join('transaksi', 'users.id = transaksi.user_id');
    $this->db->order_by('transaksi.id_trans', 'DESC');
    $this->db->limit('1');
    $this->db->where('user_id', $this->session->userdata('user_id'));
    return $this->db->get('users')->row();
  }

  // ambil total_berat dan subtotal per transaksi customer login
  function get_total_berat_dan_subtotal()
  {
    $this->db->select_sum('total_berat');
    $this->db->select_sum('subtotal');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->where('status','0');
    return $this->db->get($this->table2)->row();
  }

  function get_total_berat_dan_subtotal_finished($invoice)
  {
    $this->db->select_sum('total_berat');
    $this->db->select_sum('subtotal');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->where('status','1');
    $this->db->where('transaksi.id_trans', $invoice);
    return $this->db->get($this->table2)->row();
  }

  function get_all_finished_back($invoice)
  {
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->join('users', 'transaksi.user_id = users.id');
    $this->db->where('transaksi.id_trans', $invoice);
    return $this->db->get($this->table2);
  }

  function get_all_finished($invoice)
  {
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->join('users', 'transaksi.user_id = users.id');
    $this->db->where('transaksi.id_trans', $invoice);
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->where('status','1');
    return $this->db->get($this->table2);
  }

  function total_berat_finished_back($invoice)
  {
    $this->db->select_sum('total_berat');
    $this->db->select_sum('subtotal');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->where('status','1');
    $this->db->or_where('status','2');
    $this->db->where('transaksi.id_trans', $invoice);
    return $this->db->get($this->table2)->row();
  }

  function subtotal_history($id)
  {
    $this->db->select_sum('subtotal');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->where($this->id, $id);
    $this->db->where('user', $this->session->userdata('user_id'));
    return $this->db->get($this->table2)->row();
  }

  function get_by_id($id)
  {
    $this->db->where($this->id, $id);
    return $this->db->get($this->table)->row();
  }

  // get total rows
  function total_rows() {
    return $this->db->get($this->table)->num_rows();
  }

  // insert data
  function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  function insert_detail($data2)
  {
    $this->db->insert($this->table2, $data2);
  }

  function checkout($id, $data)
  {
    $this->db->where($this->id,$id);
    $this->db->where('user_id', $this->session->userdata('user_id'));
    $this->db->where('status','0');
    $this->db->update($this->table, $data);
  }

  // update data
  function update($id, $data)
  {
    $this->db->where($this->id,$id);
    $this->db->update($this->table, $data);
  }

  function update_transdet($id, $data)
  {
    $this->db->where('produk_id',$id);
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->update($this->table2, $data);
  }

  // delete data
  function delete($id,$id_trans)
  {
    $this->db->where('produk_id', $id);
    $this->db->where('trans_id', $id_trans);
    $this->db->delete($this->table2);
  }

  function kosongkan_keranjang($id_trans)
  {
    $this->db->where('trans_id', $id_trans);
    $this->db->delete($this->table2);
  }

  // function get_notransdetnew()
  // {
  //   $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
  //   $this->db->where('user', $this->session->userdata('user_id'));
  //   $this->db->where('status','0');
  //   return $this->db->get($this->table2)->row();
  // }

  function cart_history()
  {
    $this->db->where('user_id', $this->session->userdata('user_id'));
    $this->db->where_not_in('status','0');
    return $this->db->get($this->table);
  }

  function pay_confirm_history(){
    $id = $this->session->userdata('user_id');
    $dml = "SELECT * FROM pembayaran WHERE user_id = $id";
    $query = $this->db->query($dml);
    return $query;
  }

  function cart_history_detail()
  {
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->join('users', 'transaksi_detail.user = users.id');
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->where('status','0');
    return $this->db->get($this->table2);
  }

  function history_detail($id)
  {
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->join('users', 'transaksi_detail.user = users.id');
    $this->db->where($this->id, $id);
    $this->db->where('user_id', $this->session->userdata('user_id'));
    return $this->db->get($this->table2);
  }

  function history_total_berat($id)
  {
    $this->db->select_sum('total_berat');
    $this->db->join('transaksi', 'transaksi_detail.trans_id = transaksi.id_trans');
    $this->db->where($this->id2, $id);
    $this->db->where('user', $this->session->userdata('user_id'));
    $this->db->where('status','1');
    return $this->db->get($this->table2)->row();
  }

  function get_bulan()
  {
    $this->db->select('judul_produk, transaksi.created as tanggal');
    $this->db->select_sum('total_qty');
    $this->db->join('transaksi_detail', 'transaksi.id_trans = transaksi_detail.trans_id');
    $this->db->join('produk', 'transaksi_detail.produk_id = produk.id_produk');
    $this->db->where('month(transaksi.created)', date('m'));
    $this->db->group_by('produk_id');
    $this->db->order_by('tanggal', 'DESC');
    $this->db->limit(5);
    return $this->db->get($this->table)->result();
  }

  function total_penjualan()
  {
    $this->db->join('transaksi_detail', 'transaksi.id_trans = transaksi_detail.trans_id');
    return $this->db->get($this->table)->result();
  }

  // Laporan




  //laporan stok Barang
  function get_all_stok_barang(){
      $dml = "SELECT * FROM produk";
      $query = $this->db->query($dml)->result();
     return $query;
  }

  public function get_data_stok_barang_periode()
  {
    $tgl_awal 	= $this->input->post('tgl_awal'); //getting from post value
    $tgl_akhir 	= $this->input->post('tgl_akhir'); //getting from post value
    $dml = "SELECT * FROM produk WHERE created BETWEEN '$tgl_awal' and '$tgl_akhir'";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function buat_kode()   {
    $this->db->select('RIGHT(transaksi.no_invoice,2) as no_invoice', FALSE);
    $this->db->order_by('no_invoice','DESC');
    $this->db->limit(1);
    $query = $this->db->get('transaksi');      //cek dulu apakah  sudah ada no di tabel.
      if($query->num_rows() <> 0){
       //jika no ternyata sudah ada.
       $data = $query->row();
       $kode = intval($data->no_invoice) + 1;
    }
    else{
     //jika no belum ada
     $kode = 1;
    }
    $kodemax = str_pad($kode, 3, "00", STR_PAD_LEFT);
    $kodejadi = "FS".date('dmy').$kodemax;
    return $kodejadi;
   }

   function cek_pembayaran_by_invoice(){
     $id = $this->session->userdata('user_id');
     $dml="SELECT * FROM pembayaran where user_id = $id";
     $query = $this->db->query($dml)->row();
     return $query;
     //SELECT * FROM `pembayaran` WHERE no_invoice = 'FS110719008' and status_pembayaran = 'Belum Diterima'
   }

   function cek_pembayaran($table,$where){
    return $this->db->get_where($table,$where);
  }

  function get_trans_by_id(){
    $id = $this->session->userdata('user_id');
    $dml="SELECT * FROM transaksi where user_id = $id";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function get_invoice_by_id_trans($id){
    $dml = "SELECT * FROM transaksi WHERE id_trans = $id";
    $query = $this->db->query($dml);
    return $query;
  }

  function get_pembayaran_by_retur(){
    $id = $this->session->userdata('user_id');
    $dml = "SELECT transaksi.* ,retur.id_retur,pembayaran.id_pembayaran FROM transaksi LEFT join retur on transaksi.no_invoice = retur.no_invoice
    LEFT JOIN pembayaran on transaksi.no_invoice = pembayaran.no_invoice where
    transaksi.user_id = '$id'";
    $query = $this->db->query($dml);
    return $query;
  }

  function get_pemb(){
    $id = $this->session->userdata('user_id');
    $dml = "SELECT transaksi.* ,retur.id_retur,pembayaran.* FROM transaksi LEFT join retur on transaksi.no_invoice = retur.no_invoice
    LEFT JOIN pembayaran on transaksi.no_invoice = pembayaran.no_invoice where
    transaksi.user_id = '$id'";
    $query = $this->db->query($dml);
    return $query;
  }

  function updateTransaksi($id_trans,$data){
    $this->db->where('transaksi.id_trans',$id_trans);
    $this->db->update('transaksi',$data);
  }

  function updateTransDet($id_trans,$data){
    $this->db->where('transaksi_detail.trans_id',$id_trans);
    $this->db->update('transaksi_detail',$data);
  }

  function get_AllKota(){
    return $this->db->get('kota')->result();
  }

  function get_all_transaksi($id_trans){
    $dml = "SELECT * FROM transaksi join transaksi_detail on transaksi_detail.trans_id = transaksi.id_trans join produk on produk.id_produk = transaksi_detail.produk_id where transaksi_detail.produk_id = produk.id_produk and transaksi.id_trans = $id_trans";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_trans_row($id_trans){
    $dml = "SELECT * FROM transaksi  where transaksi.id_trans = $id_trans";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function update_stok($data,$table,$where,$value){
    $this->db->where($where,$value);
    $this->db->update($table,$data);
    if($this->db->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  function get_transdet_by_id($id_td){
    $dml = "SELECT * FROM transaksi_detail WHERE id_transdet = $id_td";
    $query = $this->db->query($dml)->row();
    return $query;
  }

}
