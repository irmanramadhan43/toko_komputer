<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{

  public $id    = 'id_pembelian';

  function get_by_id($id)
  {
    $this->db->where('judul_seo', $id);
    return $this->db->get($this->table)->row();
  }

  function get_produk_(){
    $dml = "SELECT * from produk where stok > 0";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_kategori(){
    $dml = "SELECT * from kategori";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_subkat(){
    $dml = "SELECT * from subkategori";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_super_subkat(){
    $dml = "SELECT * from supersubkategori";
    $query = $this->db->query($dml)->result();
    return $query;
  }
  // insert data

  function insert($data,$table){
    $this->db->insert($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
    }else{
      return false;
    }
  }

  function get_all_pembelian(){
      $dml = "SELECT tanggal_datang, nama_supp, judul_produk, jumlah_order, subtotal_beli
      from pembelian_detail JOIN pembelian USING (id_pembelian) JOIN supplier USING (id_supp)";
      $query = $this->db->query($dml)->result();
     return $query;
  }

  public function get_data_pembelian_periode()
	{
		$tgl_awal 	= $this->input->post('tgl_awal'); //getting from post value
    $tgl_akhir 	= $this->input->post('tgl_akhir'); //getting from post value
    $dml = "SELECT * from pembelian_detail
    JOIN pembelian USING (id_pembelian) JOIN supplier USING (id_supp)
    where tanggal_datang BETWEEN '$tgl_awal' and '$tgl_akhir'";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_supplier(){
    $dml = "SELECT id_supp,nama_supp FROM supplier";
    $query = $this->db->query($dml)->result();
    return $query;
  }
  function get_produk(){
    $dml = "SELECT * FROM produk";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_data_pembelian(){
    $dml = "SELECT * FROM `pembelian` JOIN supplier WHERE supplier.id_supp = pembelian.id_supp ";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_data_pembelian_by_id($id){
    $dml = "SELECT * FROM `pembelian` JOIN supplier WHERE supplier.id_supp = pembelian.id_supp and id_pembelian = $id";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function hitung_total_beli($id){
    $dml = "SELECT  SUM(IF( id_pembelian = $id, subtotal_beli, 0)) AS total_pem
        FROM pembelian_detail";
        $query = $this->db->query($dml)->row();
        return $query;
  }

  function hitung_jumlah_barang($id){
    $dml = "SELECT  SUM(IF( id_pembelian = $id, jumlah_datang, 0)) AS total_beli
        FROM pembelian_detail";
        $query = $this->db->query($dml)->row();
        return $query;
  }

  function get_data_penerimaan(){
    $dml = "SELECT DISTINCT * FROM pembelian JOIN supplier USING (id_supp)";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_data_pembelian_by_id_($id){
    $dml = "SELECT * FROM pembelian inner join pembelian_detail inner join supplier WHERE pembelian.id_pembelian = pembelian_detail.id_pembelian AND pembelian.id_supp = supplier.id_supp and pembelian.id_pembelian = $id";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function get_dat_pembelian($id){
    $dml = "SELECT * from pembelian
    inner join pembelian_detail WHERE pembelian.id_pembelian = pembelian_detail.id_pembelian and pembelian_detail.id_pembelian = $id";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function data_barang_dipesan($id){
    $dml = "SELECT DISTINCT * FROM pembelian inner join pembelian_detail WHERE pembelian_detail.id_pembelian = pembelian.id_pembelian and pembelian.id_pembelian = $id";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_data_pemdet($id){
    $dml = "SELECT * FROM pembelian_detail WHERE id_pemdet = $id";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function buat_kodep()   {
    $this->db->select('RIGHT(pembelian.kode_pembelian,2) as kode_pembelian', FALSE);
	  $this->db->order_by('kode_pembelian','DESC');
	  $this->db->limit(1);
    $query = $this->db->get('pembelian');      //cek dulu apakah  sudah ada no di tabel.
      if($query->num_rows() <> 0){
       //jika no ternyata sudah ada.
       $data = $query->row();
       $kode = intval($data->kode_pembelian) + 1;
	  }
	  else{
	   //jika no belum ada
	   $kode = 1;
	  }
	  $kodemax = str_pad($kode, 3, "00", STR_PAD_LEFT);
	  $kodejadi = "PO".date('dmy').$kodemax;
	  return $kodejadi;
	 }

  function buat_kode()   {
    $this->db->select('id_pembelian', FALSE);
    $this->db->order_by('id_pembelian','DESC');
    $this->db->limit(1);
    $query = $this->db->get('pembelian');      //cek dulu apakah  sudah ada no di tabel.
      if($query->num_rows() <> 0){
       //jika no ternyata sudah ada.
       $data = $query->row();
       $kode = intval($data->id_pembelian) + 1;
      }
      else{
       //jika no belum ada
       $kode = 1;
      }
    $kodemax = str_pad($kode, 1, STR_PAD_LEFT);
    $kodejadi = $kodemax;
    return $kodejadi;
 }

 function get_id_pemdet($id){
   $dml = " SELECT pembelian_detail.id_pemdet FROM pembelian_detail INNER JOIN
   pembelian WHERE pembelian_detail.id_pembelian = pembelian.id_pembelian and
   pembelian_detail.id_pembelian = $id";
   $query = $this->db->query($dml)->row();
   return $query;
 }



 function update($data,$table,$where,$value){
   $this->db->where($where,$value);
   $this->db->update($table,$data);
   if($this->db->affected_rows()>0){
     return true;
   }else{
     return false;
   }
 }

 function delete($where,$value,$table){
		$this->db->where($where,$value);
		$this->db->delete($table);
	}

  function cek_judul($table,$where){
		return $this->db->get_where($table,$where);
	}

  function get_id_produk($judul_pembelian){
    $dml = "SELECT * FROM `produk` WHERE judul_produk = '$judul_pembelian'";
    $query = $this->db->query($dml)->row();
    return $query;
  }

}

/* End of file Komentar_model.php */
/* Location: ./application/models/Komentar_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-17 02:19:21 */
/* http://harviacode.com */
