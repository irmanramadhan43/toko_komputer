<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penjualan_model extends CI_Model
{

  function get_produk(){
    $dml = "SELECT * from produk where stok > 0";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_produk_pos(){
    $dml = "SELECT * from transaksi where jenis_transaksi = 'Offline' and user_id is null";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_trans_by_id($id){
    $dml = "SELECT * from transaksi where id_trans = $id";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function get_trans_det_by_id($id){
    $dml = "SELECT * FROM `transaksi_detail` JOIN produk where trans_id = $id and produk.id_produk = transaksi_detail.produk_id";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_kategori(){
    $dml = "SELECT * from kategori";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_subkat(){
    $dml = "SELECT * from subkategori";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_super_subkat(){
    $dml = "SELECT * from supersubkategori";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_produk_by_id($id){
    $dml = "SELECT * FROM produk where id_produk = $id";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function get_penjualan_by_id($id){
    $dml = "SELECT * FROM transaksi where id_trans = $id";
    $query = $this->db->query()->row();
    return $query;
  }

  function insert($data,$table){
    $this->db->insert($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
    }else{
      return false;
    }
  }

  function get_stok_produk($id){
    $dml = "SELECT stok FROM produk WHERE id_produk = $id";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function update($data,$table,$where,$value){
    $this->db->where($where,$value);
    $this->db->update($table,$data);
    if($this->db->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  function get_status_pembayaran(){
    $dml = "SELECT status_pembayaran FROM pembayaran";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_pembayaran(){
    $dml = "SELECT * FROM pembayaran WHERE status_pembayaran = 'Belum Diterima' OR status_pembayaran = 'DI TOLAK' ";
    $query = $this->db->query($dml)->result();
    return $query;

  }

  function get_penjualan(){
    $dml = "SELECT * FROM transaksi join pembayaran JOIN users WHERE transaksi.user_id = users.id and transaksi.no_invoice = pembayaran.no_invoice";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_penjualan_transfer(){
    $dml = "SELECT transaksi.*,users.*,pembayaran.status_pembayaran FROM transaksi left join pembayaran on pembayaran.no_invoice = transaksi.no_invoice left JOIN users ON transaksi.user_id = users.id WHERE jenis_transaksi = 'online' group by id_trans";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function get_penjualan_ke_toko(){
    $dml = "SELECT transaksi.*,users.*,pembayaran.status_pembayaran FROM transaksi left join pembayaran on pembayaran.no_invoice = transaksi.no_invoice left JOIN users ON transaksi.user_id = users.id WHERE jenis_transaksi = 'offline' group by id_trans ";
    $query = $this->db->query($dml)->result();
    return $query;
  }

  function buat_kode2()   {
    $this->db->select('id_trans', FALSE);
    $this->db->order_by('id_trans','DESC');
    $this->db->limit(1);
    $query = $this->db->get('transaksi');      //cek dulu apakah  sudah ada no di tabel.
      if($query->num_rows() <> 0){
       //jika no ternyata sudah ada.
       $data = $query->row();
       $kode = intval($data->id_trans);
      }
      else{
       //jika no belum ada
       $kode = 1;
      }
    $kodemax = str_pad($kode, 1, STR_PAD_LEFT);
    $kodejadi = $kodemax;
    return $kodejadi;
 }

  function buat_kode()   {
    $this->db->select('id_trans', FALSE);
    $this->db->order_by('id_trans','DESC');
    $this->db->limit(1);
    $query = $this->db->get('transaksi');      //cek dulu apakah  sudah ada no di tabel.
      if($query->num_rows() <> 0){
       //jika no ternyata sudah ada.
       $data = $query->row();
       $kode = intval($data->id_trans) + 1;
      }
      else{
       //jika no belum ada
       $kode = 1;
      }
    $kodemax = str_pad($kode, 1, STR_PAD_LEFT);
    $kodejadi = $kodemax;
    return $kodejadi;
 }

 function cek_invoice($table,$where){
		return $this->db->get_where($table,$where);
	}

  function get_pembayarann($id){
    $dml = "SELECT * FROM pembayaran where id_pembayaran = '$id'";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function get_trans($no_invoicee){
    $dml = "SELECT * FROM transaksi where no_invoice = '$no_invoicee'";
    $query = $this->db->query($dml)->row();
    return $query;
  }

  function get_trans_detail($id_transaksi){
    $dml = "SELECT * FROM `transaksi_detail` WHERE trans_id = $id_transaksi";
    $query = $this->db->query($dml);
    return $query;
  }




}
