<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><?php echo $module ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
						<div class="box box-primary">
              <div class="box-body">
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                <div class="table-responsive no-padding">
									<table id="datatable" class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center">ID</th>
												<th style="text-align: center">Tanggal Order</th>
												<th style="text-align: center">Supplier</th>
												<th style="text-align: center">Status</th>
												<th style="text-align: center">Nota PO</th>
												<th style="text-align: center">Action</th>

											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach ($pembelian as $p):?>
											<tr>
												<td style="text-align:center"><?php echo $p->kode_pembelian ?></td>
												<td style="text-align:center"><?php echo $p->tanggal_order ?></td>
												<td style="text-align:center"><?php echo $p->nama_supp ?></td>
												<td style="text-align:center"><?php echo $p->status_beli ?></td>
												<td style="text-align:center">
													<?php if($p->status_beli == 'Diterima' || $p->status_beli == 'Disetujui' ){ ?>
                            <a href="<?php echo base_url().'admin/pembelian/notapo/'.$p->id_pembelian ?>" target="_blank">
                              <button type="button" name="status" class="btn btn-danger">Cetak</button>
                            </a>
                          <?php } ?>
												</td>
												<td style="text-align:left">
                        <?php if($this->session->userdata('usertype') == '6' && $p->status_beli == 'Tunggu Persetujuan Owner'){ ?>
                          <a href="<?php echo site_url('admin/pembelian/persetujuan_order/'.$p->id_pembelian) ?>">
  													<button type="button" class="btn btn-warning">Persetujuan Order</button></a>
                        <?php } else if($p->status_beli == 'Diterima' || $p->status_beli == 'Batal' ) { ?>
                          <a href="<?php echo site_url('admin/pembelian/rincian/'.$p->id_pembelian) ?>" type="button" class="btn btn-success">Lihat Rincian</button>
                        <?php } else { ?>
                          <a href="<?php echo site_url('admin/pembelian/rincian/'.$p->id_pembelian) ?>" type="button" class="btn btn-success">Lihat Rincian</button>
                        <a href="<?php echo site_url('admin/pembelian/batal_pembelian/'.$p->id_pembelian) ?>">
													<button type="button" class="btn btn-custon-three btn-danger btn-sm">Batal</button></a>
                        <?php } ?>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>

                </div>
							</div>
						</div>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div>
  <?php $this->load->view('back/js') ?>
	<!-- DATA TABLES-->
  <link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function confirmDialog() {
      return confirm('Apakah anda yakin?')
    }
    $('#datatable').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false,
      "aaSorting": [[0,'desc']],
      "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua"]]
    });
  </script>
</body>
</html>
