<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#"><?php echo $module ?></a></li>
					<li class="active"><?php echo $title ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6">
						<div class="box box-primary">
              <div class="box-body">
								<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
								<?php echo form_open($action);?>
                <div class="form-group"><label>No Pembelian</label>
                  <input type="text" name="id_pemdet" parsley-trigger="change" required
                        value="<?php echo $identity->id_pemdet ?>"   class="form-control" id="no_pembelian" readonly>
                        <input type="hidden" name="id_pembelian" parsley-trigger="change" required
                              value="<?php echo $identity->id_pembelian ?>"   class="form-control" id="no_pembelian" >
                </div><div class="form-group"><label>Nama Barang</label>
                  <input type="text" name="nama_barang" parsley-trigger="change"  readonly
                        value="<?php echo $identity->judul_produk ?>"   class="form-control"  >
                </div>
                <div class="form-group"><label>Jumlah Datang</label>
                  <input type="number" name="jumlah_datang" parsley-trigger="change" required
                        value="1"   class="form-control" id="no_pembelian" >
                </div>
                <div class="form-group"><label>Harga Beli</label>
                  <input type="text" name="harga_beli" parsley-trigger="change" required
                           class="form-control" id="no_pembelian" >
                </div>
                <div class="form-group"><label>Harga Jual</label>
                  <input type="text" name="harga_jual" parsley-trigger="change" required
                          class="form-control" id="no_pembelian" >
                </div>
									<button type="submit" name="submit" class="btn btn-success"><?php echo $button_submit ?></button>
									<button type="reset" name="reset" class="btn btn-danger"><?php echo $button_reset ?></button>
								<?php echo form_close(); ?>
							</div>
						</div>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div><!-- ./wrapper -->
  <?php $this->load->view('back/js') ?>
</body>
</html>
