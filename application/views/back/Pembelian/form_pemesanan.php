<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#"><?php echo $module ?></a></li>
					<li class="active"><?php echo $title ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6">
						<div class="box box-primary">
              <div class="box-body">
								<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
								<?php echo form_open($action);?>
                <div class="form-group"><label>No Pembelian</label>
                  <input type="text" name="id_pembelian" parsley-trigger="change" required
                        value="<?php echo $id_pembelian ?>"   class="form-control" id="no_pembelian" readonly>
                </div>
                <div class="form-group"><label>Tanggal</label>
                  <input type="date" name="tanggal_order" parsley-trigger="change" required
                        value="<?php echo date('Y-m-d'); ?>"   class="form-control" id="tanggal">
                </div>
                <div class="form-group"><label>Supplier</label>
                    <select class="form-control select2_1" name="id_supp">
                      <?php foreach ($supplier as $s ): ?>
                        <option value="<?php echo $s->id_supp ?>"><?php echo $s->nama_supp ?></option>
                      <?php endforeach; ?>
  							    </select>
                </div>
                <div class="form-group"><label>Kategori</label>
                    <select class="form-control" name="kategori" id="kategori">
                      <option value="">Please Select</option>
                      <?php foreach ($kategori as $k ): ?>
                        <option <?php echo $kategori_selected == $k->id_kategori ? 'selected="selected"' : '' ?>
                          value="<?php echo $k->id_kategori  ?>"><?php echo $k->judul_kategori ?></option>
                      <?php endforeach; ?>
  							    </select>
                </div>
                <div class="form-group"><label>Sub Kategori</label>
                  <select class="form-control" name="subkat" id="subkat">
                    <option value="">Please Select</option>
                    <?php foreach ($subkat as $s ): ?>
                      <option <?php echo $subkat_selected == $s->id_subkategori ? 'selected="selected"' : '' ?>
                      class="<?php echo $s->id_kat ?>" value="<?php echo $s->id_subkategori  ?>"><?php echo $s->judul_subkategori ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group"><label>Super Subkategori</label>
                  <select class="form-control" name="supersubkat" id="super_subkat">
                    <option value="">Please Select</option>
                    <?php foreach ($super_subkat as $s ): ?>
                      <option <?php echo $super_subkat_selected == $s->id_supersubkategori ? 'selected="selected"' : '' ?>
                        class="<?php echo $s->id_subkat ?>" value="<?php echo $s->id_supersubkategori  ?>"><?php echo $s->judul_supersubkategori ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

							</div>
						</div>
          </div><!-- ./col -->
          <div class="col-lg-6">
						<div class="box box-primary">
              <div class="box-body">
                <div class="form-group"><label>Nama  Barang (Jika Tidak ada ketik barang dibawah kolom ini)</label>
                  <select class="form-control" name="judul_barang" id="nama_barang">
                    <option value="">Please Select</option>
                    <?php foreach ($produk as $p ): ?>
                      <option <?php echo $produk_selected == $p->id_produk ? 'selected="selected"' : '' ?>
                         class="<?php echo $p->supersubkat_id ?>" value="<?php echo $p->judul_produk  ?>"><?php echo $p->judul_produk ?>  Stok  <?php echo $p->stok?></option>
                    <?php endforeach; ?>
                  </select><br>
                  <input type="text" name="judul_produk_c" parsley-trigger="change"
                          placeholder="Ketik nama barang" class="form-control" >
                </div>
                <div class="form-group"><label>Deskripsi Barang</label>
                  <textarea class="form-control" rows="4" name="deskripsi"></textarea>
                </div>
                <div class="form-group"><label>Jumlah</label>
                  <input type="number" name="jumlah" parsley-trigger="change" required
                          value="1" class="form-control" id="tanggal">
                </div>
									<button type="submit" name="submit" class="btn btn-success"><?php echo $button_submit ?></button>
									<button type="reset" name="reset" class="btn btn-danger"><?php echo $button_reset ?></button>

                <?php echo form_close(); ?>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
        <script src="<?php echo base_url('assets/jquery-1.10.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/jquery.chained.min.js') ?>"></script>
        <script>
              $("#subkat").chained("#kategori");
              $("#super_subkat").chained("#subkat");
              $("#nama_barang").chained("#super_subkat");
              $("#harga").chained("#nama_barang");
              $("#berat").chained("#harga");
        </script>
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div><!-- ./wrapper -->
  <?php $this->load->view('back/js') ?>
</body>
</html>
