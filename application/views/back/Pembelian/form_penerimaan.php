<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><?php echo $module ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
						<div class="box box-primary">
              <div class="box-body">
                <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
								<?php echo form_open($action);?>
                <div class="form-group"><label>No Pembelian</label><br>
                <input type="text" name="id_pembelian" class="form-control" value="<?php echo $identity->id_pembelian ?>" readonly>
                </div>
                <div class="form-group"><label>Tanggal</label><br>
                <input type="text" name="tanggal_datang" class="form-control" value="<?php echo $identity->tanggal_order ?>" readonly>
                </div>
                <div class="form-group"><label>Jumlah Datang Barang</label><br>
                <input type="text" name="jumlah_datang" class="form-control" readonly value="<?php echo $total_beli->total_beli ?>" placeholder="Masukkan Jumlah Datang Barang">
                </div>
                <div class="form-group"><label>Total Beli</label><br>
                <input type="text" name="subtotal_beli" value="<?php echo $total_pembelian->total_pem ?>"  class="form-control" readonly>
                </div>
                <label>Supplier</label><br>
                <input type="text" class="form-control" value="<?php echo $identity->nama_supp ?>" readonly>
                <div class="form-group"><label>Status</label>
                    <select class="form-control select2_1" name="status">
                        <option value="Diterima">Diterima</option>
                        <option value="Ditolak">Ditolak</option>
  							    </select>
                </div>
                <button type="submit" name="submit" class="btn btn-success"><?php echo $button_submit ?></button>
                <?php echo form_close(); ?>
                <br><hr>
                <h3>Data Barang Dipesan</h3>
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                <div class="table-responsive no-padding">
									<table id="datatable" class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center">No</th>
												<th style="text-align: center">Nama Barang</th>
												<th style="text-align: center">Jumlah Order (PO)</th>
												<th style="text-align: center">Jumlah Datang</th>
												<th style="text-align: center">Harga Beli</th>
												<th style="text-align: center">Harga Jual</th>
												<th style="text-align: center">Total</th>
												<th style="text-align: center">Action</th>


											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach ($barang as $p):?>
											<tr>
												<td style="text-align:center"><?php echo $no ?></td>
												<td style="text-align:center"><?php echo $p->judul_produk ?></td>
												<td style="text-align:center"><?php echo $p->jumlah_order ?></td>
												<td style="text-align:center"><?php if($p->jumlah_datang == 0 ){
                          echo '-';
                        } else {
                        echo $p->jumlah_datang;
                        }
                        ?></td>
												<td style="text-align:center"><?php if($p->jumlah_datang == 0 ){
                          echo '-';
                        } else {
                        echo number_format($p->harga_barang);
                        }
                        ?></td>
												<td style="text-align:center"><?php if($p->jumlah_datang == 0 ){
                          echo '-';
                        } else {
                        echo number_format($p->harga_jual);
                        }
                        ?></td>
												<td style="text-align:center"><?php if($p->jumlah_datang == 0 ){
                          echo '-';
                        } else {
                        echo number_format($p->subtotal_beli);
                        }
                        ?></td>
												<td style="text-align:center">
                          <a href="<?php echo site_url('admin/pembelian/edit_barang_pemesanan/'.$p->id_pemdet) ?>">
                            <button type="button" class="btn btn-custon-three btn-success btn-sm">Edit</button></a>
                       </td>
											</tr>

											<?php $no = $no + 1; endforeach;?>
										</tbody>
									</table>
                </div>
							</div>
						</div>
           <a href="<?php echo site_url('admin/pembelian/data_penerimaan') ?>" type="button" class="btn btn-success">Kembali</button></a>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div>
  <?php $this->load->view('back/js') ?>
	<!-- DATA TABLES-->
  <link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function confirmDialog() {
      return confirm('Apakah anda yakin?')
    }
    $('#datatable').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false,
      "aaSorting": [[0,'desc']],
      "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua"]]
    });
  </script>
</body>
</html>
