<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><?php echo $module ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
						<div class="box box-primary">
              <div class="box-body">

                <label>No Pembelian</label><br>
                <input type="text" class="form-control" value="<?php echo $identity->id_pembelian ?>" readonly>
                <label>Tanggal</label><br>
                <input type="text" class="form-control" value="<?php echo $identity->tanggal_order ?>" readonly>
                <label>Supplier</label><br>
                <input type="text" class="form-control" value="<?php echo $identity->nama_supp ?>" readonly>
                <br><hr>
                <h3>Data Barang Dipesan</h3>
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                <div class="table-responsive no-padding">
									<table id="datatable" class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center">No</th>
												<th style="text-align: center">Nama Barang</th>
												<th style="text-align: center">Jumlah Barang</th>

											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach ($barang as $p):?>
											<tr>
												<td style="text-align:center"><?php echo $no ?></td>
												<td style="text-align:center"><?php echo $p->judul_produk ?></td>
												<td style="text-align:center"><?php echo $p->jumlah_order ?></td>
											</tr>
											<?php $no = $no + 1; endforeach;?>
										</tbody>
									</table>
                </div>
							</div>
						</div>
            <?php if ($identity->status_beli == "Tunggu Persetujuan Owner"){ ?>
           <a href="<?php echo site_url('admin/pembelian/tambah_pemesanan/'.$identity->id_pembelian) ?>" type="button" class="btn btn-success">Tambah Barang</button></a>
           <a href="<?php echo site_url('admin/pembelian/') ?>" type="button" class="btn btn-success">Selesai</button></a>
         <?php } else { ?>
           <a href="<?php echo site_url('admin/pembelian/') ?>" type="button" class="btn btn-success">Kembali</button></a>
         <?php } ?>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div>
  <?php $this->load->view('back/js') ?>
	<!-- DATA TABLES-->
  <link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function confirmDialog() {
      return confirm('Apakah anda yakin?')
    }
    $('#datatable').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false,
      "aaSorting": [[0,'desc']],
      "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua"]]
    });
  </script>
</body>
</html>
