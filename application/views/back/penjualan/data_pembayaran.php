<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><?php echo $module ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
						<div class="box box-primary">
              <div class="box-body">
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                <div class="table-responsive no-padding">
									<table id="datatable" class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center">ID</th>
												<th style="text-align: center">No Invoice</th>
												<th style="text-align: center">Nama Pengirim</th>
												<th style="text-align: center">Jumlah</th>
												<th style="text-align: center">Bank Asal</th>
												<th style="text-align: center">Bank Tujuan</th>
												<th style="text-align: center">Bukti Pembayaran</th>
												<th style="text-align: center">Action</th>

											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach ($bayar as $b):?>
											<tr>
												<td style="text-align:center"><?php echo $b->id_pembayaran ?></td>
												<td style="text-align:center"><?php echo $b->no_invoice ?></td>
												<td style="text-align:center"><?php echo $b->nama_pengirim ?></td>
												<td style="text-align:center"><?php echo $b->jumlah ?></td>
												<td style="text-align:center"><?php echo $b->bank_asal ?></td>
												<td style="text-align:center"><?php echo $b->bank_tujuan ?></td>
												<td style="text-align:center"><a href="<?php echo base_url().'assets/images/buktibayar/'.$b->bukti_pembayaran ?>" target="_blank" class="btn btn-success">
                          Klik untuk melihat gambar</a></td>
												<td style="text-align:center">
													<?php //if($b->status_beli == 'Diterima' || $b->status_beli == 'Disetujui' ){ ?>
                            <a href="<?php echo base_url().'admin/penjualan/terima_pembayaran/'.$b->id_pembayaran; ?>" >
                              <button type="button" name="status" class="btn btn-success">Diterima</button>
                            </a>
                            <a href="<?php echo base_url().'admin/penjualan/tolak_pembayaran/'.$b->id_pembayaran; ?>" >
                              <button type="button" name="status" class="btn btn-warning">Ditolak</button>
                            </a>
                          <?php //} ?>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>

                </div>
							</div>
						</div>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div>
  <?php $this->load->view('back/js') ?>
	<!-- DATA TABLES-->
  <link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function confirmDialog() {
      return confirm('Apakah anda yakin?')
    }
    $('#datatable').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false,
      "aaSorting": [[0,'desc']],
      "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua"]]
    });
  </script>
</body>
</html>
