<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#"><?php echo $module ?></a></li>
					<li class="active"><?php echo $title ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6">
						<div class="box box-primary">
              <div class="box-body">
								<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
								<?php echo form_open($action);?>
                <div class="form-group"><label>Id Transaksi</label>
                      <input type="text" name="trans_id" parsley-trigger="change" required readonly
                              value="<?php echo $trans->id_trans ?>" class="form-control" id="total_qty">
                </div>
                <div class="form-group"><label>Kategori</label>
                    <select class="form-control" name="kategori" id="kategori">
                      <option value="">Please Select</option>
                      <?php foreach ($kategori as $k ): ?>
                        <option <?php echo $kategori_selected == $k->id_kategori ? 'selected="selected"' : '' ?>
                          value="<?php echo $k->id_kategori  ?>"><?php echo $k->judul_kategori ?></option>
                      <?php endforeach; ?>
  							    </select>
                </div>
                <div class="form-group"><label>Sub Kategori</label>
                  <select class="form-control" name="subkat" id="subkat">
                    <option value="">Please Select</option>
                    <?php foreach ($subkat as $s ): ?>
                      <option <?php echo $subkat_selected == $s->id_subkategori ? 'selected="selected"' : '' ?>
                      class="<?php echo $s->id_kat ?>" value="<?php echo $s->id_subkategori  ?>"><?php echo $s->judul_subkategori ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group"><label>Super Subkategori</label>
                  <select class="form-control" name="super_subkat" id="super_subkat">
                    <option value="">Please Select</option>
                    <?php foreach ($super_subkat as $s ): ?>
                      <option <?php echo $super_subkat_selected == $s->id_supersubkategori ? 'selected="selected"' : '' ?>
                        class="<?php echo $s->id_subkat ?>" value="<?php echo $s->id_supersubkategori  ?>"><?php echo $s->judul_supersubkategori ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group"><label>Nama Barang</label>
                  <select class="form-control" name="id_produk" id="nama_barang">
                    <option value="">Please Select</option>
                    <?php foreach ($produk as $p ): ?>
                      <option <?php echo $produk_selected == $p->id_produk ? 'selected="selected"' : '' ?>
                         class="<?php echo $p->supersubkat_id ?>" value="<?php echo $p->id_produk  ?>"><?php echo $p->judul_produk ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group"><label>Harga</label>
                  <select class="form-control" name="harga" id="harga">
                    <option value="">Please Select</option>
                    <?php foreach ($produk as $p ): ?>
                      <option <?php echo $harga_selected == $p->harga_normal ? 'selected="selected"' : '' ?>
                        class="<?php echo $p->id_produk ?>" value="<?php echo $p->harga_normal  ?>"><?php echo $p->harga_normal ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group"><label>Berat(Kg)</label>
                  <select class="form-control" name="berat" id="berat">
                    <option value="">Please Select</option>
                    <?php foreach ($produk as $p ): ?>
                      <option <?php echo $berat_selected == $p->berat ? 'selected="selected"' : '' ?>
                        class="<?php echo $p->harga_normal ?>" value="<?php echo $p->berat  ?>"><?php echo $p->berat ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group"><label>Jumlah</label>
                  <input type="number" name="total_qty" parsley-trigger="change" required
                          value="1" class="form-control">
                </div>
									<button type="submit" name="submit" class="btn btn-success"><?php echo $button_submit ?></button>
									<button type="reset" name="reset" class="btn btn-danger"><?php echo $button_reset ?></button>
								<?php echo form_close(); ?>
							</div>
						</div>
          </div><!-- ./col -->

            <div class="col-lg-6">
  						<div class="box box-primary">
                <div class="box-body">
                  <h3>Data Barang Dipesan</h3>
  								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                  <div class="table-responsive no-padding">
  									<table id="datatable" class="table table-striped">
  										<thead>
  											<tr>
  												<th style="text-align: center">No</th>
  												<th style="text-align: center">Nama Barang</th>
  												<th style="text-align: center">Jumlah Barang</th>

  											</tr>
  										</thead>
  										<tbody>
  											<?php $no=1; foreach ($trans_det as $p):?>
  											<tr>
  												<td style="text-align:center"><?php echo $no ?></td>
  												<td style="text-align:center"><?php echo $p->judul_produk ?></td>
  												<td style="text-align:center"><?php echo $p->total_qty ?></td>
  											</tr>
  											<?php $no = $no + 1; endforeach;?>
  										</tbody>
  									</table>
                  </div>
  							</div>
  						</div>
            </div><!-- ./col -->
        </div><!-- /.row -->
        <script src="<?php echo base_url('assets/jquery-1.10.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/jquery.chained.min.js') ?>"></script>
        <script>
              $("#subkat").chained("#kategori");
              $("#super_subkat").chained("#subkat");
              $("#nama_barang").chained("#super_subkat");
              $("#harga").chained("#nama_barang");
              $("#berat").chained("#harga");
        </script>
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div><!-- ./wrapper -->
  <?php $this->load->view('back/js') ?>
</body>
</html>
