<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><?php echo $module ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
						<div class="box box-primary">
              <div class="box-body">
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                <div class="table-responsive no-padding">
									<table id="datatable" class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center">ID</th>
												<th style="text-align: center">No Invoice</th>
												<th style="text-align: center">Pesan</th>
												<th style="text-align: center">Nama Barang</th>
												<th style="text-align: center">Tanggal</th>
												<th style="text-align: center">Status</th>
												<th style="text-align: center">Action</th>

											</tr>
										</thead>
										<tbody>
											<?php  foreach ($retur as $p):?>
											<tr>
												<td style="text-align:center"><?php echo $p->id_retur ?></td>
												<td style="text-align:center"><?php echo $p->no_invoice ?></td>
												<td style="text-align:center"><?php echo $p->pesan ?></td>
												<td style="text-align:center"><?php echo $p->barang ?></td>
												<td style="text-align:center"><?php echo $p->tanggal ?></td>
                        <td style="text-align:center">

                          <?php if($p->status_admin == 'Berhasil Retur') {?>
                            <button type="button" class="btn btn-success"><?php echo $p->status_admin ?></button>
                          <?php }else if($p->status_admin == 'Belum Retur'){  ?>
                            <button type="button" class="btn btn-danger"><?php echo $p->status_admin ?></button>
                          <?php }else if($p->status_admin == 'Retur Dibatalkan'){ ?>
                            <button type="button" class="btn btn-danger"><?php echo $p->status_admin ?></button>
                          <?php }else{ ?>
                            <button type="button" class="btn btn-warning"><?php echo $p->status_admin ?></button>
                          <?php } ?>
												</td>
												<td style="text-align:center">

                          <?php if($p->status_admin != 'Retur Dibatalkan' and $p->status_admin != 'Berhasil Retur' and $p->status_admin != 'Retur Sedang Diproses'){ ?>
                            <a href="<?php echo site_url('admin/retur/batal_retur/'.$p->id_retur) ?>">
                              <button type="button" class="btn btn-success">Batal</button></a>
                          <?php } ?>

                          <?php if($p->status_admin != 'Retur Sedang Diproses' and $p->status_admin == 'Belum Retur' and $p->status_admin != 'Retur Dibatalkan'){ ?>
                            <a href="<?php echo site_url('admin/retur/proses_retur_supplier/'.$p->id_retur) ?>">
                              <button type="button" class="btn btn-success">Proses Retur</button></a>
                          <?php } ?>
                          <?php if($p->status_admin != 'Berhasil Retur' and $p->status_admin == 'Retur Sedang Diproses' and $p->status_admin != 'Retur Dibatalkan'){ ?>
                            <a href="<?php echo site_url('admin/retur/berhasil_retur/'.$p->id_retur) ?>">
                              <button type="button" class="btn btn-success">Berhasil Retur</button></a>
                          <?php  }?>

												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>

                </div>
							</div>
						</div>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div>
  <?php $this->load->view('back/js') ?>
	<!-- DATA TABLES-->
  <link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function confirmDialog() {
      return confirm('Apakah anda yakin?')
    }
    $('#datatable').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false,
      "aaSorting": [[0,'desc']],
      "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua"]]
    });
  </script>
</body>
</html>
