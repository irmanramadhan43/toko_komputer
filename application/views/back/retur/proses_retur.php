<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#"><?php echo $module ?></a></li>
					<li class="active"><?php echo $title ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
						<div class="box box-primary">
              <div class="box-body">
								<?php echo validation_errors() ?>
								<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
								<?php echo form_open($action);?>
                <div class="form-group"><label>No Retur</label>
                  <input type="text" name="id_retur" parsley-trigger="change" required readonly
                        value="<?php echo $retur->id_retur ?>"   class="form-control"  >

                </div>
                <div class="form-group"><label>Pesan Admin</label>
                  <input type="text" name="pesan" parsley-trigger="change" required
                          class="form-control"  >
                </div>
                  <div class="form-group"><label>Status Retur</label>
                      <select class="form-control select2_1" name="status">
                          <option value="Barang Diterima">Barang Diterima</option>
                          <option value="menunggu">Barang Habis</option>
                          <option value="Barang Tidak Sesuai">Barang Tidak Sesuai</option>
    							    </select>
                  </div>
									<button type="submit" name="submit" class="btn btn-success"><?php echo $button_submit ?></button>
								<?php echo form_close(); ?>
							</div>
						</div>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div><!-- ./wrapper -->
  <?php $this->load->view('back/js') ?>
</body>
</html>
