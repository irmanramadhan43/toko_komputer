<aside class="main-sidebar">
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image"><img src="<?php echo base_url()?>assets/images/user/<?php echo $this->session->userdata('photo').$this->session->userdata('photo_type') ?>" class="img-circle" alt="User Image"/></div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('name'); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <ul class="sidebar-menu">
      <li class="header">MENU UTAMA</li>
      <li <?php if($this->uri->segment(2)=="dashboard"){echo "class='active'";} ?>>
        <a href="<?php echo base_url('admin/dashboard') ?>">
          <i class="fa fa-home"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="treeview">


      <!-- =============================================MENU MASTER================================================== -->
      <li <?php if($this->uri->segment(2) == "auth"){echo "class='active'";}
      elseif($this->uri->segment(2) == "kategori"){echo "class='active'";}
      elseif($this->uri->segment(2) == "subkategori"){echo "class='active'";}
      elseif($this->uri->segment(2) == "supersubkategori"){echo "class='active'";}
      elseif($this->uri->segment(2) == "produk"){echo "class='active'";}
      ?>>

        <a href='#'><i class='fa fa-newspaper-o'></i><span> DATA MASTER </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>

          <!-- =============================================DATA SUPPLIER================================================== -->
          <li <?php if($this->uri->segment(2) == "supplier"){echo "class='active'";} ?>>
            <a href='#'><i class='fa fa-tags'></i><span> Data Supplier </span><i class='fa fa-angle-left pull-right'></i></a>
            <ul class='treeview-menu'>
              <li <?php if($this->uri->segment(2) == "supplier" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/supplier/tambah_supplier') ?>'><i class='fa fa-circle-o'></i> Tambah Supplier </a></li>
              <li <?php if($this->uri->segment(2) == "supplier" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/supplier') ?>'><i class='fa fa-circle-o'></i> Data Supplier </a></li>
            </ul>
          </li>


        <!-- =============================================DATA KATEGORI================================================== -->
            <li <?php if($this->uri->segment(2) == "kategori"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-tags'></i><span> Data Kategori </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "kategori" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/kategori/create') ?>'><i class='fa fa-circle-o'></i> Tambah Kategori </a></li>
          <li <?php if($this->uri->segment(2) == "kategori" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/kategori') ?>'><i class='fa fa-circle-o'></i> Data Kategori </a></li>
        </ul>
      </li>
      <!-- =============================================DATA SUBKATEGORI================================================== -->
      <li <?php if($this->uri->segment(2) == "subkategori"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-tags'></i><span> Data SubKategori </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "subkategori" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/subkategori/create') ?>'><i class='fa fa-circle-o'></i> Tambah SubKategori </a></li>
          <li <?php if($this->uri->segment(2) == "subkategori" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/subkategori') ?>'><i class='fa fa-circle-o'></i> Data SubKategori </a></li>
        </ul>
      </li>
      <!-- =============================================DATA SUPERSUBKATEGORI================================================== -->
      <li <?php if($this->uri->segment(2) == "supersubkategori"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-tags'></i><span> Data SuperSubKategori </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "supersubkategori" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/supersubkategori/create') ?>'><i class='fa fa-circle-o'></i> Tambah SuperSubKategori </a></li>
          <li <?php if($this->uri->segment(2) == "supersubkategori" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/supersubkategori') ?>'><i class='fa fa-circle-o'></i> Data SuperSubKategori </a></li>
        </ul>
      </li>
      <!-- =============================================DATA PRODUK================================================== -->
      <li <?php if($this->uri->segment(2) == "produk"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-shopping-cart'></i><span> Data Produk </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "produk" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/produk/create') ?>'><i class='fa fa-circle-o'></i> Tambah Produk </a></li>
          <li <?php if($this->uri->segment(2) == "produk" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/produk') ?>'><i class='fa fa-circle-o'></i> Data Produk </a></li>
        </ul>
      </li>

        </ul>
      </li>
      <!-- =========================================================================================================== -->
      <!-- =============================================DATA PEMBELIAN================================================ -->
      <li <?php if($this->uri->segment(2) == "Pembelian"){echo "class='active'";}
      elseif($this->uri->segment(2) == "pembayaran"){echo "class='active'";}
      ?>>

      <a href='#'><i class='fa fa-newspaper-o'></i><span> DATA PEMBELIAN </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>

        <li <?php if($this->uri->segment(2)=="pembelian"){echo "class='active'";} ?>>
        <a href="<?php echo base_url('admin/Pembelian/form_pemesanan') ?>">
          <i class="fa fa-circle-o"></i> <span>Form Pemesanan</span>
        </a>
        <a href="<?php echo base_url('admin/Pembelian') ?>">
          <i class="fa fa-circle-o"></i> <span>Data Pemesanan</span>
        </a>
        <a href="<?php echo base_url('admin/Pembelian/data_penerimaan') ?>">
          <i class="fa fa-circle-o"></i> <span>Form Penerimaan</span>
        </a>
      </li>
        </ul>
      </li>
      <!-- =============================================END PEMBELIAN================================================ -->

      <!-- =============================================DATA PENJUALAN================================================ -->
      <li <?php if($this->uri->segment(2) == "penjualan"){echo "class='active'";}
      elseif($this->uri->segment(2) == "pembayaran"){echo "class='active'";}
      ?>>

      <a href='#'><i class='fa fa-newspaper-o'></i><span> DATA PENJUALAN </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>

        <li <?php if($this->uri->segment(2)=="penjualan"){echo "class='active'";} ?>>
        <a href="<?php echo base_url('admin/penjualan/form_penjualan_POS') ?>">
          <i class="fa fa-money"></i> <span>Form Penjualan POS</span>
        </a>
        <a href="<?php echo base_url('admin/penjualan/data_penjualan_pos') ?>">
          <i class="fa fa-money"></i> <span>Data Penjualan POS</span>
        </a>
        <a href="<?php echo base_url('admin/penjualan/data_transfer') ?>">
          <i class="fa fa-money"></i> <span>Data Penjualan Online</span>
        </a>
        <a href="<?php echo base_url('admin/penjualan/pembayaran') ?>">
          <i class="fa fa-money"></i> <span>Data Pembayaran</span>
        </a>
      </li>
        </ul>
      </li>


      <!-- =============================================== Data Retur ================================================ -->
      <li <?php if($this->uri->segment(2) == "laporan"){echo "class='active'";}
     elseif($this->uri->segment(2) == "pembayaran"){echo "class='active'";}
     ?>>
     <a href='#'><i class='fa fa-newspaper-o'></i><span> Data Retur </span><i class='fa fa-angle-left pull-right'></i></a>
       <ul class='treeview-menu'>

       <li <?php if($this->uri->segment(2)=="laporan"){echo "class='active'";} ?>>
       <a href="<?php echo base_url('admin/Retur/form_retur') ?>">
         <i class="fa fa-circle-o"></i> <span>Form Retur Customer</span>
       </a>
       <a href="<?php echo base_url('admin/Retur') ?>">
         <i class="fa fa-circle-o"></i> <span>Data Retur Customer</span>
       </a>
       <a href="<?php echo base_url('admin/Retur/data_retur_supplier') ?>">
         <i class="fa fa-circle-o"></i> <span>Data Retur Supplier</span>
       </a>
     </li>
       </ul>
     </li>
     <!-- =============================================LAPORAN PENJUALAN================================================== -->


     <li <?php if($this->uri->segment(2) == "laporan"){echo "class='active'";}
    elseif($this->uri->segment(2) == "pembayaran"){echo "class='active'";}
    ?>>

    <a href='#'><i class='fa fa-file'></i><span> Laporan </span><i class='fa fa-angle-left pull-right'></i></a>
      <ul class='treeview-menu'>

      <li <?php if($this->uri->segment(2)=="laporan"){echo "class='active'";} ?>>
      <a href="<?php echo base_url('admin/Laporan') ?>">
        <i class="fa fa-circle-o"></i> <span>Laporan Penjualan</span>
      </a>
      <a href="<?php echo base_url('admin/Laporan/laporan_pembelian') ?>">
        <i class="fa fa-circle-o"></i> <span>Laporan Pembelian</span>
      </a>
      <a href="<?php echo base_url('admin/Laporan/laporan_retur') ?>">
        <i class="fa fa-circle-o"></i> <span>Laporan Retur</span>
      </a>
      <a href="<?php echo base_url('admin/Laporan/laporan_stok_barang') ?>">
        <i class="fa fa-circle-o"></i> <span>Laporan Stock Barang</span>
      </a>
    </li>
      </ul>
    </li>
      <li class="header">SETTING</li>

      <!-- =============================================PREFERENCE================================================== -->
      <li <?php if($this->uri->segment(2) == "blog"){echo "class='active'";}
      elseif($this->uri->segment(2) == "featured"){echo "class='active'";}
      elseif($this->uri->segment(2) == "slider"){echo "class='active'";}
      elseif($this->uri->segment(2) == "company"){echo "class='active'";}
      elseif($this->uri->segment(2) == "editakun"){echo "class='active'";}
      elseif($this->uri->segment(2) == "kontak"){echo "class='active'";}
      ?>>

        <a href='#'><i class='fa fa-sun-o'></i><span> Preference </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
      <!-- =============================================LIHAT WEB================================================== -->
        <li class="treeview">
        <a href="<?php echo base_url() ?>" target="_blank">
          <i class="fa fa-globe"></i> <span>Lihat Website</span>
        </a>
      </li>
      <!-- =============================================BLOG======================================================== -->
      <li <?php if($this->uri->segment(2) == "blog"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-newspaper-o'></i><span> Blog </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "blog" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/blog/create') ?>'><i class='fa fa-circle-o'></i> Tambah Blog </a></li>
          <li <?php if($this->uri->segment(2) == "blog" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/blog') ?>'><i class='fa fa-circle-o'></i> Data Blog </a></li>
        </ul>
      </li>
      <!-- =============================================FEATURED================================================== -->
      <li <?php if($this->uri->segment(2) == "featured"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-star'></i><span> Featured </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "featured" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/featured/create') ?>'><i class='fa fa-circle-o'></i> Tambah Featured </a></li>
          <li <?php if($this->uri->segment(2) == "featured" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/featured') ?>'><i class='fa fa-circle-o'></i> Data Featured </a></li>
        </ul>
      </li>
      <!-- =============================================SLIDER================================================== -->
      <li <?php if($this->uri->segment(2) == "slider"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-newspaper-o'></i><span> Slider </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "slider" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/slider/create') ?>'><i class='fa fa-circle-o'></i> Tambah Slider </a></li>
          <li <?php if($this->uri->segment(2) == "slider" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/slider') ?>'><i class='fa fa-circle-o'></i> Data Slider </a></li>
        </ul>
      </li>
      <!-- =============================================PROFIL TOKO================================================== -->
      <li <?php if($this->uri->segment(2) == "company"){echo "class='active'";} ?>>
      <a href='<?php echo base_url() ?>admin/company/update/1'> <i class="fa fa-building"></i> <span>Profil Toko</span> </a> </li>

      <!-- =============================================EDIT AKUN================================================== -->
      <li <?php if($this->uri->segment(2) == "editakun"){echo "class='active'";} ?>>
      <a href='<?php $user_id = $this->session->userdata('user_id'); echo base_url('admin/auth/edit_user/'.$user_id.'') ?>'>
          <i class='fa fa-edit'></i><span> Edit Akun </span>
        </a>
      </li>
      <!-- =============================================KONTAK================================================== -->
      <li <?php if($this->uri->segment(2) == "kontak"){echo "class='active'";} ?>>
        <a href='#'><i class='fa fa-phone'></i><span> Kontak </span><i class='fa fa-angle-left pull-right'></i></a>
        <ul class='treeview-menu'>
          <li <?php if($this->uri->segment(2) == "kontak" && $this->uri->segment(3) == "create"){echo "class='active'";} ?>><a href='<?php echo base_url('admin/kontak/create') ?>'><i class='fa fa-circle-o'></i> Tambah Kontak </a></li>
          <li <?php if($this->uri->segment(2) == "kontak" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url('admin/kontak') ?>'><i class='fa fa-circle-o'></i> Data Kontak </a></li>
        </ul>
      </li>
      </ul>
      </li>

      <!-- =============================================DATA USER================================================== -->

      <li><?php if ($this->session->userdata('usertype')=='1'): ?>
        <li <?php if($this->uri->segment(2) == "auth"){echo "class='active'";} ?>>
          <a href='#'><i class='fa fa-user'></i><span> Data User </span><i class='fa fa-angle-left pull-right'></i></a>
          <ul class='treeview-menu'>
            <li <?php if($this->uri->segment(2) == "auth" && $this->uri->segment(3) == "create_user"){echo "class='active'";} ?>><a href='<?php echo base_url() ?>admin/auth/create_user'><i class='fa fa-circle-o'></i> Tambah User</a></li>
            <li <?php if($this->uri->segment(2) == "auth" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url() ?>admin/auth/data_user'><i class='fa fa-circle-o'></i> Data User</a></li>
            <?php endif ?>
            </ul>
        </li>
      </li>
      <li><?php if ($this->session->userdata('usertype')=='1'): ?>
        <li <?php if($this->uri->segment(2) == "auth"){echo "class='active'";} ?>>
          <a href='#'><i class='fa fa-user'></i><span> Data Pelanggan </span><i class='fa fa-angle-left pull-right'></i></a>
          <ul class='treeview-menu'>
            <li <?php if($this->uri->segment(2) == "auth" && $this->uri->segment(3) == ""){echo "class='active'";} ?>><a href='<?php echo base_url() ?>admin/auth/data_pelanggan'><i class='fa fa-circle-o'></i> Data Pelanggan</a></li>
            <?php endif ?>
            </ul>
        </li>
      </li>




      <li> <a href='<?php echo base_url() ?>admin/auth/logout'> <i class="fa fa-sign-out"></i> <span>Logout</span> </a> </li>
    </ul>


  </section>
</aside>
