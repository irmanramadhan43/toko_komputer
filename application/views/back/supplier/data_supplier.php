<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><?php echo $module ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
						<div class="box box-primary">
              <div class="box-body">
                <a href="<?php echo base_url('admin/supplier/tambah_supplier') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                <div class="table-responsive no-padding">
									<table id="datatable" class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center">ID</th>
												<th style="text-align: center">Nama Supplier</th>
												<th style="text-align: center">No Supplier</th>
												<th style="text-align: center">Alamat Supplier</th>
												<th style="text-align: center">Action</th>

											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach ($supplier as $s):?>
											<tr>
												<td style="text-align:center"><?php echo $s->id_supp ?></td>
												<td style="text-align:center"><?php echo $s->nama_supp ?></td>
												<td style="text-align:center"><?php echo $s->no_supp ?></td>
												<td style="text-align:center"><?php echo $s->alamat_supp ?></td>
												<td style="text-align:center">
                          <a href="<?php echo site_url('admin/supplier/edit_supplier/'.$s->id_supp) ?>">
  													<button type="button" class="btn btn-warning">Edit</button></a>
                        <a href="<?php echo site_url('admin/supplier/hapus_supp/'.$s->id_supp) ?>">
													<button type="button" class="btn btn-custon-three btn-danger btn-sm">Hapus</button></a>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>

                </div>
							</div>
						</div>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div>
  <?php $this->load->view('back/js') ?>
	<!-- DATA TABLES-->
  <link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function confirmDialog() {
      return confirm('Apakah anda yakin?')
    }
    $('#datatable').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false,
      "aaSorting": [[0,'desc']],
      "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua"]]
    });
  </script>
</body>
</html>
