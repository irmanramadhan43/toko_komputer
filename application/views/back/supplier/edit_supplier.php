<?php $this->load->view('back/meta') ?>
  <div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $title ?></h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#"><?php echo $module ?></a></li>
					<li class="active"><?php echo $title ?></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6">
						<div class="box box-primary">
              <div class="box-body">
								<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
								<?php echo form_open($action);?>
                <div class="form-group"><label>ID Supplier</label>
                  <input type="text" name="id_supp" parsley-trigger="change" required
                        value="<?php echo $supplier->id_supp ?>"   class="form-control" id="no_pembelian" readonly>
                </div>
                <div class="form-group"><label>Nama Supplier</label>
                  <input type="text" name="nama_supp" parsley-trigger="change" required
                        value="<?php echo $supplier->nama_supp ?>" class="form-control" id="nama_supp">
                </div>
                <div class="form-group"><label>No Telp</label>
                  <input type="text" name="no_supp" parsley-trigger="change" required
                        value="<?php echo $supplier->no_supp ?>" class="form-control" id="no_supp">
                </div>
                <div class="form-group"><label>Alamat</label>
                  <input type="text" name="alamat_supp" parsley-trigger="change" required
                      value="<?php echo $supplier->alamat_supp ?>"   class="form-control" id="alamat_supp">
                </div>
									<button type="submit" name="submit" class="btn btn-success"><?php echo $button_submit ?></button>
									<button type="reset" name="reset" class="btn btn-danger"><?php echo $button_reset ?></button>
								<?php echo form_close(); ?>
							</div>
						</div>
          </div><!-- ./col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
  </div><!-- ./wrapper -->
  <?php $this->load->view('back/js') ?>
</body>
</html>
