<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>
<br><br><br>
<div class="container">
	<div class="row">
    <div class="col-sm-12 col-lg-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
      	</ol>
      </nav>
    </div>
		<p><br><br>
			<div class="col-sm-12 col-lg-9"><h1>Keranjang</h1><hr>
		</p>
		<div class="row">
			<div class="col-lg-12">
				<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
				<div class="box-body table-responsive padding">
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th style="text-align: center">No.</th>
								<th style="text-align: center">Barang</th>
								<th style="text-align: center">Harga</th>
								<th style="text-align: center">Berat</th>
								<th style="text-align: center">J.Berat</th>
								<th style="text-align: center">Qty</th>
								<th style="text-align: center">Total</th>
								<th style="text-align: center">Aksi</th>
							</tr>
						</thead>
						<tbody>
						<?php $no=1; foreach ($cart_data as $cart){ ?>
							<tr>
								<td style="text-align:center"><?php echo $no++ ?></td>
								<td style="text-align:left"><a href="<?php echo base_url('produk/read/').$cart->slug_produk ?>"><?php echo $cart->judul_produk ?></a></td>
								<td style="text-align:center"><?php echo number_format($cart->harga_diskon) ?></td>
								<td style="text-align:center"><?php echo $cart->berat ?></td>
								<td style="text-align:center"><?php echo $cart->total_berat ?></td>
								<form action="<?php echo base_url('cart/update/').$cart->produk_id ?>" method="post">
								<td style="text-align:center">
									<input type="hidden" name="produk_id" value="<?php echo $cart->produk_id ?>">
									<input type="number" name="qty" style="width: 50px" value="<?php echo $cart->total_qty ?>">
								</td>
								<td style="text-align:center"><?php echo number_format($cart->subtotal) ?></td>
								<td style="text-align:center">
									<button type="submit" name="update" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i></button>
									<button type="submit" name="delete" class="btn btn-sm btn-danger"><i class="fa fa-remove"></i></button>
								</td>
								</form>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>


			<div class="row">
        <div class="col-lg-12">
          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
          <?php echo form_open($action) ?>
            <div class="form-group has-feedback"><label>Nama Pembeli</label>
              <input type="text" name="nama_pembeli" class="form-control">
            </div>
						<div class="form-group has-feedback"><label>Email</label>
              <input type="email" name="email" class="form-control">
            </div>
            <div class="form-group has-feedback"><label>Kota</label><br>
							<select id="kota" name="kota" class="form-control">
							<?php foreach ($kota as $kot): ?>
								<option value="<?php echo $kot->id_kota ?>"><?php echo $kot->nama_kota ?></option>
							<?php endforeach; ?>
						</select>
            </div>
						<div class="form-group has-feedback"><label>Alamat</label>
              <textarea name="alamat" rows="5" col="150" class="form-control"></textarea>
            </div>
						<div class="form-group has-feedback"><label>Pilih Jenis Pembayaran</label><br>
							<select id="ok" onChange="opsi()" name="jenis_pembayaran" class="form-control">
									 <option value="online">Transfer</option>
									 <option value="offline">Datang Ke Toko</option>
							</select>
            </div>
						<div class="form-group has-feedback"><label>Ongkos Kirim</label>
							<select name="kurir" class="form-control kurir" id="ongkirr" required>
								<option value="">--Silahkan Pilih--</option>
							<?php
							$kurir=array('jne','pos','tiki');
							foreach($kurir as $data_kurir){
							?>
								<option value="<?=$data_kurir;?>"  ><?=strtoupper($data_kurir);?></option>
							<?php } ?>
							</select>
            </div>
						<div class="form-group has-feedback">
							<div id="kuririnfo" style="display: none;"><br>
								<label>Service</label>
								<div class="col-lg-12">
									<p class="form-control-static" id="kurirserviceinfo" ></p>
								</div>
							</div>
            </div>


        </div>
      </div>
			<br><br>
			<table class="table table-striped table-bordered">
				<tbody>
					<tr>
						<th>Total Berat</th>
						<td colspan="2" align="right"><?php echo $total_berat_dan_subtotal->total_berat ?> (gram) / <?php echo berat($total_berat_dan_subtotal->total_berat) ?> (kg)</td>
					</tr>
					<tr>
						<th>SubTotal</th>
						<td></td>
						<td align="right"><?php echo $total_berat_dan_subtotal->subtotal ?></td>
					</tr>
					<?php if ($this->ion_auth->logged_in()): ?>
					<tr>
						<th>Pilih Jenis Pembayaran</th>
							<td>
								<select id="ok" onChange="opsi()" name="jenis_pembayaran">
										 <option value="online">Transfer</option>
										 <option value="offline">Datang Ke Toko</option>
								</select>
							</td>
						<td></td>
					</tr>
					<tr>
						<th>Ongkos Kirim</th>
						<td>Via:
							<select name="kurir" class="kurir" id="ongkirr"  required>
								<option value="">--Silahkan Pilih--</option>
							<?php
							$kurir=array('jne','pos','tiki');
							foreach($kurir as $data_kurir){
							?>
								<option value="<?=$data_kurir;?>"  ><?=strtoupper($data_kurir);?></option>
							<?php } ?>
							</select>

							<div id="kuririnfo" style="display: none;"><br>
								<label>Service</label>
								<div class="col-lg-12">
									<p class="form-control-static" id="kurirserviceinfo" ></p>
								</div>
							</div>
						</td>
						<td align="right"><font id="totalongkir"></font></td>
					</tr>
					<?php endif; ?>
					<tr>
						<th scope="row">Grand Total</th>
						<td align="right">Subtotal + Total Ongkir</td>
						<td align="right"><b><div id="grandtotal"></div></b></td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="total" id="total" value="<?php echo $total_berat_dan_subtotal->subtotal ?>"/>
			<input type="hidden" name="ongkir" id="ongkir" value="0"/>
			<input type="hidden" name="id_trans" value="<?php echo $this->session->userdata('transaksi'); ?>" />
			<button type="submit" name="button" class="btn btn-primary">Beli</button>
			<?php echo form_close() ?>
			<script type="text/javascript">
			$(document).ready(function()
			{
				$(".kurir").each(function(){
					$(this).on("change",function(){
						var did=$(this).val();
						var berat="<?php echo $total_berat_dan_subtotal->total_berat ?>";
						<?php if (isset($customer_data)): ?>
							var kota="<?php echo $customer_data->kota ?>";
						<?php else: ?>
							var kota= $("#kota").val();
						<?php endif; ?>

						$.ajax({
							method: "get",
							dataType:"html",
							url: "<?=base_url();?>cart/kurirdata",
							data: "kurir="+did+"&berat="+berat+"&kota="+kota,
						})
						.done(function(x) {
							$("#kurirserviceinfo").html(x);
							$("#kuririnfo").show();
						})
						.fail(function() {
							$("#kurirserviceinfo").html("");
							$("#kuririnfo").hide();
						});
					});
				});
				hitung();
			});

			function hitung()
			{
				var total=$('#total').val();
				var ongkir=$("#ongkir").val();
				var totalongkir= ongkir;
				var bayar=(parseFloat(total)+parseFloat(totalongkir));
				if(parseFloat(ongkir) > 0)
				{
					$("#oksimpan").show();
				}else{
					$("#oksimpan").hide();
				}
				$("#totalongkir").html(totalongkir);
				$("#grandtotal").html(bayar);
			}

			function opsi(){
			 var st = $("#ok").val();
			 if(st == "offline"){
				document.getElementById("ongkirr").disabled = true;
			 } else {
				document.getElementById("ongkirr").disabled = false;
			 }
			}
			</script>
		</div>

	</div>

  <?php $this->load->view('front/footer'); ?>
