<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>

	<div class="container">
		<div class="row">
	    <div class="col-sm-12 col-lg-12">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
						<li class="breadcrumb-item active">Detail Riwayat Transaksi</li>
				  </ol>
				</nav>
	    </div>
			<?php
			$in = $history_detail_row->no_invoice;
			$dml = "SELECT * FROM `pembayaran` WHERE status_pembayaran = 'diterima' and no_invoice = '$in' ";
			$query = $this->db->query($dml)->row();
			 ?>
	    <div class="col-lg-12"><h1>Detail Riwayat Transaksi</h1><hr>
        <h4>Invoice NO. <?php echo $history_detail_row->no_invoice ?></h4>
				<div class="row">
				  <div class="col-lg-12">
	          <div class="box-body table-responsive padding">
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th style="text-align: center">No.</th>
                    <th style="text-align: center">Produk</th>
										<th style="text-align: center">Harga</th>
                    <th style="text-align: center">Berat</th>
                    <th style="text-align: center">Jumlah Berat (gram)</th>
										<th style="text-align: center">Qty</th>
                    <th style="text-align: center">Total</th>
										<?php if(isset($query->no_invoice) == $history_detail_row->no_invoice){ ?>
                    <th style="text-align: center">Beri Rating</th>
										<?php } ?>
                  </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($history_detail as $history){ ?>
                  <tr>
                    <td style="text-align:center"><?php echo $no++ ?></td>
                    <td style="text-align:left"><?php echo $history->judul_produk ?></a></td>
										<td style="text-align:center"><?php echo $history->harga_diskon ?></td>
                    <td style="text-align:center"><?php echo $history->berat ?></td>
                    <td style="text-align:center"><?php echo $history->total_berat ?></td>
                    <td style="text-align:center"><?php echo $history->total_qty ?></td>
                    <td style="text-align:center"><?php echo $history->subtotal ?></td>

										<?php if(isset($query->no_invoice) == $history_detail_row->no_invoice){ ?>
											<td style="text-align:center">
												<a href="<?php echo base_url('Produk/kasih_rating/').$history->produk_id ?>">
													<button name="update"><img src="<?php echo base_url()."assets/images/company/rating.png" ?>" alt="Smiley face" height="28" width="28"></button>
												</a>
											</td>
										<?php } ?>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
	  			  </div>
	  			</div>
				</div>
		  </div>

			<div class="col-lg-12">
				<table class="table table-striped table-bordered">
      <tbody>
				<tr>
					<th>Total Berat</th>
					<td colspan="2" align="right"><?php echo $history_detail_row->total_berat ?> (gram) / <?php echo berat($history_detail_row->total_berat) ?> (kg)</td>
				</tr>
        <tr>
          <th>SubTotal</th>
          <td></td>
          <td align="right"><?php echo number_format($subtotal_history->subtotal) ?></td>
        </tr>
        <tr>
          <th>Ongkos Kirim</th>
          <td align="right">Via: <?php echo strtoupper($history_detail_row->kurir).' '.$history_detail_row->service ?></td>
          <td align="right"><?php echo number_format($history_detail_row->ongkir) ?></td>
        </tr>
        <tr>
          <th scope="row">Grand Total</th>
          <td align="right">Subtotal + Total Ongkir</td>
          <td align="right"><b><?php echo number_format(ceil(berat($history_detail_row->total_berat)) * $history_detail_row->ongkir + $subtotal_history->subtotal) ?></b></td>
        </tr>
      </tbody>
    </table>
			</div>
	  </div>
	</div>
<?php $this->load->view('front/footer'); ?>
