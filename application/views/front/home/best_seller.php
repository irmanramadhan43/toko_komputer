<hr><h4 align="center">Best Seller</h4><hr>
<?php
  $terlaris = "SELECT produk_id,produk.*,count(total_qty) as laris FROM `transaksi_detail` LEFT JOIN produk on transaksi_detail.produk_id = produk.id_produk GROUP BY produk_id order by laris DESC limit 10";
  $query = $this->db->query($terlaris)->result();


 ?>
<div class="row">
  <?php foreach($query as $produk){ ?>
    <?php
      $sum = "SELECT SUM(tingkat_rating) as mus FROM rating WHERE id_produk = $produk->id_produk";
      $sum_query = $this->db->query($sum)->row();
      $count = "SELECT COUNT(tingkat_rating) as con FROM rating where id_produk = $produk->id_produk";
      $count_query = $this->db->query($count)->row();
      $rating = @($sum_query->mus / $count_query->con);
     ?>
    <?php if(empty($produk->foto)){ ?>
    <?php }else{ ?>
      <div class="col-xl-3 col-lg-4 col-md-12 col-sm-6 col-xs-12">
        <div class="card mb-4 box-shadow">
          <a href="<?php echo base_url("produk/$produk->slug_produk ") ?>">
            <?php
            if(empty($produk->foto)) {echo "<img class='card-img-top' src='".base_url()."assets/images/no_image_thumb.png'>";}
            else { echo "<img class='card-img-top' src='".base_url()."assets/images/produk/".$produk->foto.'_thumb'.$produk->foto_type."'> ";}
            ?>
          </a>
          <div class="card-body">
            <a href="<?php echo base_url("produk/$produk->slug_produk ") ?>">
              <p class="card-text"><b><?php echo character_limiter($produk->judul_produk,50) ?></b></p>
            </a>
            <br>
              <strike><b>Rp <?php echo number_format($produk->harga_normal) ?></b></strike><br>
              <p align="center">
              <b>Rp <?php echo number_format($produk->harga_diskon) ?></b> <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?php echo $produk->diskon ?>% OFF</span></font>
            </p>
            <p align="center">Stok: <?php if($produk->stok > 0){echo "  <b> ".$produk->stok."</b>" ;}else{echo "<font style='font-size:15px'><span class='badge badge-pill badge-primary'>Kosong</span></font>";} ?></p>

            <p align="center">
              <div class="rate">
                <?php for ($i=0; $i < $rating ; $i++) {?>
                  <?php if ($rating == 0): ?>

                  <?php else: ?>
                    <label for="star5" >5 stars</label>
                  <?php endif; ?>

                <?php } ?>

             </div><br><br><br>
              <a href="<?php echo base_url('cart/buy/').$produk->id_produk ?>">
                <button class="btn btn btn-info"><i class="fa fa-shopping-cart"></i> Beli</button>
              </a>
              <a href="<?php echo base_url('produk/').$produk->slug_produk ?>">
                <button class="btn btn btn-danger"><i class="fa fa-eye"></i> Detail</button>
              </a>
              <a href="<?php echo base_url('produk/bandingkan/').$produk->id_produk ?>">
                <button class="btn btn btn-info"><img src="<?php echo base_url()."assets/images/company/banding.png" ?>" alt="Smiley face" height="18" width="18"></button>
              </a>
            </p>
          </div>
        </div>
      </div>

    <?php } ?>

  <?php } ?>
</div>
