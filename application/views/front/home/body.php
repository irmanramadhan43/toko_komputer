


<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>



	<?php $this->load->view('front/home/slider'); ?>
	<select id="dynamic_select">
	  <option value="" selected>Urut Berdasarkan</option>
	  <option value="<?php echo base_url()."home/sortingAZ" ?>">A Z</option>
	  <option value="<?php echo base_url()."home/sortingZA" ?>">Z A</option>
	  <option value="<?php echo base_url()."home/harga_terendah" ?>">Harga Terendah</option>
	  <option value="<?php echo base_url()."home/harga_tertinggi" ?>">Harga Tertinggi</option>
	</select>
		<?php $this->load->view('front/home/best_seller'); ?>
		<?php $this->load->view('front/home/produk_new'); ?>
		<?php $this->load->view('front/home/rekomen'); ?>
	<?php $this->load->view('front/home/blog_new'); ?>

</div>

<?php $this->load->view('front/footer'); ?>
</body>
<script>
    $(function(){
      // bind change event to select
      $('#dynamic_select').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
