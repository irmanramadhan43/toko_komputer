<?php
$this->db->join('produk', 'featured.produk_id = produk.id_produk');
$this->db->order_by('no_urut', 'ASC');
$this->db->limit('5');
$featured_data =  $this->db->get('featured')->result();

 ?>
 <hr><h4 align="center">Rekomendasi Produk</h4><hr>

 <div class="row">
   <?php foreach($featured_data as $produk){ ?>
     <?php
       $sum = "SELECT SUM(tingkat_rating) as mus FROM rating WHERE id_produk = $produk->id_produk";
       $sum_query = $this->db->query($sum)->row();
       $count = "SELECT COUNT(tingkat_rating) as con FROM rating where id_produk = $produk->id_produk";
       $count_query = $this->db->query($count)->row();
       $rating = @($sum_query->mus / $count_query->con);
      ?>
     <?php if(empty($produk->foto)){ ?>
     <?php }else{ ?>
       <div class="col-xl-3 col-lg-4 col-md-12 col-sm-6 col-xs-12">
         <div class="card mb-4 box-shadow">
           <a href="<?php echo base_url("produk/$produk->slug_produk ") ?>">
             <?php
             if(empty($produk->foto)) {echo "<img class='card-img-top' src='".base_url()."assets/images/no_image_thumb.png'>";}
             else { echo "<img class='card-img-top' src='".base_url()."assets/images/produk/".$produk->foto.'_thumb'.$produk->foto_type."'> ";}
             ?>
           </a>
           <div class="card-body">
             <a href="<?php echo base_url("produk/$produk->slug_produk ") ?>">
               <p class="card-text"><b><?php echo character_limiter($produk->judul_produk,50) ?></b></p>
             </a>
             <br>
             <p align="center">
               <strike><b>Rp <?php echo number_format($produk->harga_normal) ?></b></strike><br>
               <b>Rp <?php echo number_format($produk->harga_diskon) ?></b> <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?php echo $produk->diskon ?>% OFF</span></font>
             </p>
             <div class="rate">
               <?php for ($i=0; $i < $rating ; $i++) {?>
                 <?php if ($rating == 0): ?>

                 <?php else: ?>
                   <label for="star5" >5 stars</label>
                 <?php endif; ?>

               <?php } ?>

            </div><br><br><br>
             <p align="center">
               <a href="<?php echo base_url('cart/buy/').$produk->id_produk ?>">
                 <button class="btn btn btn-info"><i class="fa fa-shopping-cart"></i> Beli</button>
               </a>
               <a href="<?php echo base_url('produk/').$produk->slug_produk ?>">
                 <button class="btn btn btn-danger"><i class="fa fa-eye"></i> Detail</button>
               </a>
               <a href="<?php echo base_url('produk/bandingkan/').$produk->id_produk ?>">
                 <button class="btn btn btn-info"><img src="<?php echo base_url()."assets/images/company/banding.png" ?>" alt="Smiley face" height="18" width="18"></button>
               </a>
             </p>
           </div>
         </div>
       </div>

     <?php } ?>

   <?php } ?>
 </div>
