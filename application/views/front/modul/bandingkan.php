<select id="dynamic_select">
  <option value="" selected>Urut Berdasarkan</option>
  <option value="<?php echo base_url()."home/sortingAZ" ?>">A Z</option>
  <option value="<?php echo base_url()."home/sortingZA" ?>">Z A</option>
  <option value="<?php echo base_url()."home/harga_terendah" ?>">Harga Terendah</option>
  <option value="<?php echo base_url()."home/harga_tertinggi" ?>">Harga Tertinggi</option>
</select>
<hr><h4 align="center">Anda Membandingkan</h4><hr>

<div class="row">
  <?php foreach($produk_new_data as $produk){ ?>
    <?php if(empty($produk->foto)){ ?>
    <?php }else{ ?>
      <div class="col-xl-3 col-lg-4 col-md-12 col-sm-6 col-xs-12">
        <div class="card mb-4 box-shadow">
          <a href="<?php echo base_url("produk/$produk->slug_produk ") ?>">
            <?php
            if(empty($produk->foto)) {echo "<img class='card-img-top' src='".base_url()."assets/images/no_image_thumb.png'>";}
            else { echo "<img class='card-img-top' src='".base_url()."assets/images/produk/".$produk->foto.'_thumb'.$produk->foto_type."'> ";}
            ?>
          </a>
          <div class="card-body">
            <a href="<?php echo base_url("produk/$produk->slug_produk ") ?>">
              <p class="card-text"><b><?php echo character_limiter($produk->judul_produk,50) ?></b></p>
            </a>
            <br>
            <p align="center">
              <strike><b>Rp <?php echo number_format($produk->harga_normal) ?></b></strike><br>
              <b>Rp <?php echo number_format($produk->harga_diskon) ?></b> <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?php echo $produk->diskon ?>% OFF</span></font>
            </p>
            <a href="<?php echo base_url("produk/hapus_banding/$produk->id_produk  ") ?>">
              <p class="card-text"><b><?php echo "Hapus" ?></b></p>
            </a>
            </p>
            <hr><h4>Deskripsi Produk</h4><hr>
    					<p><?php echo $produk->deskripsi ?></p>
          </div>
        </div>
      </div>
    <?php } ?>

  <?php } ?>
</div>
<script>
    $(function(){
      // bind change event to select
      $('#dynamic_select').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
