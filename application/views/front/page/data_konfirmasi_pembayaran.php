<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>
<br>
<div class="container">
	<div class="row">
    <div class="col-lg-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
					<li class="breadcrumb-item active">Riwayat Transaksi</li>
			  </ol>
			</nav>
    </div>

    <div class="col-lg-12"><h1>Konfirmasi Pembayaran</h1><hr>
			<div class="row">
			  <div class="col-lg-12">
          <div class="box-body table-responsive padding">
						<?php if(empty($cek->id_pembayaran)){echo "Anda belum ada Konfirmasi Pembayaran";}else{ ?>
            	<table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th style="text-align: center">No.</th>
                  <th style="text-align: center">Invoice</th>
									<th style="text-align: center">Nama</th>
                  <th style="text-align: center">Jumlah</th>
									<th style="text-align: center">Bank Asal</th>
									<th style="text-align: center">Bank Tujuan</th>
                  <th style="text-align: center">Status Pembayaran</th>
                </tr>
              </thead>
              <tbody>
              <?php $no=1; foreach ($konfirm as $history){ ?>
                <tr>
                  <td style="text-align:center"><?php echo $no++ ?></td>
                  <td style="text-align:center"><?php echo $history->no_invoice ?></a></td>
									<td style="text-align:center"><?php echo $history->nama_pengirim ?></td>
									<td style="text-align:center"><?php echo $history->jumlah ?></td>
									<td style="text-align:center"><?php echo $history->bank_asal ?></td>
									<td style="text-align:center"><?php echo $history->bank_tujuan ?></td>
									<td style="text-align:center"><?php echo $history->status_pembayaran ?></td>\
                </tr>
              <?php } ?>
              </tbody>
            </table>
						<?php } ?>
  			  </div>
  			</div>
			</div>
	  </div>
	</div>
</div>

<?php $this->load->view('front/footer'); ?>
