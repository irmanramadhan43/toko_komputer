<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>
<?php
$getbank = "SELECT * FROM bank";
$query = $this->db->query($getbank)->result();

 ?>
<br><br>
<div class="container">
	<div class="row">
    <div class="col-sm-12 col-lg-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
      	  <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
      	  <li class="active">Konfirmasi Pembayaran</li>
      	</ol>
      </nav>
    </div>
		<div class="col-sm-12 col-lg-9"><h1>Konfirmasi Pembayaran</h1><hr>
			<div class="row">
        <div class="col-lg-12">
          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
          <?php echo form_open_multipart($action) ?>
            <div class="form-group has-feedback"><label>No. Invoice</label>
              <input type="text" value="<?php echo $history_detail_row->no_invoice ?>"  name="invoice" class="form-control" readonly>
            </div>
            <div class="form-group has-feedback"><label>Nama Pengirim</label>
              <input type="text" name="nama" class="form-control">
            </div>
            <div class="form-group has-feedback"><label>Jumlah</label>
              <input type="text" name="jumlah" class="form-control">
            </div>
						<div class="form-group">
							<label for="inputName" class="control-label">Gambar</label><br>
							<input type="file" id="input-file-now" name="gambar" class="form-control"  required>
						</div>
            <div class="form-group has-feedback"><label>Bank Asal</label>
							<input type="text" name="bank_asal" class="form-control">

            </div>
            <div class="form-group has-feedback"><label>Bank Tujuan</label>
							<select class="form-control" name="bank_tujuan" id="kategori" required>
								<option value="">Please Select</option>
								<?php foreach ($query as $k ): ?>
									<option value="<?php echo $k->nama_bank; ?>"><?php echo $k->nama_bank; ?></option>
								<?php endforeach; ?>
							</select>
            </div>
            <button type="submit" name="button" class="btn btn-primary">Kirim</button>
          <?php echo form_close() ?>
        </div>
      </div>
		</div>

	</div>

  <?php $this->load->view('front/footer'); ?>
