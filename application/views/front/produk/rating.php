<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>

<select id="dynamic_select">
  <option value="" selected>Urut Berdasarkan</option>
  <option value="<?php echo base_url()."home/sortingAZ" ?>">A Z</option>
  <option value="<?php echo base_url()."home/sortingZA" ?>">Z A</option>
  <option value="<?php echo base_url()."home/harga_terendah" ?>">Harga Terendah</option>
  <option value="<?php echo base_url()."home/harga_tertinggi" ?>">Harga Tertinggi</option>
</select>
<div class="container">
 <div class="row">
    <div class="col-lg-12">
     <nav aria-label="breadcrumb">
       <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
         <li class="breadcrumb-item"><a href="#">Produk</a></li>
         <li class="breadcrumb-item active"><?php echo $produk->judul_produk ?></li>
       </ol>
     </nav>
    </div>
   <!-- Kolom kiri -->
   <div class="col-lg-9">
      <br>
     <?php if(empty($produk->foto)){ ?>
     <?php }else{ ?>
       <h1><?php echo $produk->judul_produk ?></h1><hr>
       <div class="row">
         <div class="col-sm-5" align="center">
           <?php
           if(empty($produk->foto)) {echo "<img class='img-thumbnail' src='".base_url()."assets/images/no_image_thumb.png' width='400' height='400'>";}
           else
           {
             echo "
             <a href='".base_url()."assets/images/produk/".$produk->foto.$produk->foto_type."'>
             <img data-action='zoom' class='img-thumbnail' src='".base_url()."assets/images/produk/".$produk->foto.'_thumb'.$produk->foto_type."' title='$produk->judul_produk' alt='$produk->judul_produk' width='400' height='400'>
             </a>";}
           ?>
           <br>
         </div>
         <div class="col-sm-7"><p><h4>Spesifikasi Produk</h4></p><hr>
           <p>Berat: <?php echo $produk->berat ?> kg</p>
           <p>Harga: Rp <strike><b>Rp <?php echo number_format($produk->harga_normal) ?></b></strike> | <b>Rp <?php echo number_format($produk->harga_diskon) ?></b> <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?php echo $produk->diskon ?>% OFF</span></font></p>
           <p>Stok: <?php if($produk->stok > 0){echo "<font style='font-size:15px'><span class='badge badge-pill badge-success'>Tersedia</span></font>";}else{echo "<font style='font-size:15px'><span class='badge badge-pill badge-primary'>Kosong</span></font>";} ?></p>
           <p>Kategori:
             <a href="<?php echo base_url('kategori/read/').$produk->slug_kat ?>">
               <?php echo $produk->judul_kategori ?>
             </a> /
             <a href="<?php echo base_url('kategori/read/').$produk->slug_kat."/".$produk->slug_subkat ?>">
               <?php echo $produk->judul_subkategori ?>
             </a> /
             <a href="<?php echo base_url('kategori/read/').$produk->slug_kat."/".$produk->slug_subkat."/".$produk->slug_supersubkat ?>">
               <?php echo $produk->judul_supersubkategori ?>

             </a>
             <br><br>
             <a href="<?php echo base_url('cart/buy/').$produk->id_produk ?>">

             </a>
             <?php echo form_open('Produk/kasih_rating_act') ?>
               <input type="hidden" value="<?php echo $produk->id_produk ?>" name="id_produk" class="form-control">
             <div class="rate">
                  <input type="radio" id="star5" name="rate" value="5" />
                  <label for="star5" title="text">5 stars</label>
                  <input type="radio" id="star4" name="rate" value="4" />
                  <label for="star4" title="text">4 stars</label>
                  <input type="radio" id="star3" name="rate" value="3" />
                  <label for="star3" title="text">3 stars</label>
                  <input type="radio" id="star2" name="rate" value="2" />
                  <label for="star2" title="text">2 stars</label>
                  <input type="radio" id="star1" name="rate" value="1" />
                  <label for="star1" title="text">1 star</label>
            </div><br><br><br>
            <div class="form-group has-feedback"><label>Komentar Anda</label>
              <input type="text" name="komentar" class="form-control">
            </div>
            <button type="submit" name="button" class="btn btn-primary">Kirim</button>
            <?php echo form_close() ?>
           </p>
         </div>
       </div>
     <?php } ?>
   </div>
   <script>
       $(function(){
         // bind change event to select
         $('#dynamic_select').on('change', function () {
             var url = $(this).val(); // get selected value
             if (url) { // require a URL
                 window.location = url; // redirect
             }
             return false;
         });
       });
   </script>
   <!-- Kolom kanan -->
<?php $this->load->view('front/footer'); ?>
