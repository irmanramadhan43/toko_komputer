<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>
<br><br><br>
<div class="container">
	<div class="row">
    <div class="col-sm-12 col-lg-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
      	</ol>
      </nav>
    </div>
		<p><br><br>
			<div class="col-sm-12 col-lg-9"><h1>Simulasi Perakitan Komputer</h1><hr>

			<div class="row">
        <div class="col-lg-12">
            <div class="form-group has-feedback">
              <?php echo form_open($action);?>
              <table>
                <tr>
                  <td>Nama Barang</td>
                  <td>QTY</td>
									<td>Harga</td>
                </tr>
								<?php $i=1; ?>
                <?php foreach ($simulasi->result() as $s): ?>
                <tr>
                  <td>
                    <?php
                      $dml="SELECT * FROM produk where supersubkat_id = $s->id_supersubkategori";
                      $query=$this->db->query($dml)->result();
                     ?>
                    <select class="form-control select harga" name="nama_barang[]" id="harga-<?= $i ?>" idHarga="<?= $i ?>" harga='200'>
                        <option value=""><?php echo $s->judul_supersubkategori ?></option>
                        <?php foreach ($query as $e): ?>
                            <option harga="<?= $e->harga_normal ?>" value="<?php echo $e->judul_produk."#".$e->harga_normal  ?>"><?php echo $e->judul_produk ?>  <pre> </pre>  (Rp. <?php echo number_format($e->harga_normal) ?>)</option>
                        <?php endforeach; ?>
                    </select>
                  </td>
                  <td>
                    <select class="form-control select2_1 qty" id="qty-<?= $i ?>" idQty="<?= $i ?>" name="qty[]">
                        <?php for ($j=1; $j < 10; $j++) {?>
                          <option value="<?php echo $j ?>"><?php echo $j ?></option>
                        <?php
                        } ?>
                    </select>
                  </td>
									<td class="">
										<input type="text" class="form-control" id="qtyharga-<?php echo $i; ?>"  readonly>
									</td>
                </tr><br>
								<?php $i++ ?>
              <?php endforeach; ?>
							<input type="hidden" id="sizeArray" value="<?=$simulasi->num_rows() ?>">
							<tr colspan="2">
								<td> </td>
								<td>Total</td>
								<td id="totalHarga"></td>

							</tr>
              </table>
            </div>
            <button type="submit" formtarget="_blank" name="button" class="btn btn-primary">Kirim</button>
          <?php echo form_close() ?>
        </div>
      </div>
		</div>
  </div>

	</div>

  <?php $this->load->view('front/footer'); ?>
