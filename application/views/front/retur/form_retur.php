<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>
<br><br><br>
<div class="container">
	<div class="row">
    <div class="col-sm-12 col-lg-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
      	</ol>
      </nav>
    </div>
		<p><br><br>
			<div class="col-sm-12 col-lg-9"><h1>Form Retur</h1><hr>
			Syarat Retur Barang :<br>
1. Tidak melebihi 3 hari sejak pembelian bila lebih silahkan claim garansi langsung ke
vendor yang bersangkutan.<br>
2. Segel produk masih utuh dan sertakan invoice pembelian <br>
3. Bila barang yang datang dalam keadaan rusak atau packagenya tidak utuh silahkan
claim garansi ke exspedisi yang bersangkutan.
		</p>

			<div class="row">
        <div class="col-lg-12">
          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
          <?php echo form_open($action) ?>
            <div class="form-group has-feedback"><label>No. Invoice</label>
              <input type="text" value="<?php echo $history_detail_row->no_invoice ?>"  name="invoice" class="form-control" readonly>
                <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id') ?>" class="form-control">
            </div>
            <div class="form-group has-feedback"><label>Pesan</label>
              <input type="text" name="pesan" class="form-control">
            </div>
            <div class="form-group has-feedback"><label>Nama Barang</label><br>
                <?php foreach ($barang as $s ): ?>
									<input type='checkbox' class="form-group has-feedback" name='nama_barang[]' value='<?php echo $s->judul_produk." , ";  ?>'><?php echo $s->judul_produk ?><br>
                  <!-- <option value="<?php// echo $s->judul_produk ?>"><?php //echo $s->judul_produk ?></option> -->
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group has-feedback"><label>No Resi (Wajib DIisi)</label>
              <input type="text" name="no_resi" class="form-control">
            </div>
            <div class="form-group has-feedback"><label>Tanggal</label>
              <input type="date" value="<?php echo date('Y-m-d') ?>" name="tanggal" class="form-control">
            </div>
            <button type="submit" name="button" class="btn btn-primary">Kirim</button>
          <?php echo form_close() ?>
        </div>
      </div>
		</div>

	</div>

  <?php $this->load->view('front/footer'); ?>
