<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>
<br><br>
<div class="container">
	<div class="row">
    <div class="col-lg-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
					<li class="breadcrumb-item active">Riwayat Retur</li>
			  </ol>
			</nav>
    </div>

    <div class="col-lg-12"><h1>Riwayat Retur</h1><hr>
			<div class="row">
			  <div class="col-lg-12">
          <div class="box-body table-responsive padding">
						<?php if(empty($retur_row->no_invoice)){echo "Anda belum ada retur";}else{ ?>
            	<table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th style="text-align: center">No.</th>
                  <th style="text-align: center">Invoice</th>
									<th style="text-align: center">Resi customer</th>
									<th style="text-align: center">Resi Toko</th>
                  <th style="text-align: center">Pesan</th>
                  <th style="text-align: center">Pesan dari toko</th>
									<th style="text-align: center">Nama Barang</th>
                  <th style="text-align: center">Tanggal</th>
                  <th style="text-align: center">Status</th>
                  <th style="text-align: center">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php $no=1; foreach ($retur as $history){ ?>
                <tr>
                  <td style="text-align:center"><?php echo $no++ ?></td>
                  <td style="text-align:center"><?php echo $history->no_invoice ?></td>
									<td style="text-align:center"><?php echo $history->no_resi ?></td>
									<td style="text-align:center"><?php echo $history->no_resi_admin ?></td>
									<td style="text-align:center"><?php echo $history->pesan ?></td>
									<td style="text-align:center"><?php echo $history->pesan_admin ?></td>
									<td style="text-align:center"><?php echo $history->barang ?></td>
									<td style="text-align:center"><?php echo $history->tanggal ?></td>
									<td style="text-align:center">
                    <button name="update" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-zoom-in"></i> <?php echo $history->status_customer ?></button></td>
									<td style="text-align:center">
                    <a href="<?php// echo base_url('cart/history_detail/').$history->id_trans ?>">
                      <button name="update" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-zoom-in"></i> Detail</button>
                    </a>
                    <?php if($history->status_customer == "Dikirim"){ ?>
                      <a href="<?php// echo base_url('cart/history_detail/').$history->id_trans ?>">
                      <button name="update" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-zoom-in"></i> Terima Barang</button>
                    </a>
                    <?php } ?>
									</td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
						<?php } ?>
  			  </div>
  			</div>
			</div>
	  </div>
	</div>
</div>

<?php $this->load->view('front/footer'); ?>
