-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Jul 2019 pada 04.38
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bank`
--

CREATE TABLE `bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `atas_nama` varchar(100) NOT NULL,
  `norek` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bank`
--

INSERT INTO `bank` (`id_bank`, `nama_bank`, `atas_nama`, `norek`, `logo`) VALUES
(1, 'BNI', 'Microtron', '12345678', 'bni.png'),
(2, 'BRI', 'Microtron', '87873412323', 'bri.png'),
(3, 'Mandiri', 'Microtron', '778734098', 'mandiri.png'),
(4, 'BCA', 'Microtron', '998980342487', 'bca.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id_blog` int(11) NOT NULL,
  `judul_blog` varchar(100) NOT NULL,
  `slug_blog` varchar(100) NOT NULL DEFAULT '0',
  `isi_blog` text,
  `foto` text,
  `foto_type` char(10) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id_blog`, `judul_blog`, `slug_blog`, `isi_blog`, `foto`, `foto_type`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'test', 'test', '<p><strong>Note:</strong> We have also made the navbar responsive, resize the browser window to see the effect.</p>\r\n<p>Lorem ipsum dolor dummy text to enable scrolling, sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'test20190526011640', '.PNG', '2019-05-26 01:16:41', 'administrator', NULL, ''),
(2, 'test 2', 'test-2', '<p><strong>Note:</strong> We have also made the navbar responsive, resize the browser window to see the effect.</p>\r\n<p>Lorem ipsum dolor dummy text to enable scrolling, sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'test-220190526011658', '.PNG', '2019-05-26 01:16:58', 'administrator', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `company`
--

CREATE TABLE `company` (
  `id_company` int(11) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_desc` text NOT NULL,
  `company_address` text NOT NULL,
  `company_maps` text NOT NULL,
  `company_phone` char(30) NOT NULL,
  `company_phone2` char(30) NOT NULL,
  `company_fax` char(30) NOT NULL,
  `company_email` char(30) NOT NULL,
  `foto` text NOT NULL,
  `foto_type` char(10) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL,
  `created_by` char(50) NOT NULL,
  `modified_by` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `company`
--

INSERT INTO `company` (`id_company`, `company_name`, `company_desc`, `company_address`, `company_maps`, `company_phone`, `company_phone2`, `company_fax`, `company_email`, `foto`, `foto_type`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'FS KOMPUTER', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel nibh ac nisl porttitor tempus sit amet et diam. Etiam sed leo eu elit varius venenatis sed ac arcu. Praesent malesuada gravida diam et tincidunt. Mauris quis metus eget magna efficitur scelerisque. Sed mollis porttitor erat ullamcorper sodales. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse congue, dolor ultricies mollis molestie, libero diam auctor mauris, ultrices euismod leo justo vel enim. Etiam non rutrum arcu. Maecenas at dictum dui, sit amet gravida mauris. Vivamus sagittis neque in purus dapibus, ut pellentesque purus pulvinar. Nunc pretium porta ipsum, at iaculis felis elementum in. Duis cursus ex vitae nunc hendrerit blandit.\r\n\r\nMorbi vel est sed dui tristique elementum sed sed purus. Ut interdum nisi et felis vulputate, quis tempus diam blandit. Mauris tincidunt tellus faucibus, posuere turpis a, consectetur lacus. Nullam quis ipsum neque. Praesent sapien tellus, molestie et diam vel, cursus tristique neque. Nullam sit amet ornare odio. Ut vehicula risus id lacus blandit rutrum. Duis non molestie purus. Etiam turpis ligula, tincidunt sit amet dolor at, rutrum viverra orci. Etiam egestas urna id velit bibendum mollis.\r\n\r\nSed eu sem cursus, congue massa at, bibendum leo. Praesent cursus in nulla a egestas. Fusce aliquam leo eu enim feugiat ullamcorper. Nullam pulvinar dolor eu lacinia bibendum. Integer id ipsum cursus, luctus enim nec, fringilla dolor. Sed sit amet ipsum sit amet quam suscipit gravida vitae ut elit. Donec pellentesque non tortor vitae euismod. Praesent suscipit tempor ex ac viverra. Nunc ut sapien eu velit tempor hendrerit. Vestibulum posuere nisl massa, ornare commodo lorem sagittis ultrices. Sed eget rutrum neque, sed ullamcorper dui. Sed ultricies purus vitae lectus cursus, vestibulum faucibus quam posuere. Donec cursus vitae ipsum nec ullamcorper. Donec maximus orci finibus ante hendrerit, vitae maximus quam facilisis. Cras commodo fringilla porttitor.\r\n\r\nNam pharetra a tortor quis venenatis. Nunc lectus nibh, auctor id ante vel, interdum maximus felis. Cras libero est, mattis a sollicitudin sit amet, ultricies sed tellus. Ut augue lacus, luctus convallis enim quis, ultricies aliquet sem. Sed venenatis eros sit amet velit varius, ac rhoncus nibh sodales. Etiam sit amet efficitur est, vel pretium arcu. Morbi diam nulla, dictum quis ornare ultrices, pharetra quis mi. Nam sollicitudin pharetra congue. Praesent sed mauris at ante tincidunt blandit. Aliquam cursus ante efficitur, iaculis turpis eget, ornare quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla quis lobortis leo. Cras ut risus orci. Sed mattis purus ac libero suscipit, nec venenatis tortor semper. Aliquam sodales massa eget dignissim pharetra.\r\n\r\nNam sed enim vitae erat vulputate feugiat in tempus metus. In maximus erat risus. Donec et viverra nibh. Maecenas hendrerit, sapien id suscipit fermentum, tellus nisl sollicitudin erat, non laoreet dui ex sit amet odio. Nullam sit amet arcu sed felis tempor dapibus. Aliquam erat volutpat. Aenean malesuada a eros sed aliquet. Phasellus condimentum lobortis sapien, sit amet viverra sem iaculis venenatis. Morbi interdum nulla ut nulla fringilla commodo. In eu magna ornare libero pellentesque congue. Vestibulum ultrices congue feugiat.', 'foursquare computer   BTM  Lant  2 Block  DD No  27  Jl. Ibrahim Adjie No. 40 Bandung', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16327777.649419477!2d108.84621849858628!3d-2.415291213289622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2c4c07d7496404b7%3A0xe37b4de71badf485!2sIndonesia!5e0!3m2!1sen!2sid!4v1506312173230\" width=\"100%\" height=\"200\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '087725657897', '0711412402', '24141', 'toko@gmail.com', 'fs-komputer20190526020003', '.png', '2017-11-09 06:45:34', NULL, '', 'administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `featured`
--

CREATE TABLE `featured` (
  `id_featured` int(11) NOT NULL,
  `no_urut` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(10) NOT NULL,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `featured`
--

INSERT INTO `featured` (`id_featured`, `no_urut`, `produk_id`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 1, 13, '2019-05-22 16:53:53', 'administra', NULL, ''),
(2, 2, 12, '2019-05-22 16:54:04', 'administra', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) NOT NULL,
  `name` varchar(8) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `judul_kategori` varchar(20) NOT NULL,
  `slug_kat` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `judul_kategori`, `slug_kat`) VALUES
(3, 'Prosesor', 'prosesor'),
(4, 'RAM', 'ram'),
(5, 'VGA', 'vga'),
(6, 'Strorage', 'strorage');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE `kontak` (
  `id_kontak` int(11) NOT NULL,
  `nama` char(50) NOT NULL,
  `nohp` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`id_kontak`, `nama`, `nohp`) VALUES
(1, 'Faisal', '6281228289776'),
(2, 'Irman Ramadhan', '087725657897');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota`
--

CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `nama_kota` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kota`
--

INSERT INTO `kota` (`id_kota`, `provinsi_id`, `nama_kota`) VALUES
(1, 21, 'Aceh Barat'),
(2, 21, 'Aceh Barat Daya'),
(3, 21, 'Aceh Besar'),
(4, 21, 'Aceh Jaya'),
(5, 21, 'Aceh Selatan'),
(6, 21, 'Aceh Singkil'),
(7, 21, 'Aceh Tamiang'),
(8, 21, 'Aceh Tengah'),
(9, 21, 'Aceh Tenggara'),
(10, 21, 'Aceh Timur'),
(11, 21, 'Aceh Utara'),
(12, 32, 'Agam'),
(13, 23, 'Alor'),
(14, 19, 'Ambon'),
(15, 34, 'Asahan'),
(16, 24, 'Asmat'),
(17, 1, 'Badung'),
(18, 13, 'Balangan'),
(19, 15, 'Balikpapan'),
(20, 21, 'Banda Aceh'),
(21, 18, 'Bandar Lampung'),
(22, 9, 'Bandung'),
(23, 9, 'Bandung'),
(24, 9, 'Bandung Barat'),
(25, 29, 'Banggai'),
(26, 29, 'Banggai Kepulauan'),
(27, 2, 'Bangka'),
(28, 2, 'Bangka Barat'),
(29, 2, 'Bangka Selatan'),
(30, 2, 'Bangka Tengah'),
(31, 11, 'Bangkalan'),
(32, 1, 'Bangli'),
(33, 13, 'Banjar'),
(34, 9, 'Banjar'),
(35, 13, 'Banjarbaru'),
(36, 13, 'Banjarmasin'),
(37, 10, 'Banjarnegara'),
(38, 28, 'Bantaeng'),
(39, 5, 'Bantul'),
(40, 33, 'Banyuasin'),
(41, 10, 'Banyumas'),
(42, 11, 'Banyuwangi'),
(43, 13, 'Barito Kuala'),
(44, 14, 'Barito Selatan'),
(45, 14, 'Barito Timur'),
(46, 14, 'Barito Utara'),
(47, 28, 'Barru'),
(48, 17, 'Batam'),
(49, 10, 'Batang'),
(50, 8, 'Batang Hari'),
(51, 11, 'Batu'),
(52, 34, 'Batu Bara'),
(53, 30, 'Bau-Bau'),
(54, 9, 'Bekasi'),
(55, 9, 'Bekasi'),
(56, 2, 'Belitung'),
(57, 2, 'Belitung Timur'),
(58, 23, 'Belu'),
(59, 21, 'Bener Meriah'),
(60, 26, 'Bengkalis'),
(61, 12, 'Bengkayang'),
(62, 4, 'Bengkulu'),
(63, 4, 'Bengkulu Selatan'),
(64, 4, 'Bengkulu Tengah'),
(65, 4, 'Bengkulu Utara'),
(66, 15, 'Berau'),
(67, 24, 'Biak Numfor'),
(68, 22, 'Bima'),
(69, 22, 'Bima'),
(70, 34, 'Binjai'),
(71, 17, 'Bintan'),
(72, 21, 'Bireuen'),
(73, 31, 'Bitung'),
(74, 11, 'Blitar'),
(75, 11, 'Blitar'),
(76, 10, 'Blora'),
(77, 7, 'Boalemo'),
(78, 9, 'Bogor'),
(79, 9, 'Bogor'),
(80, 11, 'Bojonegoro'),
(81, 31, 'Bolaang Mongondow (Bolmong)'),
(82, 31, 'Bolaang Mongondow Selatan'),
(83, 31, 'Bolaang Mongondow Timur'),
(84, 31, 'Bolaang Mongondow Utara'),
(85, 30, 'Bombana'),
(86, 11, 'Bondowoso'),
(87, 28, 'Bone'),
(88, 7, 'Bone Bolango'),
(89, 15, 'Bontang'),
(90, 24, 'Boven Digoel'),
(91, 10, 'Boyolali'),
(92, 10, 'Brebes'),
(93, 32, 'Bukittinggi'),
(94, 1, 'Buleleng'),
(95, 28, 'Bulukumba'),
(96, 16, 'Bulungan (Bulongan)'),
(97, 8, 'Bungo'),
(98, 29, 'Buol'),
(99, 19, 'Buru'),
(100, 19, 'Buru Selatan'),
(101, 30, 'Buton'),
(102, 30, 'Buton Utara'),
(103, 9, 'Ciamis'),
(104, 9, 'Cianjur'),
(105, 10, 'Cilacap'),
(106, 3, 'Cilegon'),
(107, 9, 'Cimahi'),
(108, 9, 'Cirebon'),
(109, 9, 'Cirebon'),
(110, 34, 'Dairi'),
(111, 24, 'Deiyai (Deliyai)'),
(112, 34, 'Deli Serdang'),
(113, 10, 'Demak'),
(114, 1, 'Denpasar'),
(115, 9, 'Depok'),
(116, 32, 'Dharmasraya'),
(117, 24, 'Dogiyai'),
(118, 22, 'Dompu'),
(119, 29, 'Donggala'),
(120, 26, 'Dumai'),
(121, 33, 'Empat Lawang'),
(122, 23, 'Ende'),
(123, 28, 'Enrekang'),
(124, 25, 'Fakfak'),
(125, 23, 'Flores Timur'),
(126, 9, 'Garut'),
(127, 21, 'Gayo Lues'),
(128, 1, 'Gianyar'),
(129, 7, 'Gorontalo'),
(130, 7, 'Gorontalo'),
(131, 7, 'Gorontalo Utara'),
(132, 28, 'Gowa'),
(133, 11, 'Gresik'),
(134, 10, 'Grobogan'),
(135, 5, 'Gunung Kidul'),
(136, 14, 'Gunung Mas'),
(137, 34, 'Gunungsitoli'),
(138, 20, 'Halmahera Barat'),
(139, 20, 'Halmahera Selatan'),
(140, 20, 'Halmahera Tengah'),
(141, 20, 'Halmahera Timur'),
(142, 20, 'Halmahera Utara'),
(143, 13, 'Hulu Sungai Selatan'),
(144, 13, 'Hulu Sungai Tengah'),
(145, 13, 'Hulu Sungai Utara'),
(146, 34, 'Humbang Hasundutan'),
(147, 26, 'Indragiri Hilir'),
(148, 26, 'Indragiri Hulu'),
(149, 9, 'Indramayu'),
(150, 24, 'Intan Jaya'),
(151, 6, 'Jakarta Barat'),
(152, 6, 'Jakarta Pusat'),
(153, 6, 'Jakarta Selatan'),
(154, 6, 'Jakarta Timur'),
(155, 6, 'Jakarta Utara'),
(156, 8, 'Jambi'),
(157, 24, 'Jayapura'),
(158, 24, 'Jayapura'),
(159, 24, 'Jayawijaya'),
(160, 11, 'Jember'),
(161, 1, 'Jembrana'),
(162, 28, 'Jeneponto'),
(163, 10, 'Jepara'),
(164, 11, 'Jombang'),
(165, 25, 'Kaimana'),
(166, 26, 'Kampar'),
(167, 14, 'Kapuas'),
(168, 12, 'Kapuas Hulu'),
(169, 10, 'Karanganyar'),
(170, 1, 'Karangasem'),
(171, 9, 'Karawang'),
(172, 17, 'Karimun'),
(173, 34, 'Karo'),
(174, 14, 'Katingan'),
(175, 4, 'Kaur'),
(176, 12, 'Kayong Utara'),
(177, 10, 'Kebumen'),
(178, 11, 'Kediri'),
(179, 11, 'Kediri'),
(180, 24, 'Keerom'),
(181, 10, 'Kendal'),
(182, 30, 'Kendari'),
(183, 4, 'Kepahiang'),
(184, 17, 'Kepulauan Anambas'),
(185, 19, 'Kepulauan Aru'),
(186, 32, 'Kepulauan Mentawai'),
(187, 26, 'Kepulauan Meranti'),
(188, 31, 'Kepulauan Sangihe'),
(189, 6, 'Kepulauan Seribu'),
(190, 31, 'Kepulauan Siau Tagulandang Biaro (Sitaro)'),
(191, 20, 'Kepulauan Sula'),
(192, 31, 'Kepulauan Talaud'),
(193, 24, 'Kepulauan Yapen (Yapen Waropen)'),
(194, 8, 'Kerinci'),
(195, 12, 'Ketapang'),
(196, 10, 'Klaten'),
(197, 1, 'Klungkung'),
(198, 30, 'Kolaka'),
(199, 30, 'Kolaka Utara'),
(200, 30, 'Konawe'),
(201, 30, 'Konawe Selatan'),
(202, 30, 'Konawe Utara'),
(203, 13, 'Kotabaru'),
(204, 31, 'Kotamobagu'),
(205, 14, 'Kotawaringin Barat'),
(206, 14, 'Kotawaringin Timur'),
(207, 26, 'Kuantan Singingi'),
(208, 12, 'Kubu Raya'),
(209, 10, 'Kudus'),
(210, 5, 'Kulon Progo'),
(211, 9, 'Kuningan'),
(212, 23, 'Kupang'),
(213, 23, 'Kupang'),
(214, 15, 'Kutai Barat'),
(215, 15, 'Kutai Kartanegara'),
(216, 15, 'Kutai Timur'),
(217, 34, 'Labuhan Batu'),
(218, 34, 'Labuhan Batu Selatan'),
(219, 34, 'Labuhan Batu Utara'),
(220, 33, 'Lahat'),
(221, 14, 'Lamandau'),
(222, 11, 'Lamongan'),
(223, 18, 'Lampung Barat'),
(224, 18, 'Lampung Selatan'),
(225, 18, 'Lampung Tengah'),
(226, 18, 'Lampung Timur'),
(227, 18, 'Lampung Utara'),
(228, 12, 'Landak'),
(229, 34, 'Langkat'),
(230, 21, 'Langsa'),
(231, 24, 'Lanny Jaya'),
(232, 3, 'Lebak'),
(233, 4, 'Lebong'),
(234, 23, 'Lembata'),
(235, 21, 'Lhokseumawe'),
(236, 32, 'Lima Puluh Koto/Kota'),
(237, 17, 'Lingga'),
(238, 22, 'Lombok Barat'),
(239, 22, 'Lombok Tengah'),
(240, 22, 'Lombok Timur'),
(241, 22, 'Lombok Utara'),
(242, 33, 'Lubuk Linggau'),
(243, 11, 'Lumajang'),
(244, 28, 'Luwu'),
(245, 28, 'Luwu Timur'),
(246, 28, 'Luwu Utara'),
(247, 11, 'Madiun'),
(248, 11, 'Madiun'),
(249, 10, 'Magelang'),
(250, 10, 'Magelang'),
(251, 11, 'Magetan'),
(252, 9, 'Majalengka'),
(253, 27, 'Majene'),
(254, 28, 'Makassar'),
(255, 11, 'Malang'),
(256, 11, 'Malang'),
(257, 16, 'Malinau'),
(258, 19, 'Maluku Barat Daya'),
(259, 19, 'Maluku Tengah'),
(260, 19, 'Maluku Tenggara'),
(261, 19, 'Maluku Tenggara Barat'),
(262, 27, 'Mamasa'),
(263, 24, 'Mamberamo Raya'),
(264, 24, 'Mamberamo Tengah'),
(265, 27, 'Mamuju'),
(266, 27, 'Mamuju Utara'),
(267, 31, 'Manado'),
(268, 34, 'Mandailing Natal'),
(269, 23, 'Manggarai'),
(270, 23, 'Manggarai Barat'),
(271, 23, 'Manggarai Timur'),
(272, 25, 'Manokwari'),
(273, 25, 'Manokwari Selatan'),
(274, 24, 'Mappi'),
(275, 28, 'Maros'),
(276, 22, 'Mataram'),
(277, 25, 'Maybrat'),
(278, 34, 'Medan'),
(279, 12, 'Melawi'),
(280, 8, 'Merangin'),
(281, 24, 'Merauke'),
(282, 18, 'Mesuji'),
(283, 18, 'Metro'),
(284, 24, 'Mimika'),
(285, 31, 'Minahasa'),
(286, 31, 'Minahasa Selatan'),
(287, 31, 'Minahasa Tenggara'),
(288, 31, 'Minahasa Utara'),
(289, 11, 'Mojokerto'),
(290, 11, 'Mojokerto'),
(291, 29, 'Morowali'),
(292, 33, 'Muara Enim'),
(293, 8, 'Muaro Jambi'),
(294, 4, 'Muko Muko'),
(295, 30, 'Muna'),
(296, 14, 'Murung Raya'),
(297, 33, 'Musi Banyuasin'),
(298, 33, 'Musi Rawas'),
(299, 24, 'Nabire'),
(300, 21, 'Nagan Raya'),
(301, 23, 'Nagekeo'),
(302, 17, 'Natuna'),
(303, 24, 'Nduga'),
(304, 23, 'Ngada'),
(305, 11, 'Nganjuk'),
(306, 11, 'Ngawi'),
(307, 34, 'Nias'),
(308, 34, 'Nias Barat'),
(309, 34, 'Nias Selatan'),
(310, 34, 'Nias Utara'),
(311, 16, 'Nunukan'),
(312, 33, 'Ogan Ilir'),
(313, 33, 'Ogan Komering Ilir'),
(314, 33, 'Ogan Komering Ulu'),
(315, 33, 'Ogan Komering Ulu Selatan'),
(316, 33, 'Ogan Komering Ulu Timur'),
(317, 11, 'Pacitan'),
(318, 32, 'Padang'),
(319, 34, 'Padang Lawas'),
(320, 34, 'Padang Lawas Utara'),
(321, 32, 'Padang Panjang'),
(322, 32, 'Padang Pariaman'),
(323, 34, 'Padang Sidempuan'),
(324, 33, 'Pagar Alam'),
(325, 34, 'Pakpak Bharat'),
(326, 14, 'Palangka Raya'),
(327, 33, 'Palembang'),
(328, 28, 'Palopo'),
(329, 29, 'Palu'),
(330, 11, 'Pamekasan'),
(331, 3, 'Pandeglang'),
(332, 9, 'Pangandaran'),
(333, 28, 'Pangkajene Kepulauan'),
(334, 2, 'Pangkal Pinang'),
(335, 24, 'Paniai'),
(336, 28, 'Parepare'),
(337, 32, 'Pariaman'),
(338, 29, 'Parigi Moutong'),
(339, 32, 'Pasaman'),
(340, 32, 'Pasaman Barat'),
(341, 15, 'Paser'),
(342, 11, 'Pasuruan'),
(343, 11, 'Pasuruan'),
(344, 10, 'Pati'),
(345, 32, 'Payakumbuh'),
(346, 25, 'Pegunungan Arfak'),
(347, 24, 'Pegunungan Bintang'),
(348, 10, 'Pekalongan'),
(349, 10, 'Pekalongan'),
(350, 26, 'Pekanbaru'),
(351, 26, 'Pelalawan'),
(352, 10, 'Pemalang'),
(353, 34, 'Pematang Siantar'),
(354, 15, 'Penajam Paser Utara'),
(355, 18, 'Pesawaran'),
(356, 18, 'Pesisir Barat'),
(357, 32, 'Pesisir Selatan'),
(358, 21, 'Pidie'),
(359, 21, 'Pidie Jaya'),
(360, 28, 'Pinrang'),
(361, 7, 'Pohuwato'),
(362, 27, 'Polewali Mandar'),
(363, 11, 'Ponorogo'),
(364, 12, 'Pontianak'),
(365, 12, 'Pontianak'),
(366, 29, 'Poso'),
(367, 33, 'Prabumulih'),
(368, 18, 'Pringsewu'),
(369, 11, 'Probolinggo'),
(370, 11, 'Probolinggo'),
(371, 14, 'Pulang Pisau'),
(372, 20, 'Pulau Morotai'),
(373, 24, 'Puncak'),
(374, 24, 'Puncak Jaya'),
(375, 10, 'Purbalingga'),
(376, 9, 'Purwakarta'),
(377, 10, 'Purworejo'),
(378, 25, 'Raja Ampat'),
(379, 4, 'Rejang Lebong'),
(380, 10, 'Rembang'),
(381, 26, 'Rokan Hilir'),
(382, 26, 'Rokan Hulu'),
(383, 23, 'Rote Ndao'),
(384, 21, 'Sabang'),
(385, 23, 'Sabu Raijua'),
(386, 10, 'Salatiga'),
(387, 15, 'Samarinda'),
(388, 12, 'Sambas'),
(389, 34, 'Samosir'),
(390, 11, 'Sampang'),
(391, 12, 'Sanggau'),
(392, 24, 'Sarmi'),
(393, 8, 'Sarolangun'),
(394, 32, 'Sawah Lunto'),
(395, 12, 'Sekadau'),
(396, 28, 'Selayar (Kepulauan Selayar)'),
(397, 4, 'Seluma'),
(398, 10, 'Semarang'),
(399, 10, 'Semarang'),
(400, 19, 'Seram Bagian Barat'),
(401, 19, 'Seram Bagian Timur'),
(402, 3, 'Serang'),
(403, 3, 'Serang'),
(404, 34, 'Serdang Bedagai'),
(405, 14, 'Seruyan'),
(406, 26, 'Siak'),
(407, 34, 'Sibolga'),
(408, 28, 'Sidenreng Rappang/Rapang'),
(409, 11, 'Sidoarjo'),
(410, 29, 'Sigi'),
(411, 32, 'Sijunjung (Sawah Lunto Sijunjung)'),
(412, 23, 'Sikka'),
(413, 34, 'Simalungun'),
(414, 21, 'Simeulue'),
(415, 12, 'Singkawang'),
(416, 28, 'Sinjai'),
(417, 12, 'Sintang'),
(418, 11, 'Situbondo'),
(419, 5, 'Sleman'),
(420, 32, 'Solok'),
(421, 32, 'Solok'),
(422, 32, 'Solok Selatan'),
(423, 28, 'Soppeng'),
(424, 25, 'Sorong'),
(425, 25, 'Sorong'),
(426, 25, 'Sorong Selatan'),
(427, 10, 'Sragen'),
(428, 9, 'Subang'),
(429, 21, 'Subulussalam'),
(430, 9, 'Sukabumi'),
(431, 9, 'Sukabumi'),
(432, 14, 'Sukamara'),
(433, 10, 'Sukoharjo'),
(434, 23, 'Sumba Barat'),
(435, 23, 'Sumba Barat Daya'),
(436, 23, 'Sumba Tengah'),
(437, 23, 'Sumba Timur'),
(438, 22, 'Sumbawa'),
(439, 22, 'Sumbawa Barat'),
(440, 9, 'Sumedang'),
(441, 11, 'Sumenep'),
(442, 8, 'Sungaipenuh'),
(443, 24, 'Supiori'),
(444, 11, 'Surabaya'),
(445, 10, 'Surakarta (Solo)'),
(446, 13, 'Tabalong'),
(447, 1, 'Tabanan'),
(448, 28, 'Takalar'),
(449, 25, 'Tambrauw'),
(450, 16, 'Tana Tidung'),
(451, 28, 'Tana Toraja'),
(452, 13, 'Tanah Bumbu'),
(453, 32, 'Tanah Datar'),
(454, 13, 'Tanah Laut'),
(455, 3, 'Tangerang'),
(456, 3, 'Tangerang'),
(457, 3, 'Tangerang Selatan'),
(458, 18, 'Tanggamus'),
(459, 34, 'Tanjung Balai'),
(460, 8, 'Tanjung Jabung Barat'),
(461, 8, 'Tanjung Jabung Timur'),
(462, 17, 'Tanjung Pinang'),
(463, 34, 'Tapanuli Selatan'),
(464, 34, 'Tapanuli Tengah'),
(465, 34, 'Tapanuli Utara'),
(466, 13, 'Tapin'),
(467, 16, 'Tarakan'),
(468, 9, 'Tasikmalaya'),
(469, 9, 'Tasikmalaya'),
(470, 34, 'Tebing Tinggi'),
(471, 8, 'Tebo'),
(472, 10, 'Tegal'),
(473, 10, 'Tegal'),
(474, 25, 'Teluk Bintuni'),
(475, 25, 'Teluk Wondama'),
(476, 10, 'Temanggung'),
(477, 20, 'Ternate'),
(478, 20, 'Tidore Kepulauan'),
(479, 23, 'Timor Tengah Selatan'),
(480, 23, 'Timor Tengah Utara'),
(481, 34, 'Toba Samosir'),
(482, 29, 'Tojo Una-Una'),
(483, 29, 'Toli-Toli'),
(484, 24, 'Tolikara'),
(485, 31, 'Tomohon'),
(486, 28, 'Toraja Utara'),
(487, 11, 'Trenggalek'),
(488, 19, 'Tual'),
(489, 11, 'Tuban'),
(490, 18, 'Tulang Bawang'),
(491, 18, 'Tulang Bawang Barat'),
(492, 11, 'Tulungagung'),
(493, 28, 'Wajo'),
(494, 30, 'Wakatobi'),
(495, 24, 'Waropen'),
(496, 18, 'Way Kanan'),
(497, 10, 'Wonogiri'),
(498, 10, 'Wonosobo'),
(499, 24, 'Yahukimo'),
(500, 24, 'Yalimo'),
(501, 5, 'Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `bukti_pembayaran` varchar(60) NOT NULL,
  `nama_pengirim` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bank_asal` varchar(20) NOT NULL,
  `bank_tujuan` varchar(20) NOT NULL,
  `status_pembayaran` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `user_id`, `no_invoice`, `bukti_pembayaran`, `nama_pengirim`, `jumlah`, `bank_asal`, `bank_tujuan`, `status_pembayaran`) VALUES
(24, 24, 'FS220719004', '1112.jpg', 'iqballlshorfana', 900000, 'BNI', 'BRI', 'Diterima'),
(25, 24, 'FS220719005', '1_(3)2.png', 'ballll', 16000000, 'BNI', 'BCA', 'Diterima'),
(26, 24, 'FS220719006', '1_(5)4.png', 'muhammad', 8900000, 'Bank Mega', 'BNI', 'Diterima'),
(27, 24, 'FS220719007', '1_(2).png', 'bal On', 92012398, 'Bank BCA', 'BRI', 'Diterima'),
(28, 24, 'FS230719010', '12.png', 'iqbal shorfana', 9000000, 'BNI', 'BRI', 'Diterima'),
(29, 24, 'FS230719011', '1_(2)1.png', 'iqbalbalbalbal', 18623723, 'BNI', 'BNI', 'Diterima'),
(30, 24, 'FS230719012', '1_(3)3.png', 'iqballablablab', 1230929, 'BNI', 'BRI', 'Diterima'),
(31, 24, 'FS230719014', '1_(3)4.png', 'balbalbalbalbal', 123123, 'BRI', 'BRI', 'Diterima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `kode_pembelian` varchar(20) NOT NULL,
  `tanggal_order` date NOT NULL,
  `id_supp` varchar(5) NOT NULL,
  `tanggal_datang` date NOT NULL,
  `total_pembelian` int(11) NOT NULL,
  `status_beli` varchar(35) NOT NULL,
  `no_nota` varchar(50) NOT NULL,
  `kat_id` int(11) NOT NULL,
  `subkat_id` int(11) NOT NULL,
  `supersubkat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `kode_pembelian`, `tanggal_order`, `id_supp`, `tanggal_datang`, `total_pembelian`, `status_beli`, `no_nota`, `kat_id`, `subkat_id`, `supersubkat_id`) VALUES
(1, 'PO220719001', '2019-07-22', 'SP3', '2019-07-22', 2200000, 'Diterima', '', 3, 8, 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pemdet` int(11) NOT NULL,
  `id_pembelian` varchar(50) NOT NULL,
  `judul_produk` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `jumlah_order` int(5) NOT NULL,
  `jumlah_datang` int(5) NOT NULL,
  `subtotal_beli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pemdet`, `id_pembelian`, `judul_produk`, `deskripsi`, `harga_barang`, `harga_jual`, `jumlah_order`, `jumlah_datang`, `subtotal_beli`) VALUES
(21, '1', 'intel i5 9700', '', 1200000, 1300000, 14, 1, 1200000),
(22, '1', 'Ryzen™ 7', 'Pinnacle Ridge nampak jelas memiliki clockspeed yang lumayan ditingkatkan dari Ryzen Generasi pertama ‘Summit Ridge’. Pada Ryzen 7 2700X, AMD menyatakan clockspeed maksimum saat boost adalah 4.3Ghz. Jika Anda menyaksikan berbagai artikel overclocking yang dibuat oleh rekan-rekan kami di JagatOC, nilai 4.3Ghz ini jelas merupakan clockspeed yang belum bisa dicapai Ryzen generasi sebelumnya , dan ini mengindikasikan bahwa AMD melakukan sejumlah optimalisasi pada proses fabrikasi yang mereka pakai untuk Ryzen 2nd Gen.', 1000000, 2000000, 20, 1, 1000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `judul_produk` varchar(100) NOT NULL,
  `slug_produk` varchar(50) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `berat` int(11) NOT NULL,
  `kat_id` int(11) NOT NULL,
  `subkat_id` int(11) NOT NULL,
  `supersubkat_id` int(11) NOT NULL,
  `harga_normal` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `harga_diskon` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `foto` text NOT NULL,
  `foto_type` char(10) NOT NULL,
  `uploader` char(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updater` char(20) NOT NULL,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `bandingkan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id_produk`, `judul_produk`, `slug_produk`, `keywords`, `deskripsi`, `berat`, `kat_id`, `subkat_id`, `supersubkat_id`, `harga_normal`, `diskon`, `harga_diskon`, `stok`, `foto`, `foto_type`, `uploader`, `created`, `updater`, `modified`, `bandingkan`) VALUES
(12, 'Intel Core i7-9700K', 'intel-core-i7-9700k', 'intel i9 prosesor gaming ', '<table xss=removed>\r\n<tbody>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed><strong>Intel Core i7-9700K</strong></td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>Coffee Lake Refresh</td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>8 Cores & 8 Threads</td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>3.6 GHz</td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>4.9 GHz </td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>LGA 1151</td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>12MB </td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>Intel UHD Graphics 630</td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>Dual-Channel DDR4-2666Mhz</td>\r\n</tr>\r\n<tr>\r\n<td xss=removed> </td>\r\n<td xss=removed>Intel 300 Series</td>\r\n</tr>\r\n</tbody>\r\n</table>', 2, 3, 8, 12, 11000000, 2, 10780000, 46, 'intel-core-i7-9700k20190521193541', '.jpg', 'superadmin', '2019-05-21 19:35:41', '', '2019-07-19 08:51:31', ''),
(13, 'AMD - Ryzen 7 2700X', 'amd-ryzen-7-2700x', 'amd, prosesor, gaming, ryzen', '<table border=\"1\">\r\n<tbody>\r\n<tr>\r\n<td width=\"30%\">Segment</td>\r\n<td width=\"70%\">Desktop</td>\r\n</tr>\r\n<tr>\r\n<td>Family</td>\r\n<td>AMD Ryzen 7</td>\r\n</tr>\r\n<tr>\r\n<td>Model Number</td>\r\n<td>2700X</td>\r\n</tr>\r\n<tr>\r\n<td>Part Number</td>\r\n<td>YD270XBGAFBOX</td>\r\n</tr>\r\n<tr>\r\n<td>Frequency</td>\r\n<td>3700 MHz</td>\r\n</tr>\r\n<tr>\r\n<td>Turbo Frequency</td>\r\n<td>4300 MHz</td>\r\n</tr>\r\n<tr>\r\n<td>Socket</td>\r\n<td>Socket AM4</td>\r\n</tr>\r\n<tr>\r\n<td>Introduction Date</td>\r\n<td>Q1 2018</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><br><strong>Architecture / Microarchitecture</strong></p>\r\n<table border=\"1\">\r\n<tbody>\r\n<tr>\r\n<td width=\"30%\">Processor Core</td>\r\n<td width=\"70%\">Pinnacle Ridge</td>\r\n</tr>\r\n<tr>\r\n<td>Manufacturing Process</td>\r\n<td>12 nm</td>\r\n</tr>\r\n<tr>\r\n<td>Data Width</td>\r\n<td>64 bit</td>\r\n</tr>\r\n<tr>\r\n<td>The Number of CPU Cores</td>\r\n<td>8</td>\r\n</tr>\r\n<tr>\r\n<td>The Number of Threads</td>\r\n<td>16</td>\r\n</tr>\r\n<tr>\r\n<td>Level 1 cache size</td>\r\n<td>8 x 64 KB instruction caches<br>8 x 32 KB data caches</td>\r\n</tr>\r\n<tr>\r\n<td>Level 2 cache size</td>\r\n<td>8 x 512 KB caches</td>\r\n</tr>\r\n<tr>\r\n<td>Level 3 cache size</td>\r\n<td>16 MB shared cache</td>\r\n</tr>\r\n<tr>\r\n<td>Physical memory</td>\r\n<td>64 GB</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><br><strong>Integrated Peripherals / Components</strong></p>\r\n<table border=\"1\">\r\n<tbody>\r\n<tr>\r\n<td width=\"30%\">Display controller</td>\r\n<td width=\"70%\">-</td>\r\n</tr>\r\n<tr>\r\n<td>Integrated graphics</td>\r\n<td>None</td>\r\n</tr>\r\n<tr>\r\n<td>Memory controller</td>\r\n<td>Memory channels: 2<br>Supported memory: DDR4-2933</td>\r\n</tr>\r\n<tr>\r\n<td>Other peripherals</td>\r\n<td>-</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><br><strong>Other Information</strong></p>\r\n<table border=\"1\">\r\n<tbody>\r\n<tr>\r\n<td width=\"30%\">Thermal Design Power</td>\r\n<td width=\"70%\">105 Watt</td>\r\n</tr>\r\n<tr>\r\n<td>Unlocked Clock Multiplier</td>\r\n<td>Yes</td>\r\n</tr>\r\n<tr>\r\n<td><span class=\"tooltippable\">ECC Memory Supported</span></td>\r\n<td>No</td>\r\n</tr>\r\n<tr>\r\n<td><span class=\"tooltippable\">Fan Included</span></td>\r\n<td>Yes (Wraith Spire (LED))</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>', 1, 3, 9, 16, 4800000, 2, 4704000, 76, 'amd-ryzen-7-2700x20190522003423', '.jpg', 'superadmin', '2019-05-22 00:34:24', 'administrator', '2019-07-23 07:54:44', ''),
(33, 'intel i5 9700', 'intel-i5-9700', '', '<p>wadidaw</p>', 2, 3, 8, 12, 1000000, 0, 1000000, 51, 'intel-i5-970020190718071145', '.png', '', '2019-07-04 18:19:57', '', '2019-07-22 11:21:54', ''),
(34, 'AMD FirePro 512 MB 2460', 'amd-firepro-512-mb-2460', 'amd firepro 512 mb 2460', '<p>Dengan menghadirkan teknologi multi-display inovatif yang disebut dengan AMD Eyefinity, kartu grafis <strong>AMD FirePro 2460</strong> dapat dijadikan sebagai solusi ideal apabila Anda ingin merakit PC dalam ukuran form-factor yang kecil namun memiliki kemampuan hebat dalam menampilkan display berkualitas dan beresolusi tinggi. Dengan dukungan data display yang mencapai hingga 16 megapiksel, kartu grafis ini mampu menanggung beban kerja multi-monitor yang sibuk sehingga Anda dapat dengan nyaman menampilkan semua yang diperlukan secara bersamaan.</p>', 1, 5, 10, 16, 9000000, 10, 8100000, 1, 'amd-firepro-512-mb-246020190717175054', '.jpg', 'iqbalowner', '2019-07-17 17:50:54', '', '2019-07-23 08:06:02', ''),
(35, 'INTEL Processor Core i9-4150', 'intel-processor-core-i9-4150', 'intel processor core i9-4150', '<table class=\"table bt-product__table\">\r\n<tbody>\r\n<tr>\r\n<td class=\"key\">Socket</td>\r\n<td>\r\n<ul>\r\n<li>LGA1150</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Jumlah Inti / Core</td>\r\n<td>\r\n<ul>\r\n<li>2</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Jumlah Benang / Thread</td>\r\n<td>\r\n<ul>\r\n<li>4</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Kecepatan Clock</td>\r\n<td>\r\n<ul>\r\n<li>3.5 GHz</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Cache</td>\r\n<td>\r\n<ul>\r\n<li>3 MB</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">DMI</td>\r\n<td>\r\n<ul>\r\n<li>5 GT/s</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Instruction Set</td>\r\n<td>\r\n<ul>\r\n<li>64-bit</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Litography</td>\r\n<td>\r\n<ul>\r\n<li>22 nm</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Maks. Memori</td>\r\n<td>\r\n<ul>\r\n<li>32 GB</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Tipe Memori</td>\r\n<td>\r\n<ul>\r\n<li>DDR3-1333/1600</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Number of Memory Channel</td>\r\n<td>\r\n<ul>\r\n<li>Dual</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Kesesuaian Memori ECC</td>\r\n<td>\r\n<ul>\r\n<li>Yes</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Processor Graphics</td>\r\n<td>\r\n<ul>\r\n<li>Intel® HD Graphics 4400</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">PCI Express Revision</td>\r\n<td>\r\n<ul>\r\n<li>3.0</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class=\"key\">Garansi</td>\r\n<td>\r\n<ul>\r\n<li>12 Bulan dari Distributor Resmi di Indonesia</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 2, 3, 8, 12, 4000000, 10, 3600000, 4, 'intel-processor-core-i9-415020190718081459', '.jpg', 'owner', '2019-07-18 08:14:59', '', '2019-07-23 00:02:19', ''),
(36, 'Ryzen™ 7', 'ryzen-7', '', '<p>Pinnacle Ridge nampak jelas memiliki clockspeed yang lumayan ditingkatkan dari Ryzen Generasi pertama ‘Summit Ridge’. Pada Ryzen 7 2700X, AMD menyatakan clockspeed maksimum saat boost adalah 4.3Ghz. Jika Anda menyaksikan berbagai artikel overclocking yang dibuat oleh rekan-rekan kami di JagatOC, nilai 4.3Ghz ini jelas merupakan clockspeed yang belum bisa dicapai Ryzen generasi sebelumnya , dan ini mengindikasikan bahwa AMD melakukan sejumlah optimalisasi pada proses fabrikasi yang mereka pakai untuk Ryzen 2nd Gen.</p>', 0, 3, 8, 12, 2000000, 0, 0, 1, 'ryzen-720190722232044', '.jpg', '', '2019-07-22 11:21:54', '', '2019-07-23 00:02:23', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `nama_provinsi`) VALUES
(1, 'Bali'),
(2, 'Bangka Belitung'),
(3, 'Banten'),
(4, 'Bengkulu'),
(5, 'DI Yogyakarta'),
(6, 'DKI Jakarta'),
(7, 'Gorontalo'),
(8, 'Jambi'),
(9, 'Jawa Barat'),
(10, 'Jawa Tengah'),
(11, 'Jawa Timur'),
(12, 'Kalimantan Barat'),
(13, 'Kalimantan Selatan'),
(14, 'Kalimantan Tengah'),
(15, 'Kalimantan Timur'),
(16, 'Kalimantan Utara'),
(17, 'Kepulauan Riau'),
(18, 'Lampung'),
(19, 'Maluku'),
(20, 'Maluku Utara'),
(21, 'Nanggroe Aceh Darussalam (NAD)'),
(22, 'Nusa Tenggara Barat (NTB)'),
(23, 'Nusa Tenggara Timur (NTT)'),
(24, 'Papua'),
(25, 'Papua Barat'),
(26, 'Riau'),
(27, 'Sulawesi Barat'),
(28, 'Sulawesi Selatan'),
(29, 'Sulawesi Tengah'),
(30, 'Sulawesi Tenggara'),
(31, 'Sulawesi Utara'),
(32, 'Sumatera Barat'),
(33, 'Sumatera Selatan'),
(34, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id_rating` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tingkat_rating` int(5) NOT NULL,
  `komentar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rating`
--

INSERT INTO `rating` (`id_rating`, `id_produk`, `id_user`, `tingkat_rating`, `komentar`) VALUES
(1, 35, 24, 4, 'asli keren'),
(2, 33, 9, 5, 'Mantap Sekali Barangnya'),
(3, 34, 24, 5, 'Bagus Sekali'),
(4, 34, 9, 5, 'Keren Euy'),
(5, 35, 24, 4, 'Bagus Nih');

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur`
--

CREATE TABLE `retur` (
  `id_retur` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pesan` text NOT NULL,
  `pesan_admin` text NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  `no_resi` varchar(20) NOT NULL,
  `no_resi_admin` varchar(20) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `no_rekening` varchar(30) NOT NULL,
  `status_customer` varchar(100) NOT NULL,
  `status_admin` varchar(50) NOT NULL,
  `biaya_kirim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `retur`
--

INSERT INTO `retur` (`id_retur`, `user_id`, `pesan`, `pesan_admin`, `nama_barang`, `tanggal`, `no_resi`, `no_resi_admin`, `no_invoice`, `no_rekening`, `status_customer`, `status_admin`, `biaya_kirim`) VALUES
(25, 24, 'Barangnya ada yang lecet', '', '', '2019-07-22', '123572', '', 'FS220719007', '', 'terkirim ke toko', 'Belum Retur', 0),
(26, 24, 'Ini Barangnya akan segera diganti yang baru', 'Barang sudah diterima, mohon menunggu beberapa hari lagi barang akan dikirim ke rumah anda', '', '2019-07-22', '343123', '4232', 'FS220719005', '', 'Dikirim', 'Belum Retur', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_detail`
--

CREATE TABLE `retur_detail` (
  `id_retur_detail` int(11) NOT NULL,
  `id_retur` int(11) NOT NULL,
  `nama_barang` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `retur_detail`
--

INSERT INTO `retur_detail` (`id_retur_detail`, `id_retur`, `nama_barang`) VALUES
(19, 25, 'INTEL Processor Core i9-4150 , '),
(20, 25, 'AMD FirePro 512 MB 2460 , '),
(21, 25, 'AMD - Ryzen 7 2700X , '),
(22, 26, 'INTEL Processor Core i9-4150 , '),
(23, 26, 'AMD FirePro 512 MB 2460 , '),
(24, 26, 'AMD - Ryzen 7 2700X , ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider`
--

CREATE TABLE `slider` (
  `id_slider` int(11) NOT NULL,
  `no_urut` int(11) NOT NULL,
  `judul_slider` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `foto` text NOT NULL,
  `foto_type` char(10) NOT NULL,
  `foto_size` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(50) NOT NULL,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slider`
--

INSERT INTO `slider` (`id_slider`, `no_urut`, `judul_slider`, `link`, `foto`, `foto_type`, `foto_size`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(3, 1, 'FS KOMPUTER', 'http://www.facebook.com', 'fs-komputer20190521214058', '.png', 167, '2017-11-25 08:05:03', '', '2019-05-21 21:40:58', 'irman'),
(4, 3, 'MS Lighting Nvdia RTX 2081', '', 'nvdia-rtx-208120190521202905', '.png', 0, '2019-05-21 19:52:06', 'superadmin', '2019-05-21 21:25:12', 'irman');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subkategori`
--

CREATE TABLE `subkategori` (
  `id_subkategori` int(11) NOT NULL,
  `id_kat` int(11) NOT NULL,
  `judul_subkategori` char(50) NOT NULL,
  `slug_subkat` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `subkategori`
--

INSERT INTO `subkategori` (`id_subkategori`, `id_kat`, `judul_subkategori`, `slug_subkat`) VALUES
(8, 3, 'Intel', 'intel'),
(9, 3, 'AMD', 'amd'),
(10, 5, 'AMD', 'amd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supersubkategori`
--

CREATE TABLE `supersubkategori` (
  `id_supersubkategori` int(11) NOT NULL,
  `id_subkat` int(11) NOT NULL,
  `id_kat` int(11) NOT NULL,
  `judul_supersubkategori` char(50) NOT NULL,
  `slug_supersubkat` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supersubkategori`
--

INSERT INTO `supersubkategori` (`id_supersubkategori`, `id_subkat`, `id_kat`, `judul_supersubkategori`, `slug_supersubkat`) VALUES
(12, 8, 3, 'i9 series', 'i9-series'),
(14, 10, 5, 'ASUS', 'asus'),
(15, 10, 5, 'MSI', 'msi'),
(16, 9, 3, 'Ryzen 2nd', 'ryzen-2nd'),
(17, 10, 5, 'AMD FirePro', 'amd-firepro');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `id_supp` varchar(5) NOT NULL,
  `nama_supp` varchar(35) NOT NULL,
  `no_supp` varchar(13) NOT NULL,
  `alamat_supp` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`id_supp`, `nama_supp`, `no_supp`, `alamat_supp`) VALUES
('SP1', 'Techno Magic', '089626352', 'Jl Sekeloa '),
('SP2', 'Technologi', '0836253662', 'Jl Kubang Sari Selatan'),
('SP3', 'Astrindo', '0897266599', 'bandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_trans` int(11) NOT NULL,
  `nama_pembeli` varchar(50) NOT NULL,
  `no_invoice` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` date NOT NULL,
  `ongkir` int(11) NOT NULL,
  `kurir` char(20) NOT NULL,
  `service` char(50) NOT NULL,
  `status` int(11) NOT NULL,
  `resi` char(50) DEFAULT NULL,
  `jenis_transaksi` varchar(20) NOT NULL,
  `alamat_pembeli` varchar(255) NOT NULL,
  `cart` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_trans`, `nama_pembeli`, `no_invoice`, `user_id`, `created`, `ongkir`, `kurir`, `service`, `status`, `resi`, `jenis_transaksi`, `alamat_pembeli`, `cart`, `email`, `alamat`) VALUES
(197, 'iqbalshorf', 'FS220719001', NULL, '2019-07-22', 10000, 'jne', 'OKE', 2, '36234', 'online', '', 'ya', 'shorfanaiqbal98@gmail.com', 'Jl Keramat raya'),
(198, 'imam', 'FS220719002', NULL, '2019-07-22', 0, '', '', 1, NULL, 'offline', '', 'ya', 'imam@email.com', 'Jl Banjar'),
(199, 'jupri', 'FS220719003', NULL, '2019-07-22', 47000, 'jne', 'OKE', 1, NULL, 'online', '', 'ya', '', ''),
(200, 'sasay', 'FS220719004', 24, '2019-07-22', 40000, 'jne', 'OKE', 2, '31234', 'online', '', '', '', ''),
(201, '', 'FS220719005', 24, '2019-07-22', 40000, 'pos', 'Paket Kilat Khusus', 2, '4152', 'online', '', '', '', ''),
(202, '', 'FS220719006', 24, '2019-07-22', 20000, 'pos', 'Paket Kilat Khusus', 2, '1223', 'online', '', '', '', ''),
(203, '', 'FS220719007', 24, '2019-07-22', 40000, 'jne', 'OKE', 2, '23123', 'online', '', '', '', ''),
(204, '', 'FS230719008', 24, '2019-07-23', 40000, 'jne', 'OKE', 1, NULL, 'online', '', '', '', ''),
(205, '', 'FS230719009', 24, '2019-07-23', 80000, 'jne', 'OKE', 1, NULL, 'online', '', '', '', ''),
(206, '', 'FS230719010', 24, '2019-07-23', 250000, 'jne', 'OKE', 1, NULL, 'online', '', '', '', ''),
(207, '', 'FS230719011', 24, '2019-07-23', 230000, 'jne', 'OKE', 1, NULL, 'online', '', '', '', ''),
(208, '', 'FS230719012', 24, '2019-07-23', 11000, 'jne', 'REG', 1, NULL, 'online', '', '', '', ''),
(209, '', 'FS230719013', 24, '2019-07-23', 11000, 'jne', 'REG', 1, NULL, 'online', '', '', '', ''),
(210, '', 'FS230719014', 24, '2019-07-23', 10000, 'pos', 'Paket Kilat Khusus', 1, NULL, 'online', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `id_transdet` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL,
  `produk_id` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `total_berat` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`id_transdet`, `trans_id`, `user`, `produk_id`, `harga`, `berat`, `total_qty`, `total_berat`, `subtotal`) VALUES
(246, 197, NULL, 35, 4000000, 2, 2, 4, 7200000),
(247, 197, NULL, 34, 9000000, 1, 2, 2, 16200000),
(248, 197, NULL, 13, 4800000, 1, 1, 1, 4800000),
(249, 198, NULL, 35, 4000000, 2, 2, 4, 7200000),
(250, 199, NULL, 34, 8100000, 1, 2, 2, 16200000),
(251, 200, 24, 35, 3600000, 2, 4, 8, 14400000),
(252, 201, 24, 35, 3600000, 2, 4, 8, 14400000),
(253, 201, 24, 34, 8100000, 1, 4, 4, 32400000),
(254, 201, 24, 13, 4704000, 1, 23, 23, 108192000),
(255, 202, 24, 35, 3600000, 2, 4, 8, 14400000),
(256, 203, 24, 35, 3600000, 2, 4, 8, 14400000),
(257, 203, 24, 34, 8100000, 1, 4, 4, 32400000),
(258, 203, 24, 13, 4704000, 1, 23, 23, 108192000),
(260, 204, 24, 34, 8100000, 1, 4, 4, 32400000),
(261, 205, 24, 35, 3600000, 2, 4, 8, 14400000),
(262, 206, 24, 13, 4704000, 1, 23, 23, 108192000),
(263, 207, 24, 13, 4704000, 1, 23, 23, 108192000),
(264, 208, 24, 34, 8100000, 1, 1, 1, 8100000),
(265, 209, 24, 34, 8100000, 1, 1, 1, 8100000),
(266, 210, 24, 34, 8100000, 1, 1, 1, 8100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `provinsi` int(11) DEFAULT NULL,
  `kota` int(11) DEFAULT NULL,
  `address` text NOT NULL,
  `usertype` int(11) NOT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `photo` text,
  `photo_type` varchar(10) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `verifikasi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `phone`, `provinsi`, `kota`, `address`, `usertype`, `active`, `photo`, `photo_type`, `ip_address`, `salt`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `last_login`, `created_on`, `modified`, `verifikasi`) VALUES
(1, 'Super Admin', 'superadmin', '$2y$12$yCBAksqkS3cp6WFu6UuKY.PycHpprH43dvZlbRHurBCDIMqYh4Nfa', 'azmicolejr@gmail.com', '081228289766', 6, 151, 'asdasdasdsa', 1, 1, 'mazmi20180205001726', '.jpg', '::1', NULL, 'c6ad242e6fd3de875568c7de5ba23af4a24137ef', 'HhFOZk3TMjY6njEY8wfBiea50e1717cfd64c6435', 1519646514, 'jx5JLtg2FLyxAHQBURioQe', 1558531023, 2147483647, '2019-05-22 20:17:03', ''),
(2, 'Armageddon', 'armageddon', '$2y$12$yCBAksqkS3cp6WFu6UuKY.PycHpprH43dvZlbRHurBCDIMqYh4Nfa', 'armageddon@gmail.com', '0824124124', 6, 155, 'Jl. SUka saya aja', 5, 1, NULL, NULL, '::1', NULL, '75e7428e2e8d15940b14607a13ac0d1b5b364932', NULL, NULL, NULL, 1520134948, 2147483647, '2019-05-21 19:15:17', ''),
(3, 'Gaming', 'gaming', '$2y$12$yCBAksqkS3cp6WFu6UuKY.PycHpprH43dvZlbRHurBCDIMqYh4Nfa', 'gaming@gmail.com', '0841274124', 9, 22, 'gaksdlfskldfj', 5, 1, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL, 1519708259, 2147483647, '2019-05-21 19:15:22', ''),
(4, 'Administrator', 'administrator', '$2y$08$RxA8ieI7rk5fQ4jGe6kLhubIGX294.GDj.v.QrO3i3vhNjg9QapUq', 'admin@gmail.com', '08214124', 6, 151, 'akldjlasdj', 2, 1, 'administrator20180210234128', '.png', '::1', NULL, NULL, 'C.SX8e8r3GGWQ.CDALstRe9ed4f49a4a75569f85', 1560856888, 'VQZF3LNSVdiOiTBZKgYuvu', 1560862351, 2147483647, '2019-06-23 05:28:54', ''),
(5, 'Azmi Jr', 'azmijr', '$2y$12$yCBAksqkS3cp6WFu6UuKY.PycHpprH43dvZlbRHurBCDIMqYh4Nfa', 'azmi2793@gmail.com', '081228289766', 18, 21, 'akjdlajsdkl asldjals djaslkdjsa ldjalskjd lsakjd', 5, 1, NULL, NULL, '36.77.102.70', NULL, NULL, NULL, NULL, NULL, 1519646726, 1519390594, '2019-05-21 19:15:29', ''),
(6, 'Mazmi', 'mazmi2793', '$2y$12$yCBAksqkS3cp6WFu6UuKY.PycHpprH43dvZlbRHurBCDIMqYh4Nfa', 'mazmi2793@gmail.com', '0812414124', 2, 27, 'lAKJsdl jaslkdjaskldj ojqwo djqiowd jioqwd', 5, 0, NULL, NULL, '36.77.102.70', NULL, '5f6c1d811a88a3e01072c45034d5efec61643783', 'rgcFpWT..xG8dAPcLm3.7u3c240e1445898c72a1', 1519535860, NULL, 1521376310, 1519535701, '2019-05-22 16:36:15', ''),
(7, 'irman', 'irman', '$2y$08$KM.CohqHO0Q6cG6hMjDBqedugO1ximCl9JJjgtnJGL5rPqBQ4GhpW', 'sullivanx94@gmail.com', '087725657897', 9, 22, 'qweqwq', 5, 1, NULL, NULL, '::1', NULL, NULL, NULL, NULL, 'wR.4vVyivA1Ys9ixaBLJ3O', 1558531914, 1558445887, '2019-05-22 20:31:54', ''),
(8, 'werwer', 'erqerwe', '$2y$08$mBGBV9AoBEq4SFnkyPLneOjTU2Yr.B28W/NpcnNXR3KHkHZ5KeNCK', 'irman_boys@ymail.com', '08678787', 9, 22, 'sdfsdfsdf', 5, 1, NULL, NULL, '::1', NULL, 'hahaha', NULL, NULL, NULL, 1558967867, 1558967658, '2019-07-16 18:46:22', ''),
(9, 'fun mr', 'mrfun', '$2y$08$.1a4iccNkrWg/CDImkz4d.eEsErHnX4uvbXcy073/Q5D/3Mg7DRgS', 'sullivanx954@gmail.com', '567567474674', 9, 22, 'qwwrrqr', 5, 1, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL, 1559022214, 1558971734, '2019-05-28 12:43:34', ''),
(17, 'admin', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin@gmail.com', '0895372111373', 16, 10, 'asadsa', 2, NULL, 'admin20190723091650', '.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-07-23 09:16:50', ''),
(18, 'owner', 'owner', '579233b2c479241523cba5e3af55d0f50f2d6414', 'owner@gmail.com', '0898237765', 6, NULL, 'dasdasdw', 6, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-07-05 04:17:12', ''),
(24, 'iqbal', 'iqbal', '$2y$08$7NMZFA4zvi3iCtAbDotizePHaHERO.YxQ29WAQ9/x/NIvbBhSZe1q', 'iqbalshorfana@gmail.com', '089636432', 6, 155, 'Jl Tubagus Ismail', 5, 2, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL, 1563935719, 1563769661, '2019-07-24 09:35:19', NULL),
(25, 'superadmin aja', 'superadmin', '1a37e5eb4ea24b24b0d287e0df00f91715ca2e04', 'superadmin11@gmail.com', '089176212', 13, 10, 'Jl kubangsari', 1, 1, 'superadmin20190723092430', '.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-07-23 09:27:16', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_groups`
--

CREATE TABLE `users_groups` (
  `id_group` int(11) NOT NULL,
  `name_group` varchar(20) NOT NULL,
  `group_id` mediumint(8) NOT NULL,
  `user_id` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users_groups`
--

INSERT INTO `users_groups` (`id_group`, `name_group`, `group_id`, `user_id`) VALUES
(1, 'superadmin', 0, 0),
(2, 'admin', 0, 0),
(3, 'member', 0, 0),
(4, 'seller', 0, 0),
(5, 'buyer', 0, 0),
(6, 'owner', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indeks untuk tabel `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id_company`);

--
-- Indeks untuk tabel `featured`
--
ALTER TABLE `featured`
  ADD PRIMARY KEY (`id_featured`),
  ADD KEY `FK_featured_produk` (`produk_id`);

--
-- Indeks untuk tabel `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id_kontak`);

--
-- Indeks untuk tabel `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`),
  ADD KEY `FK_kota_provinsi` (`provinsi_id`);

--
-- Indeks untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indeks untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indeks untuk tabel `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pemdet`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `FK_produk_kategori` (`kat_id`),
  ADD KEY `FK_produk_subkategori` (`subkat_id`),
  ADD KEY `FK_produk_supersubkategori` (`supersubkat_id`);

--
-- Indeks untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indeks untuk tabel `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id_rating`);

--
-- Indeks untuk tabel `retur`
--
ALTER TABLE `retur`
  ADD PRIMARY KEY (`id_retur`);

--
-- Indeks untuk tabel `retur_detail`
--
ALTER TABLE `retur_detail`
  ADD PRIMARY KEY (`id_retur_detail`);

--
-- Indeks untuk tabel `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indeks untuk tabel `subkategori`
--
ALTER TABLE `subkategori`
  ADD PRIMARY KEY (`id_subkategori`),
  ADD KEY `subkategori_ibfk_1` (`id_kat`);

--
-- Indeks untuk tabel `supersubkategori`
--
ALTER TABLE `supersubkategori`
  ADD PRIMARY KEY (`id_supersubkategori`),
  ADD KEY `supersubkategori_ibfk_1` (`id_subkat`),
  ADD KEY `supersubkategori_ibfk_2` (`id_kat`);

--
-- Indeks untuk tabel `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supp`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_trans`),
  ADD KEY `FK_transaksi_users` (`user_id`);

--
-- Indeks untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`id_transdet`),
  ADD KEY `FK_transaksi_detail_transaksi` (`trans_id`),
  ADD KEY `FK_transaksi_detail_produk` (`produk_id`),
  ADD KEY `FK_transaksi_detail_users` (`user`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_users_users_group` (`usertype`),
  ADD KEY `kota` (`kota`),
  ADD KEY `provinsi` (`provinsi`);

--
-- Indeks untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id_group`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id_blog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `company`
--
ALTER TABLE `company`
  MODIFY `id_company` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `featured`
--
ALTER TABLE `featured`
  MODIFY `id_featured` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id_kontak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pemdet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_provinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `rating`
--
ALTER TABLE `rating`
  MODIFY `id_rating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `retur`
--
ALTER TABLE `retur`
  MODIFY `id_retur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `retur_detail`
--
ALTER TABLE `retur_detail`
  MODIFY `id_retur_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `slider`
--
ALTER TABLE `slider`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `subkategori`
--
ALTER TABLE `subkategori`
  MODIFY `id_subkategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `supersubkategori`
--
ALTER TABLE `supersubkategori`
  MODIFY `id_supersubkategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_trans` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `id_transdet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `featured`
--
ALTER TABLE `featured`
  ADD CONSTRAINT `FK_featured_produk` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kota`
--
ALTER TABLE `kota`
  ADD CONSTRAINT `FK_kota_provinsi` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id_provinsi`);

--
-- Ketidakleluasaan untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD CONSTRAINT `FK_login_attempts_users` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `FK_produk_kategori` FOREIGN KEY (`kat_id`) REFERENCES `kategori` (`id_kategori`),
  ADD CONSTRAINT `FK_produk_subkategori` FOREIGN KEY (`subkat_id`) REFERENCES `subkategori` (`id_subkategori`),
  ADD CONSTRAINT `FK_produk_supersubkategori` FOREIGN KEY (`supersubkat_id`) REFERENCES `supersubkategori` (`id_supersubkategori`);

--
-- Ketidakleluasaan untuk tabel `subkategori`
--
ALTER TABLE `subkategori`
  ADD CONSTRAINT `subkategori_ibfk_1` FOREIGN KEY (`id_kat`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `supersubkategori`
--
ALTER TABLE `supersubkategori`
  ADD CONSTRAINT `supersubkategori_ibfk_1` FOREIGN KEY (`id_subkat`) REFERENCES `subkategori` (`id_subkategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supersubkategori_ibfk_2` FOREIGN KEY (`id_kat`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `FK_transaksi_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD CONSTRAINT `FK_transaksi_detail_produk` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_transaksi_detail_transaksi` FOREIGN KEY (`trans_id`) REFERENCES `transaksi` (`id_trans`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_transaksi_detail_users` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_users_users_group` FOREIGN KEY (`usertype`) REFERENCES `users_groups` (`id_group`),
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`kota`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`provinsi`) REFERENCES `provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
